package com.kamil.exampleapp.screen.browseproducts;

import com.kamil.exampleapp.screen.browseproducts.model.ProductItem;
import com.kamil.exampleapp.screen.browseproducts.model.InFavoritesState;
import com.kamil.exampleapp.screen.browseproducts.model.RemovalState;
import com.kamil.exampleapp.testdata.ProductItemTestData;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class ProductItemTest {

    private ProductItemTestData testData;

    @Before
    public void setUp() {
        testData = new ProductItemTestData();
    }

    @Test
    public void shouldProductItemBeValidWhenAllInputDataCorrect() {
        ProductItem productItem =
                new ProductItem.Builder()
                        .id(testData.goodId)
                        .name(testData.goodName)
                        .brand(testData.goodBrand)
                        .price(testData.goodPrice)
                        .inFavoriteState(InFavoritesState.NOT_ADDED)
                        .removalState(RemovalState.NOT_REMOVED)
                        .assignedHierarchies(testData.goodHierarchiesId)
                        .build();

        assertTrue(productItem.isValid());
    }

    @Test
    public void shouldProductItemBeNotValidWhenInputIdIsNull() {
        ProductItem productItem =
                new ProductItem.Builder()
                        .id(null)
                        .name(testData.goodName)
                        .brand(testData.goodBrand)
                        .price(testData.goodPrice)
                        .inFavoriteState(InFavoritesState.NOT_ADDED)
                        .removalState(RemovalState.NOT_REMOVED)
                        .assignedHierarchies(testData.goodHierarchiesId)
                        .build();

        assertFalse(productItem.isValid());
    }

    @Test
    public void shouldProductItemBeNotValidWhenInputIdIsZero() {
        ProductItem productItem =
                new ProductItem.Builder()
                        .id(testData.badZeroId)
                        .name(testData.goodName)
                        .brand(testData.goodBrand)
                        .price(testData.goodPrice)
                        .inFavoriteState(InFavoritesState.NOT_ADDED)
                        .removalState(RemovalState.NOT_REMOVED)
                        .assignedHierarchies(testData.goodHierarchiesId)
                        .build();

        assertFalse(productItem.isValid());
    }

    @Test
    public void shouldProductItemBeNotValidWhenInputNameIsEmpty() {
        ProductItem productItem =
                new ProductItem.Builder()
                        .id(testData.goodId)
                        .name(testData.empty_string)
                        .brand(testData.goodBrand)
                        .price(testData.goodPrice)
                        .inFavoriteState(InFavoritesState.NOT_ADDED)
                        .removalState(RemovalState.NOT_REMOVED)
                        .assignedHierarchies(testData.goodHierarchiesId)
                        .build();

        assertFalse(productItem.isValid());
    }

    @Test
    public void shouldProductItemBeNotValidWhenInputNameIsNull() {
        ProductItem productItem =
                new ProductItem.Builder()
                        .id(testData.goodId)
                        .name(null)
                        .brand(testData.goodBrand)
                        .price(testData.goodPrice)
                        .inFavoriteState(InFavoritesState.NOT_ADDED)
                        .removalState(RemovalState.NOT_REMOVED)
                        .assignedHierarchies(testData.goodHierarchiesId)
                        .build();

        assertFalse(productItem.isValid());
    }

    @Test
    public void shouldProductItemBeNotValidWhenInputBrandIsEmpty() {
        ProductItem productItem =
                new ProductItem.Builder()
                        .id(testData.goodId)
                        .name(testData.goodName)
                        .brand(testData.empty_string)
                        .price(testData.goodPrice)
                        .inFavoriteState(InFavoritesState.NOT_ADDED)
                        .removalState(RemovalState.NOT_REMOVED)
                        .assignedHierarchies(testData.goodHierarchiesId)
                        .build();

        assertFalse(productItem.isValid());
    }

    @Test
    public void shouldProductItemBeNotValidWhenInputBrandIsNull() {
        ProductItem productItem =
                new ProductItem.Builder()
                        .id(testData.goodId)
                        .name(testData.goodName)
                        .brand(null)
                        .price(testData.goodPrice)
                        .inFavoriteState(InFavoritesState.NOT_ADDED)
                        .removalState(RemovalState.NOT_REMOVED)
                        .assignedHierarchies(testData.goodHierarchiesId)
                        .build();

        assertFalse(productItem.isValid());
    }

    @Test
    public void shouldProductItemBeNotValidWhenInputPriceIsNull() {
        ProductItem productItem =
                new ProductItem.Builder()
                        .id(testData.goodId)
                        .name(testData.goodName)
                        .brand(testData.goodBrand)
                        .price(null)
                        .inFavoriteState(InFavoritesState.NOT_ADDED)
                        .removalState(RemovalState.NOT_REMOVED)
                        .assignedHierarchies(testData.goodHierarchiesId)
                        .build();

        assertFalse(productItem.isValid());
    }

    @Test
    public void shouldProductItemBeNotValidWhenInputPriceIsEmpty() {
        ProductItem productItem =
                new ProductItem.Builder()
                        .id(testData.goodId)
                        .name(testData.goodName)
                        .brand(testData.goodBrand)
                        .price(testData.empty_string)
                        .inFavoriteState(InFavoritesState.NOT_ADDED)
                        .removalState(RemovalState.NOT_REMOVED)
                        .assignedHierarchies(testData.goodHierarchiesId)
                        .build();

        assertFalse(productItem.isValid());
    }

    @Test
    public void shouldProductItemBeNotValidWhenInputPriceBadFormat() {
        ProductItem productItem =
                new ProductItem.Builder()
                        .id(testData.goodId)
                        .name(testData.goodName)
                        .brand(testData.goodBrand)
                        .price(testData.badPriceFormat)
                        .inFavoriteState(InFavoritesState.NOT_ADDED)
                        .removalState(RemovalState.NOT_REMOVED)
                        .assignedHierarchies(testData.goodHierarchiesId)
                        .build();

        assertFalse(productItem.isValid());
    }

    @Test
    public void shouldProductItemBeNotValidWhenInputInFavoritesStateIsNull() {
        ProductItem productItem =
                new ProductItem.Builder()
                        .id(testData.goodId)
                        .name(testData.goodName)
                        .brand(testData.goodBrand)
                        .price(testData.goodPrice)
                        .inFavoriteState(null)
                        .removalState(RemovalState.NOT_REMOVED)
                        .assignedHierarchies(testData.goodHierarchiesId)
                        .build();

        assertFalse(productItem.isValid());
    }

    @Test
    public void shouldProductItemBeNotValidWhenInputRemovalStateIsNull() {
        ProductItem productItem =
                new ProductItem.Builder()
                        .id(testData.goodId)
                        .name(testData.goodName)
                        .brand(testData.goodBrand)
                        .price(testData.goodPrice)
                        .inFavoriteState(InFavoritesState.NOT_ADDED)
                        .removalState(null)
                        .assignedHierarchies(testData.goodHierarchiesId)
                        .build();

        assertFalse(productItem.isValid());
    }



    @Test
    public void shouldProductItemBeNotValidWhenInputAssignedHierarchiesNull() {
        ProductItem productItem =
                new ProductItem.Builder()
                        .id(testData.goodId)
                        .name(testData.goodName)
                        .brand(testData.goodBrand)
                        .price(testData.goodPrice)
                        .inFavoriteState(InFavoritesState.NOT_ADDED)
                        .removalState(RemovalState.NOT_REMOVED)
                        .assignedHierarchies(null)
                        .build();

        assertFalse(productItem.isValid());
    }

    @Test
    public void shouldProductItemBeNotValidWhenInputAssignedHierarchiesEmpty() {
        ProductItem productItem =
                new ProductItem.Builder()
                        .id(testData.goodId)
                        .name(testData.goodName)
                        .brand(testData.goodBrand)
                        .price(testData.goodPrice)
                        .inFavoriteState(InFavoritesState.NOT_ADDED)
                        .removalState(RemovalState.NOT_REMOVED)
                        .assignedHierarchies(testData.badEmptyHierarchiesId)
                        .build();

        assertFalse(productItem.isValid());
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionWhenCopyAndModifyInFavoritesStateInputProductItemNull() {
        ProductItem.copyAndModify(null, InFavoritesState.ADDED);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionWhenCopyAndModifyRemovalStateInputProductItemNull() {
        ProductItem.copyAndModify(null, RemovalState.NOT_REMOVED);
    }
}
