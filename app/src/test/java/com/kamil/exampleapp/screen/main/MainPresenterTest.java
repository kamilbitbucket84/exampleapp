package com.kamil.exampleapp.screen.main;

import com.kamil.exampleapp.screen.main.model.ExampleHierarchyTree;
import com.kamil.exampleapp.screen.main.model.MainModel;
import com.kamil.exampleapp.screen.main.presenter.MainPresenter;
import com.kamil.exampleapp.screen.main.view.MainMvpView;
import com.kamil.exampleapp.screen.main.model.HierarchyTree;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import rx.Observable;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class MainPresenterTest {

    private final List<HierarchyTree> EMPTY_LIST = new ArrayList<>();
    private final List<HierarchyTree> NOT_EMPTY_LIST
            = Arrays.asList(new ExampleHierarchyTree(new ArrayList<>()), new ExampleHierarchyTree(new ArrayList<>()));

    private MainPresenter mainPresenter;
    @Mock private MainModel mainModel;
    @Mock private MainMvpView mainMvpView;

    @Before
    public void setup() {
        mainPresenter = new MainPresenter(mainModel);
        mainPresenter.attachView(mainMvpView);
    }

    @After
    public void tearDown() {
        mainPresenter.detachView();
    }

    @Test
    public void shouldCallOnGetHierarchyTreeStartedOnScreenCreated() {
        when(mainModel.getHierarchyTrees()).thenReturn(Observable.just(EMPTY_LIST));

        mainPresenter.onScreenCreated();

        verify(mainMvpView).onGetHierarchyTreesStarted();
    }

    @Test
    public void shouldCallOnGetHierarchyTreeSuccessEmptyWhenModelReceiveEmptyHierarchies() {
        when(mainModel.getHierarchyTrees()).thenReturn(Observable.just(EMPTY_LIST));

        mainPresenter.onScreenCreated();

        verify(mainMvpView).onGetHierarchyTreesSuccessEmpty();
    }

    @Test
    public void shouldCallOnGetHierarchyTreeSuccessEmptyWhenModelReceiveNullHierarchies() {
        when(mainModel.getHierarchyTrees()).thenReturn(Observable.just(null));

        mainPresenter.onScreenCreated();

        verify(mainMvpView).onGetHierarchyTreesSuccessEmpty();
    }

    @Test
    public void shouldCallOnGetHierarchyTreeSuccessNotEmptyWhenModelReceiveNotEmptyHierarchies() {
        when(mainModel.getHierarchyTrees()).thenReturn(Observable.just(NOT_EMPTY_LIST));

        mainPresenter.onScreenCreated();

        verify(mainMvpView).onGetHierarchyTreesSuccessNotEmpty(NOT_EMPTY_LIST);
    }

    @Test
    public void shouldCallOnGetHierarchyTreeErrorWhenModelErrorReceivingHierarchies() {
        when(mainModel.getHierarchyTrees()).thenReturn(Observable.error(new Exception()));

        mainPresenter.onScreenCreated();

        verify(mainMvpView).onGetHierarchyTreesError();
    }
}
