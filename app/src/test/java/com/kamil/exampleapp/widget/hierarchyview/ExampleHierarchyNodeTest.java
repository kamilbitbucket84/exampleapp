package com.kamil.exampleapp.widget.hierarchyview;

import com.kamil.exampleapp.screen.main.model.ExampleHierarchyNode;
import com.kamil.exampleapp.screen.main.model.HierarchyNode;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class ExampleHierarchyNodeTest {

    @Test
    public void shouldGetParentNodeReturnProperParentAfterConnection() {
        ExampleHierarchyNode root = new ExampleHierarchyNode(1L, "TEST", null);
        ExampleHierarchyNode child = new ExampleHierarchyNode(2L, "CHILD", 1L);

        child.connectParentNode(root);

        assertTrue(child.getParentNode() == root);
    }

    @Test
    public void shouldGetPreviousNodesReturnProperNumberOfParents() {
        ExampleHierarchyNode root = new ExampleHierarchyNode(1L, "ROOT_CLOTHES", null);
        ExampleHierarchyNode category = new ExampleHierarchyNode(2L, "CATEGORY", 1L);
        ExampleHierarchyNode subcategory = new ExampleHierarchyNode(3L, "SUBCATEGORY", 2L);
        ExampleHierarchyNode subsubcategory = new ExampleHierarchyNode(4L, "SUBSUBCATEGORY", 3L);
        ExampleHierarchyNode leaf = new ExampleHierarchyNode(5L, "LEAF", 4L);

        leaf.connectParentNode(subsubcategory);
        subsubcategory.connectParentNode(subcategory);
        subcategory.connectParentNode(category);
        category.connectParentNode(root);
        List<HierarchyNode> allLeafPreviousNodes = leaf.getPreviousNodes();

        assertTrue(allLeafPreviousNodes.size() == 4);
    }

    @Test
    public void shouldGetPreviousNodesReturnProperlyOrderedParents() {
        ExampleHierarchyNode root = new ExampleHierarchyNode(1L, "ROOT_CLOTHES", null);
        ExampleHierarchyNode category = new ExampleHierarchyNode(2L, "CATEGORY", 1L);
        ExampleHierarchyNode subcategory = new ExampleHierarchyNode(3L, "SUBCATEGORY", 2L);
        ExampleHierarchyNode subsubcategory = new ExampleHierarchyNode(4L, "SUBSUBCATEGORY", 3L);
        ExampleHierarchyNode leaf = new ExampleHierarchyNode(5L, "LEAF", 4L);

        leaf.connectParentNode(subsubcategory);
        subsubcategory.connectParentNode(subcategory);
        subcategory.connectParentNode(category);
        category.connectParentNode(root);

        List<HierarchyNode> allLeafPreviousNodes = leaf.getPreviousNodes();
        assertTrue(allLeafPreviousNodes.get(0) == root);
        assertTrue(allLeafPreviousNodes.get(1) == category);
        assertTrue(allLeafPreviousNodes.get(2) == subcategory);
        assertTrue(allLeafPreviousNodes.get(3) == subsubcategory);
    }


    @Test
    public void shouldGetPreviousNodesReturnEmptyListWhenNothingConnected() {
        ExampleHierarchyNode leaf = new ExampleHierarchyNode(5L, "LEAF", 4L);

        List<HierarchyNode> allLeafPreviousNodes = leaf.getPreviousNodes();

        assertTrue(allLeafPreviousNodes.isEmpty());
    }

    @Test
    public void shouldGetChildrenReturnProperlySizedList() {
        ExampleHierarchyNode root = new ExampleHierarchyNode(1L, "PARENT", null);
        ExampleHierarchyNode childA = new ExampleHierarchyNode(2L, "CHILD_A", 1L);
        ExampleHierarchyNode childB = new ExampleHierarchyNode(3L, "CHILD_B", 1L);
        ExampleHierarchyNode childC = new ExampleHierarchyNode(4L, "CHILD_C", 1L);

        root.addChild(childA);
        root.addChild(childB);
        root.addChild(childC);

        assertTrue(root.getChildren().size() == 3);
    }

    @Test
    public void shouldGetChildrenReturnEmptyListWhenNothingAdded() {
        ExampleHierarchyNode root = new ExampleHierarchyNode(1L, "PARENT", null);

        assertTrue(root.getChildren().isEmpty());
    }


    @Test
    public void shouldNodeHasChildrenReturnFalseWhenNoChildrenAdded() {
        ExampleHierarchyNode root = new ExampleHierarchyNode(1L, "PARENT", null);

        assertTrue(!root.hasChildren());
    }

    @Test
    public void shouldNodeHasChildrenReturnTrueWhenChildrenAdded() {
        ExampleHierarchyNode root = new ExampleHierarchyNode(1L, "PARENT", null);
        ExampleHierarchyNode childA = new ExampleHierarchyNode(2L, "CHILD_A", 1L);

        root.addChild(childA);

        assertTrue(root.hasChildren());
    }

    @Test
    public void shouldisRootReturnTrueWhenParentNotConnected() {
        ExampleHierarchyNode root = new ExampleHierarchyNode(1L, "PARENT", null);

        assertTrue(root.isRoot());
    }

    @Test
    public void shouldisRootReturnFalseWhenParentConnected() {
        ExampleHierarchyNode root = new ExampleHierarchyNode(1L, "PARENT", null);
        ExampleHierarchyNode child = new ExampleHierarchyNode(2L, "CHILD", 1L);

        child.connectParentNode(root);

        assertTrue(!child.isRoot());
    }

}
