package com.kamil.exampleapp.widget.hierarchyview;

import com.kamil.exampleapp.screen.main.model.HierarchyTreeInputValidator;
import com.kamil.exampleapp.testdata.HierarchyTreeInputValidatorTestData;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;

import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class HierarchyTreeInputValidatorTest {

    private HierarchyTreeInputValidator validator;
    private HierarchyTreeInputValidatorTestData testData;

    @Before
    public void setUp() {
        validator = new HierarchyTreeInputValidator();
        testData = new HierarchyTreeInputValidatorTestData();
    }

    @Test
    public void shouldReturnFalseWhenInputNodesNull() {
        assertTrue(!validator.isInputValid(null));
    }

    @Test
    public void shouldReturnFalseWhenInputNodesAreEmpty() {
        assertTrue(!validator.isInputValid(new ArrayList<>()));
    }

    @Test
    public void shouldReturnFalseWhenInputHasNoRootNode() {
        assertTrue(!validator.isInputValid(testData.getErrorNodesNoRoot()));
    }

    @Test
    public void shouldReturnFalseWhenInputHasMoreThanOneRootNode() {
        assertTrue(!validator.isInputValid(testData.getErrorNodesTwoRoots()));
    }

    @Test
    public void shouldReturnFalseWhenNotAllNodesConnected() {
        assertTrue(!validator.isInputValid(testData.getErrorNodesNotAllConnected()));
    }

    @Test
    public void shouldReturnTrueWhenInputHasOnlyOneRootNode() {
        assertTrue(validator.isInputValid(testData.getCorrectNodesOnlyOneRoot()));
    }

    @Test
    public void shouldReturnTrueWhenAllNodesConnected() {
        assertTrue(validator.isInputValid(testData.getCorrectNodesAllConnected()));
    }

    @Test
    public void shouldReturnFalseWhenNodesIdRepeated() {
        assertTrue(!validator.isInputValid(testData.getErrorNodesOneIdRepeated()));
    }
}
