package com.kamil.exampleapp.widget.hierarchyview;

import com.kamil.exampleapp.screen.main.model.ExampleHierarchyTree;
import com.kamil.exampleapp.testdata.ExampleHierarchyTreeTestData;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;


@RunWith(JUnit4.class)
public class ExampleHierarchyTreeTest {

    private ExampleHierarchyTreeTestData testData;

    @Before
    public void setUp() {
        testData = new ExampleHierarchyTreeTestData();
    }

    @Test
    public void shouldSwitchToNodeReturnFalseWhenNodeNotInTree() {
        ExampleHierarchyTree tree = new ExampleHierarchyTree(testData.clothNodes());

        boolean switchResult = tree.switchToNode(testData.intruderNode());

        assertTrue(!switchResult);
    }

    @Test
    public void shouldReturnProperRootNode() {
        ExampleHierarchyTree tree = new ExampleHierarchyTree(testData.clothNodes());

        assertTrue(tree.getRootNode() == testData.rootClothes());
    }


    @Test
    public void shouldSwitchToNodeReturnTrueWhenNodeInTree() {
        ExampleHierarchyTree tree = new ExampleHierarchyTree(testData.clothNodes());

        tree.expandRoot();
        boolean switchResult = tree.switchToNode(testData.forWomen());

        assertTrue(switchResult);
    }

    @Test
    public void shouldSwitchToNodeJumpToProperNodes() {
        ExampleHierarchyTree tree = new ExampleHierarchyTree(testData.clothNodes());

        tree.expandRoot();

        tree.switchToNode(testData.skirts());
            assertTrue(tree.getCurrentNode() == testData.skirts());
        tree.switchToNode(testData.rootClothes());
            assertTrue(tree.getCurrentNode() == testData.rootClothes());
        tree.switchToNode(testData.forWomen());
            assertTrue(tree.getCurrentNode() == testData.forWomen());
        tree.switchToNode(testData.skirts());
            assertTrue(tree.getCurrentNode() == testData.skirts());
        tree.switchToNode(testData.longSkirts());
            assertTrue(tree.getCurrentNode() == testData.longSkirts());
        tree.switchToNode(testData.rootClothes());
            assertTrue(tree.getCurrentNode() == testData.rootClothes());
        tree.collapseRoot();
        tree.expandRoot();
        tree.switchToNode(testData.forWomen());
            assertTrue(tree.getCurrentNode() == testData.forWomen());
    }

    @Test
    public void shouldIsRootExpandedReturnTrueWhenExpandRootCalled() {
        ExampleHierarchyTree tree = new ExampleHierarchyTree(testData.clothNodes());

        tree.expandRoot();

        assertTrue(tree.isRootExpanded());
    }

    @Test
    public void shouldIsRootExpandedReturnFalseWhenExpandRootNotCalled() {
        ExampleHierarchyTree tree = new ExampleHierarchyTree(testData.clothNodes());

        assertTrue(!tree.isRootExpanded());
    }

    @Test
    public void shouldIsRootExpandedReturnFalseWhenCollapseRootCalled() {
        ExampleHierarchyTree tree = new ExampleHierarchyTree(testData.clothNodes());

        tree.collapseRoot();

        assertTrue(!tree.isRootExpanded());
    }

    @Test
    public void shouldSwitchToNodeAvailableWhenExpandRootCalled() {
        ExampleHierarchyTree tree = new ExampleHierarchyTree(testData.clothNodes());

        tree.expandRoot();
        boolean switchResult = tree.switchToNode(testData.forWomen());

        assertTrue(switchResult);
        assertTrue(tree.getCurrentNode() == testData.forWomen());
    }

    @Test
    public void shouldGetCurrentNodeReturnRootNodeWhenTreeCreated() {
        ExampleHierarchyTree tree = new ExampleHierarchyTree(testData.clothNodes());

        assertTrue(tree.getCurrentNode() == testData.rootClothes());
    }

    @Test
    public void shouldIsRootExpandedReturnFalseWhenTreeCreated() {
        ExampleHierarchyTree tree = new ExampleHierarchyTree(testData.clothNodes());

        assertTrue(!tree.isRootExpanded());
    }

    @Test
    public void shouldGetAllNodesReturnProperNumberOfNodes() {
        ExampleHierarchyTree tree = new ExampleHierarchyTree(testData.clothNodes());

        assertTrue(testData.clothNodes().size() == tree.getAllNodes().size());
    }

    @Test
    public void shouldGetAllNodesReturnNotEmptyListWhenTreeIsValid() {
        ExampleHierarchyTree tree = new ExampleHierarchyTree(testData.clothNodes());

        assertTrue(!tree.getAllNodes().isEmpty());
    }

    @Test
    public void shouldGetAllNodesReturnEmptyListWhenTreeIsNotValid() {
        ExampleHierarchyTree tree = new ExampleHierarchyTree(testData.badNodes());

        assertTrue(tree.getAllNodes().isEmpty());
    }

}
