package com.kamil.exampleapp.widget.hierarchyview;


import com.kamil.exampleapp.screen.main.model.HierarchyNode;
import com.kamil.exampleapp.screen.main.model.HierarchyTreeNodeConnector;
import com.kamil.exampleapp.testdata.ExampleHierarchyTreeTestData;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class HierarchyTreeNodeConnectorTest {

    private HierarchyTreeNodeConnector connector;
    private ExampleHierarchyTreeTestData testData;
    private HierarchyNode rootNode;

    @Before
    public void setUp() {
        connector = new HierarchyTreeNodeConnector();
        testData = new ExampleHierarchyTreeTestData();
        rootNode = connector.findRootNode(testData.clothNodes());
    }

    @Test
    public void shouldFindProperRootNode() {
        HierarchyNode rootNode = connector.findRootNode(testData.clothNodes());
        assertTrue(rootNode == testData.rootClothes());
    }

    @Test
    public void shouldConnectProperNumberOfChildren() {
        connector.connectAllNodes(rootNode, testData.clothNodes());

        List<HierarchyNode> clothesChildren = testData.rootClothes().getChildren();
        List<HierarchyNode> forMenChildren = testData.forMen().getChildren();
        List<HierarchyNode> forWomenChildren = testData.forWomen().getChildren();
        List<HierarchyNode> skirtsChildren = testData.skirts().getChildren();
        List<HierarchyNode> glovesChildren = testData.gloves().getChildren();

        assertTrue(clothesChildren.size() == 2);
        assertTrue(forMenChildren.size() == 2);
        assertTrue(forWomenChildren.size() == 2);
        assertTrue(skirtsChildren.size() == 4);
        assertTrue(glovesChildren.size() == 2);
    }

    @Test
    public void shouldHaveProperlyConnectedChildren() {
        connector.connectAllNodes(rootNode, testData.clothNodes());

        List<HierarchyNode> clothesChildren = testData.rootClothes().getChildren();
        List<HierarchyNode> forMenChildren = testData.forMen().getChildren();
        List<HierarchyNode> forWomenChildren = testData.forWomen().getChildren();
        List<HierarchyNode> skirtsChildren = testData.skirts().getChildren();
        List<HierarchyNode> glovesChildren = testData.gloves().getChildren();

        assertTrue(clothesChildren.contains(testData.forMen()));
        assertTrue(clothesChildren.contains(testData.forWomen()));
        assertTrue(forMenChildren.contains(testData.belts()));
        assertTrue(forMenChildren.contains(testData.gloves()));
        assertTrue(forWomenChildren.contains(testData.skirts()));
        assertTrue(forWomenChildren.contains(testData.dresses()));
        assertTrue(skirtsChildren.contains(testData.miniSkirts()));
        assertTrue(skirtsChildren.contains(testData.mediumSkirts()));
        assertTrue(skirtsChildren.contains(testData.longSkirts()));
        assertTrue(skirtsChildren.contains(testData.ultraLongSkirts()));
        assertTrue(glovesChildren.contains(testData.leatherGloves()));
        assertTrue(glovesChildren.contains(testData.cottonGloves()));
    }

    @Test
    public void shouldHaveProperlyConnectedParents() {
        connector.connectAllNodes(rootNode, testData.clothNodes());

        assertTrue(testData.rootClothes().getParentNode() == null);
        assertTrue(testData.forMen().getParentNode() == testData.rootClothes());
        assertTrue(testData.forWomen().getParentNode() == testData.rootClothes());
        assertTrue(testData.skirts().getParentNode() == testData.forWomen());
        assertTrue(testData.dresses().getParentNode() == testData.forWomen());
        assertTrue(testData.belts().getParentNode() == testData.forMen());
        assertTrue(testData.gloves().getParentNode() == testData.forMen());
        assertTrue(testData.miniSkirts().getParentNode() == testData.skirts());
        assertTrue(testData.mediumSkirts().getParentNode() == testData.skirts());
        assertTrue(testData.longSkirts().getParentNode() == testData.skirts());
        assertTrue(testData.ultraLongSkirts().getParentNode() == testData.skirts());
        assertTrue(testData.leatherGloves().getParentNode() == testData.gloves());
        assertTrue(testData.cottonGloves().getParentNode() == testData.gloves());
    }

    @Test
    public void shouldHaveProperLeafNodes() {
        connector.connectAllNodes(rootNode, testData.clothNodes());

        assertTrue(!testData.dresses().hasChildren());
        assertTrue(!testData.belts().hasChildren());
        assertTrue(!testData.miniSkirts().hasChildren());
        assertTrue(!testData.mediumSkirts().hasChildren());
        assertTrue(!testData.longSkirts().hasChildren());
        assertTrue(!testData.ultraLongSkirts().hasChildren());
        assertTrue(!testData.leatherGloves().hasChildren());
        assertTrue(!testData.cottonGloves().hasChildren());
    }

    @Test
    public void shouldAllAddedParentNodesHasChildren() {
        connector.connectAllNodes(rootNode, testData.clothNodes());

        assertTrue(testData.rootClothes().hasChildren());
        assertTrue(testData.forMen().hasChildren());
        assertTrue(testData.forWomen().hasChildren());
        assertTrue(testData.skirts().hasChildren());
        assertTrue(testData.gloves().hasChildren());
    }
}
