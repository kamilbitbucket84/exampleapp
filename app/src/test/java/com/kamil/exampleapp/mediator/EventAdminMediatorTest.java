package com.kamil.exampleapp.mediator;

import com.kamil.exampleapp.service.event.eventtype.TimeEvent;
import com.kamil.exampleapp.service.event.launcher.ProximityEventLaunchParams;
import com.kamil.exampleapp.service.event.launcher.TimeEventLaunchParams;
import com.kamil.exampleapp.service.event.mediator.EventAdminMediator;
import com.kamil.exampleapp.service.event.mediator.EventGenerator;
import com.kamil.exampleapp.service.event.mediator.EventMediator;
import com.kamil.exampleapp.service.event.mediator.EventReceiver;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
public class EventAdminMediatorTest {

    @Mock private EventGenerator eventGenerator;
    @Mock private EventReceiver eventReceiver;
    private EventMediator coreEventMediator;
    private TimeEventLaunchParams timeEventLaunchParams;
    private ProximityEventLaunchParams proximityEventLaunchParams;
    private TimeEvent timeEvent;

    @Before
    public void setUp() {
        coreEventMediator = new EventAdminMediator();
        timeEventLaunchParams = new TimeEventLaunchParams(0, 1, TimeUnit.SECONDS);
        timeEvent = new TimeEvent(System.currentTimeMillis());
        proximityEventLaunchParams = new ProximityEventLaunchParams(2);
    }

    private void connectAll() {
        coreEventMediator.connectEventGenerator(eventGenerator);
        coreEventMediator.connectEventReceiver(eventReceiver);
    }

    private void connectGenerator() {
        coreEventMediator.connectEventGenerator(eventGenerator);
    }

    private void connectReceiver() {
        coreEventMediator.connectEventReceiver(eventReceiver);
    }

    @Test
    public void shouldNotStartTimeEventLauncherWhenNobodyNotConnected() {
        assertFalse(coreEventMediator.startTimeEventsLauncher(timeEventLaunchParams));
        verify(eventGenerator, never()).startTimeEventsLauncher(timeEventLaunchParams);
    }

    @Test
    public void shouldNotStartTimeEventLauncherWhenOnlyGeneratorConnected() {
        connectGenerator();
        assertFalse(coreEventMediator.startTimeEventsLauncher(timeEventLaunchParams));
        verify(eventGenerator, never()).startTimeEventsLauncher(timeEventLaunchParams);
    }

    @Test
    public void shouldNotStartTimeEventLauncherWhenOnlyReceiverConnected() {
        connectReceiver();
        assertFalse(coreEventMediator.startTimeEventsLauncher(timeEventLaunchParams));
        verify(eventGenerator, never()).startTimeEventsLauncher(timeEventLaunchParams);
    }

    @Test
    public void shouldStartTimeEventLauncherWhenAllConnected() {
        connectAll();
        assertTrue(coreEventMediator.startTimeEventsLauncher(timeEventLaunchParams));
        verify(eventGenerator).startTimeEventsLauncher(timeEventLaunchParams);
    }

    @Test
    public void shouldNotStopTimeEventLauncherWhenNobodyConnected() {
        assertFalse(coreEventMediator.stopTimeEventsLauncher());
        verify(eventGenerator, never()).stopTimeEventsLauncher();
    }

    @Test
    public void shouldNotStopTimeEventLauncherWhenOnlyGeneratorConnected() {
        connectGenerator();
        assertFalse(coreEventMediator.stopTimeEventsLauncher());
        verify(eventGenerator, never()).stopTimeEventsLauncher();
    }

    @Test
    public void shouldNotStopTimeEventLauncherWhenOnlyReceiverConnected() {
        connectReceiver();
        assertFalse(coreEventMediator.stopTimeEventsLauncher());
        verify(eventGenerator, never()).stopTimeEventsLauncher();
    }

    @Test
    public void shouldStopTimeEventLauncherWhenAllConnected() {
        connectAll();
        assertTrue(coreEventMediator.stopTimeEventsLauncher());
        verify(eventGenerator).stopTimeEventsLauncher();
    }

    @Test
    public void shouldNotStartProximityEventLauncherWhenNobodyConnected() {
        assertFalse(coreEventMediator.startProximityEventsLauncher(proximityEventLaunchParams));
        verify(eventGenerator, never()).startProximityEventsLauncher(proximityEventLaunchParams);
    }

    @Test
    public void shouldNotStartProximityEventLauncherWhenOnlyGeneratorConnected() {
        connectGenerator();
        assertFalse(coreEventMediator.startProximityEventsLauncher(proximityEventLaunchParams));
        verify(eventGenerator, never()).startProximityEventsLauncher(proximityEventLaunchParams);
    }

    @Test
    public void shouldNotStartProximityEventLauncherWhenOnlyReceiverConnected() {
        connectReceiver();
        assertFalse(coreEventMediator.startProximityEventsLauncher(proximityEventLaunchParams));
        verify(eventGenerator, never()).startProximityEventsLauncher(proximityEventLaunchParams);
    }

    @Test
    public void shouldStartProximityEventLauncherWhenAllConnected() {
        connectAll();
        assertTrue(coreEventMediator.startProximityEventsLauncher(proximityEventLaunchParams));
        verify(eventGenerator).startProximityEventsLauncher(proximityEventLaunchParams);
    }

    @Test
    public void shouldNotStopProximityEventLauncherWhenNobodyConnected() {
        assertFalse(coreEventMediator.stopProximityEventsLauncher());
        verify(eventGenerator, never()).stopProximityEventsLauncher();
    }

    @Test
    public void shouldNotStopProximityEventLauncherWhenOnlyReceiverConnected() {
        connectReceiver();
        assertFalse(coreEventMediator.stopProximityEventsLauncher());
        verify(eventGenerator, never()).stopProximityEventsLauncher();
    }

    @Test
    public void shouldNotStopProximityEventLauncherWhenOnlyGeneratorConnected() {
        connectGenerator();
        assertFalse(coreEventMediator.stopProximityEventsLauncher());
        verify(eventGenerator, never()).stopProximityEventsLauncher();
    }

    @Test
    public void shouldStopProximityEventLauncherWhenAllConnected() {
        connectAll();
        assertTrue(coreEventMediator.stopProximityEventsLauncher());
        verify(eventGenerator).stopProximityEventsLauncher();
    }

    @Test
    public void shouldNotLaunchEventWhenNobodyConnected() {
        assertFalse(coreEventMediator.launchEvent(timeEvent));
        verify(eventReceiver, never()).onEventReceived(timeEvent);
    }

    @Test
    public void shouldNotLaunchEventWhenOnlyGeneratorConnected() {
        connectGenerator();
        assertFalse(coreEventMediator.launchEvent(timeEvent));
        verify(eventReceiver, never()).onEventReceived(timeEvent);
    }

    @Test
    public void shouldNotLaunchEventWhenOnlyreceiverConnected() {
        connectReceiver();
        assertFalse(coreEventMediator.launchEvent(timeEvent));
        verify(eventReceiver, never()).onEventReceived(timeEvent);
    }

    @Test
    public void shouldLaunchEventWhenAllConnected() {
        connectAll();
        assertTrue(coreEventMediator.launchEvent(timeEvent));
        verify(eventReceiver).onEventReceived(timeEvent);
    }
}
