package com.kamil.exampleapp.datasource;

import com.kamil.exampleapp.datasource.prefs.SettingsClient;
import com.kamil.exampleapp.datasource.prefs.SettingsInPrefsClient;
import com.kamil.exampleapp.service.event.launcher.EventLaunchParams;
import com.kamil.exampleapp.service.event.launcher.ProximityEventLaunchParams;
import com.kamil.exampleapp.service.event.launcher.TimeEventLaunchParams;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;


import rx.Observable;
import rx.observers.TestSubscriber;

@RunWith(RobolectricTestRunner.class)
@Config(manifest = "app/src/main/AndroidManifest.xml")
public class SettingsInPrefsClientTest {

    private SettingsClient settingsClient;
    private TimeEventLaunchParams timeEventLaunchParams;
    private ProximityEventLaunchParams proximityEventLaunchParams;

    private boolean testTimeLauncherWorking = true;
    private boolean testProximityLauncherWorking = true;
    private int testTimeEventDelay = 0;
    private int testTimeEventPeriod = 5;
    private TimeUnit testTimeEventTimeUnit = TimeUnit.SECONDS;
    private int testProximityEventNumberToLaunch = 4;

    @Before
    public void setUp() {
        settingsClient = new SettingsInPrefsClient(RuntimeEnvironment.application);
        timeEventLaunchParams
                = new TimeEventLaunchParams(testTimeEventDelay, testTimeEventPeriod, testTimeEventTimeUnit);
        proximityEventLaunchParams
                = new ProximityEventLaunchParams(testProximityEventNumberToLaunch);
    }

    private void saveExampleEventLaunchParams() {
        TestSubscriber<Boolean> saveTimeLauncherStateSubscriber = new TestSubscriber<>();
        settingsClient.setTimeLauncherOn().subscribe(saveTimeLauncherStateSubscriber);
        saveTimeLauncherStateSubscriber.assertNoErrors();

        TestSubscriber<Boolean> saveProximityLauncherStateSubscriber = new TestSubscriber<>();
        settingsClient.setProximityLauncherOn().subscribe(saveProximityLauncherStateSubscriber);
        saveProximityLauncherStateSubscriber.assertNoErrors();

        TestSubscriber<Boolean> saveTimeEventLaunchParamsSubscriber = new TestSubscriber<>();
        settingsClient.setTimeEventLaunchParams(timeEventLaunchParams).subscribe(saveTimeEventLaunchParamsSubscriber);
        saveTimeEventLaunchParamsSubscriber.assertNoErrors();

        TestSubscriber<Boolean> saveProximityEventLaunchParamsSubscriber = new TestSubscriber<>();
        settingsClient.setProximityEventLaunchParams(proximityEventLaunchParams).subscribe(saveProximityEventLaunchParamsSubscriber);
        saveProximityEventLaunchParamsSubscriber.assertNoErrors();
    }

    private EventLaunchParams loadEventLaunchParams() {
        TestSubscriber<EventLaunchParams> testSubscriber = new TestSubscriber<>();
        settingsClient.loadEventLaunchParams().subscribe(testSubscriber);
        testSubscriber.assertNoErrors();
        return testSubscriber.getOnNextEvents().get(0);
    }

    @Test
    public void shouldLoadEventLaunchParamsProperly() {
        saveExampleEventLaunchParams();

        EventLaunchParams eventLaunchParams = loadEventLaunchParams();

        assertTrue(eventLaunchParams.timeEventsLauncherWorking == testTimeLauncherWorking);
        assertTrue(eventLaunchParams.timeEventLaunchParams.delay == timeEventLaunchParams.delay);
        assertTrue(eventLaunchParams.timeEventLaunchParams.eventPeriod == timeEventLaunchParams.eventPeriod);
        assertTrue(eventLaunchParams.timeEventLaunchParams.timeUnit.equals(timeEventLaunchParams.timeUnit));
        assertTrue(eventLaunchParams.proximityEventsLauncherWorking == testProximityLauncherWorking);
        assertTrue(eventLaunchParams.proximityEventLaunchParams.numberOfEventsToLaunch == proximityEventLaunchParams.numberOfEventsToLaunch);
    }

    @Test
    public void shouldSetTimeLauncherOnProperly() {
        TestSubscriber<Boolean> setTimeLauncherOnSubscriber = new TestSubscriber<>();
        settingsClient.setTimeLauncherOn().subscribe(setTimeLauncherOnSubscriber);
        setTimeLauncherOnSubscriber.assertNoErrors();

        EventLaunchParams eventLaunchParams = loadEventLaunchParams();

        assertTrue(eventLaunchParams.timeEventsLauncherWorking);
    }

    @Test
    public void shouldSetTimeLauncherOffProperly() {
        TestSubscriber<Boolean> setTimeLauncherOffSubscriber = new TestSubscriber<>();
        settingsClient.setTimeLauncherOff().subscribe(setTimeLauncherOffSubscriber);
        setTimeLauncherOffSubscriber.assertNoErrors();

        EventLaunchParams eventLaunchParams = loadEventLaunchParams();

        assertTrue(!eventLaunchParams.timeEventsLauncherWorking);
    }

    @Test
    public void shouldSetProximityLauncherOnProperly() {
        TestSubscriber<Boolean> setProximityLauncherOnSubscriber = new TestSubscriber<>();
        settingsClient.setProximityLauncherOn().subscribe(setProximityLauncherOnSubscriber);
        setProximityLauncherOnSubscriber.assertNoErrors();

        EventLaunchParams eventLaunchParams = loadEventLaunchParams();

        assertTrue(eventLaunchParams.proximityEventsLauncherWorking);
    }

    @Test
    public void shouldSetProximityLauncherOffProperly() {
        TestSubscriber<Boolean> setProximityLauncherOffSubscriber = new TestSubscriber<>();
        settingsClient.setProximityLauncherOff().subscribe(setProximityLauncherOffSubscriber);
        setProximityLauncherOffSubscriber.assertNoErrors();

        EventLaunchParams eventLaunchParams = loadEventLaunchParams();

        assertTrue(!eventLaunchParams.proximityEventsLauncherWorking);
    }

    @Test
    public void shouldSetProximityEventLaunchParamsProperly() {
        TestSubscriber<Boolean> testSubscriber = new TestSubscriber<>();
        settingsClient.setProximityEventLaunchParams(proximityEventLaunchParams).subscribe(testSubscriber);
        testSubscriber.assertNoErrors();

        EventLaunchParams eventLaunchParams = loadEventLaunchParams();

        assertTrue(eventLaunchParams.proximityEventLaunchParams.numberOfEventsToLaunch
                == proximityEventLaunchParams.numberOfEventsToLaunch);
    }

    @Test
    public void shouldSetTimeEventLaunchParamsProperly() {
        TestSubscriber<Boolean> testSubscriber = new TestSubscriber<>();
        settingsClient.setTimeEventLaunchParams(timeEventLaunchParams).subscribe(testSubscriber);
        testSubscriber.assertNoErrors();

        EventLaunchParams eventLaunchParams = loadEventLaunchParams();

        assertTrue(eventLaunchParams.timeEventLaunchParams.delay == timeEventLaunchParams.delay);
        assertTrue(eventLaunchParams.timeEventLaunchParams.eventPeriod == timeEventLaunchParams.eventPeriod);
        assertTrue(eventLaunchParams.timeEventLaunchParams.timeUnit == timeEventLaunchParams.timeUnit);
    }

    @After
    public void tearDown() {
        Observable<Boolean> goBackToDefaultSettings = settingsClient.goBackToDefaultEventSettings();
        goBackToDefaultSettings.toBlocking().first();
        goBackToDefaultSettings.subscribe();
    }
}
