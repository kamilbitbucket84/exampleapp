package com.kamil.exampleapp.datasource;

import android.database.sqlite.SQLiteConstraintException;

import com.kamil.exampleapp.datasource.database.ExampleSQLiteDatabaseClient;
import com.kamil.exampleapp.datasource.database.model.HierarchyDbEntity;
import com.kamil.exampleapp.datasource.database.model.HierarchyDbEntityRefresh;
import com.kamil.exampleapp.datasource.database.model.ProductDbEntity;
import com.kamil.exampleapp.screen.browseproducts.model.ProductItem;
import com.kamil.exampleapp.testdata.ExampleSqliteDatabaseClientTestData;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.List;

import rx.Observable;
import rx.observers.TestSubscriber;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
@Config(manifest = "app/src/main/AndroidManifest.xml")
public class ExampleSqliteDatabaseClientTest {

    private ExampleSQLiteDatabaseClient dbClient;
    private ExampleSqliteDatabaseClientTestData testData;

    @Before
    public void setUp() {
        dbClient = new ExampleSQLiteDatabaseClient(RuntimeEnvironment.application);
        testData = new ExampleSqliteDatabaseClientTestData();
    }

    private void insertExampleHierarchyDbEntities() {
        Observable<HierarchyDbEntityRefresh> refreshHierarchyDbEntities
                = dbClient.refreshDbHierarchies(testData.clothesHierarchyTrees);
        refreshHierarchyDbEntities.toBlocking().first();
        refreshHierarchyDbEntities.subscribe();
    }

    private void insertExampleProductItems() {
        TestSubscriber<ProductItem> insertFirstSubscriber = new TestSubscriber<>();
        dbClient.insertProductItemWithAssignedHierarchies(testData.firstProductItem).subscribe(insertFirstSubscriber);
        insertFirstSubscriber.assertNoErrors();

        TestSubscriber<ProductItem> insertSecondSubscriber = new TestSubscriber<>();
        dbClient.insertProductItemWithAssignedHierarchies(testData.secondProductItem).subscribe(insertSecondSubscriber);
        insertSecondSubscriber.assertNoErrors();

        TestSubscriber<ProductItem> insertThirdSubscriber = new TestSubscriber<>();
        dbClient.insertProductItemWithAssignedHierarchies(testData.thirdProductItem).subscribe(insertThirdSubscriber);
        insertThirdSubscriber.assertNoErrors();
    }

    @Test
    public void shouldSelectProperNumberOfHierarchyDbEntities() {
        insertExampleHierarchyDbEntities();

        TestSubscriber<List<HierarchyDbEntity>> selectAllSubscriber = new TestSubscriber<>();
        dbClient.selectAllHierarchyDbEntities().subscribe(selectAllSubscriber);
        selectAllSubscriber.assertNoErrors();
        List<HierarchyDbEntity> hierarchyDbEntities = selectAllSubscriber.getOnNextEvents().get(0);

        assertTrue(hierarchyDbEntities.size() == testData.clothesNodes.size());
    }


    @Test
    public void shouldDeleteAllHierarchyDbEntities() {
        insertExampleHierarchyDbEntities();

        TestSubscriber<Integer> deleteAllSubscriber = new TestSubscriber<>();
        dbClient.deleteAllHierarchyDbEntities().subscribe(deleteAllSubscriber);
        deleteAllSubscriber.assertNoErrors();

        TestSubscriber<List<HierarchyDbEntity>> selectAllSubscriber = new TestSubscriber<>();
        dbClient.selectAllHierarchyDbEntities().subscribe(selectAllSubscriber);
        selectAllSubscriber.assertNoErrors();
        List<HierarchyDbEntity> hierarchyDbEntities = selectAllSubscriber.getOnNextEvents().get(0);

        assertTrue(hierarchyDbEntities.isEmpty());
    }

    @Test
    public void shouldInsertProperNumberOfProductDbEntities() {
        insertExampleHierarchyDbEntities();
        insertExampleProductItems();

        TestSubscriber<List<ProductDbEntity>> selectProductsSubscriber = new TestSubscriber<>();
        dbClient.selectAllProductDbEntities().subscribe(selectProductsSubscriber);
        selectProductsSubscriber.assertNoErrors();
        List<ProductDbEntity> insertedProductDbEntities = selectProductsSubscriber.getOnNextEvents().get(0);

        assertTrue(insertedProductDbEntities.size() == 3);
    }

    @Test
    public void shouldNotInsertProductDbEntityWithSameId() {
        insertExampleHierarchyDbEntities();

        TestSubscriber<ProductItem> insertSubscriber = new TestSubscriber<>();
        dbClient.insertProductItemWithAssignedHierarchies(testData.firstProductItem).subscribe(insertSubscriber);
        insertSubscriber.assertNoErrors();

        TestSubscriber<ProductItem> insertBadSubscriber = new TestSubscriber<>();
        dbClient.insertProductItemWithAssignedHierarchies(testData.firstProductItem).subscribe(insertBadSubscriber);
        insertBadSubscriber.assertError(SQLiteConstraintException.class);
    }

    @Test
    public void shouldSelectProperNumberOfProductItemsFromGivenHierarchyId() {
        insertExampleHierarchyDbEntities();
        insertExampleProductItems();

        TestSubscriber<List<ProductItem>> selectClothesSubcriber = new TestSubscriber<>();
        dbClient.selectProductItemsWithHierarchyId(testData.rootClothes.getId()).subscribe(selectClothesSubcriber);
        selectClothesSubcriber.assertNoErrors();
        List<ProductItem> rootClothesItems = selectClothesSubcriber.getOnNextEvents().get(0);
        assertTrue(rootClothesItems.size() == 3);

        TestSubscriber<List<ProductItem>> selectForWomenSubscriber = new TestSubscriber<>();
        dbClient.selectProductItemsWithHierarchyId(testData.forWomen.getId()).subscribe(selectForWomenSubscriber);
        selectForWomenSubscriber.assertNoErrors();
        List<ProductItem> forWomenItems = selectForWomenSubscriber.getOnNextEvents().get(0);
        assertTrue(forWomenItems.size() == 2);

        TestSubscriber<List<ProductItem>> selectSkirtsSubscriber = new TestSubscriber<>();
        dbClient.selectProductItemsWithHierarchyId(testData.skirts.getId()).subscribe(selectSkirtsSubscriber);
        selectSkirtsSubscriber.assertNoErrors();
        List<ProductItem> skirtItems = selectSkirtsSubscriber.getOnNextEvents().get(0);
        assertTrue(skirtItems.size() == 2);

        TestSubscriber<List<ProductItem>> selectMiniSkirtsSubscriber = new TestSubscriber<>();
        dbClient.selectProductItemsWithHierarchyId(testData.miniSkirts.getId()).subscribe(selectMiniSkirtsSubscriber);
        selectMiniSkirtsSubscriber.assertNoErrors();
        List<ProductItem> miniSkirtsItems = selectMiniSkirtsSubscriber.getOnNextEvents().get(0);
        assertTrue(miniSkirtsItems.size() == 2);

        TestSubscriber<List<ProductItem>> selectForMenSubscriber = new TestSubscriber<>();
        dbClient.selectProductItemsWithHierarchyId(testData.forMen.getId()).subscribe(selectForMenSubscriber);
        selectForMenSubscriber.assertNoErrors();
        List<ProductItem> forMenItems = selectForMenSubscriber.getOnNextEvents().get(0);
        assertTrue(forMenItems.size() == 1);

        TestSubscriber<List<ProductItem>> selectGlovesSubscriber = new TestSubscriber<>();
        dbClient.selectProductItemsWithHierarchyId(testData.forMen.getId()).subscribe(selectGlovesSubscriber);
        selectGlovesSubscriber.assertNoErrors();
        List<ProductItem> gloves = selectGlovesSubscriber.getOnNextEvents().get(0);
        assertTrue(gloves.size() == 1);

        TestSubscriber<List<ProductItem>> selectLeatherGlovesSubscriber = new TestSubscriber<>();
        dbClient.selectProductItemsWithHierarchyId(testData.forMen.getId()).subscribe(selectLeatherGlovesSubscriber);
        selectLeatherGlovesSubscriber.assertNoErrors();
        List<ProductItem> leatherGloves = selectLeatherGlovesSubscriber.getOnNextEvents().get(0);
        assertTrue(leatherGloves.size() == 1);
    }

    @Test
    public void shouldSelectProductItemSameAsInsertedOne() {
        insertExampleHierarchyDbEntities();

        TestSubscriber<ProductItem> insertSubscriber = new TestSubscriber<>();
        dbClient.insertProductItemWithAssignedHierarchies(testData.firstProductItem).subscribe(insertSubscriber);
        insertSubscriber.assertNoErrors();
        ProductItem insertedItem = insertSubscriber.getOnNextEvents().get(0);

        TestSubscriber<List<ProductItem>> selectSubscriber = new TestSubscriber<>();
        dbClient.selectProductItemsWithHierarchyId(testData.firstProductItem.getAssignedHierarchies().get(0))
                .subscribe(selectSubscriber);
        selectSubscriber.assertNoErrors();
        ProductItem selectedItem = selectSubscriber.getOnNextEvents().get(0).get(0);


        assertTrue(insertedItem.getId().equals(selectedItem.getId()));
        assertTrue(insertedItem.getName().equals(selectedItem.getName()));
        assertTrue(insertedItem.getBrand().equals(selectedItem.getBrand()));
        assertTrue(insertedItem.getPrice().equals(selectedItem.getPrice()));
        assertArrayEquals(insertedItem.getAssignedHierarchies().toArray(), selectedItem.getAssignedHierarchies().toArray());
        assertTrue(insertedItem.isValid());
        assertTrue(selectedItem.isValid());
    }

    @Test
    public void shouldRefreshHierarchyDbEntitiesWhenDataChanged() {
        insertExampleHierarchyDbEntities();

        TestSubscriber<HierarchyDbEntityRefresh> refreshTestSubscriber = new TestSubscriber<>();
        dbClient.refreshDbHierarchies(testData.updatedClothesHierarchyTrees).subscribe(refreshTestSubscriber);
        refreshTestSubscriber.assertNoErrors();
        HierarchyDbEntityRefresh refresh = refreshTestSubscriber.getOnNextEvents().get(0);

        assertTrue(refresh.getInsertedEntities().size() == 2);
        assertTrue(refresh.getUpdatedEntities().size() == 2);
    }

    @Test
    public void shouldNotRefreshHierarchyDbEntitiesWhenDataNotChanged() {
        insertExampleHierarchyDbEntities();

        TestSubscriber<HierarchyDbEntityRefresh> refreshTestSubscriber = new TestSubscriber<>();
        dbClient.refreshDbHierarchies(testData.clothesHierarchyTrees).subscribe(refreshTestSubscriber);
        refreshTestSubscriber.assertNoErrors();
        HierarchyDbEntityRefresh refresh = refreshTestSubscriber.getOnNextEvents().get(0);

        assertTrue(refresh.getInsertedEntities().isEmpty());
        assertTrue(refresh.getUpdatedEntities().isEmpty());
    }

    @After
    public void tearDown() {
        Observable<Integer> deleteAllHierarchyDbEntities = dbClient.deleteAllHierarchyDbEntities();
        Observable<Integer> deleteAllProductDbEntities = dbClient.deleteAllProductDbEntities();

        deleteAllHierarchyDbEntities.toBlocking().first();
        deleteAllHierarchyDbEntities.subscribe();

        deleteAllProductDbEntities.toBlocking().first();
        deleteAllProductDbEntities.subscribe();
    }

}
