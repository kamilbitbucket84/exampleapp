package com.kamil.exampleapp.mappers;

import com.kamil.exampleapp.datasource.api.mapper.HierarchiesResponseMapper;
import com.kamil.exampleapp.datasource.api.model.HierarchyNodeEntity;
import com.kamil.exampleapp.datasource.api.model.HierarchyResponseEntity;
import com.kamil.exampleapp.datasource.api.model.HierarchyTreeEntity;
import com.kamil.exampleapp.screen.main.model.HierarchyTree;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class HierarchyResponseEntityMapperTest {

    private HierarchiesResponseMapper mapper;

    @Before
    public void setUp() {
        mapper = new HierarchiesResponseMapper();
    }

    @Test
    public void shouldReturnEmptyHierarchyListWhenResponseNull() {
        List<HierarchyTree> hierarchyTrees = mapper.from(null);
        assertTrue(hierarchyTrees.isEmpty());
    }

    @Test
    public void shouldReturnEmptyHierarchyListWhenResponseHierarchyTreeEntitiesNull() {
        HierarchyResponseEntity responseEntity = new HierarchyResponseEntity();
        responseEntity.treeEntities = null;
        List<HierarchyTree> hierarchyTrees = mapper.from(responseEntity);

        assertTrue(hierarchyTrees.isEmpty());
    }

    @Test
    public void shouldReturnEmptyHierarchyListWhenResponseHierarchyTreeEntitiesEmpty() {
        HierarchyResponseEntity responseEntity = new HierarchyResponseEntity();
        responseEntity.treeEntities = new ArrayList<>();
        List<HierarchyTree> hierarchyTrees = mapper.from(responseEntity);

        assertTrue(hierarchyTrees.isEmpty());
    }

    @Test
    public void shouldReturnNotEmptyHierarchyListWhenResponseHierarchyEntitiesNotEmpty() {
        HierarchyResponseEntity responseEntity = new HierarchyResponseEntity();
        responseEntity.treeEntities = new ArrayList<>();
        HierarchyTreeEntity treeEntity = new HierarchyTreeEntity();
        treeEntity.nodeEntities = new ArrayList<>();
        HierarchyNodeEntity nodeEntity = new HierarchyNodeEntity();
        nodeEntity.id = 1L;
        nodeEntity.name = "test";
        nodeEntity.parentId = null;
        treeEntity.nodeEntities.add(nodeEntity);
        responseEntity.treeEntities.add(treeEntity);

        List<HierarchyTree> hierarchyTrees = mapper.from(responseEntity);

        assertTrue(!hierarchyTrees.isEmpty());
    }


}
