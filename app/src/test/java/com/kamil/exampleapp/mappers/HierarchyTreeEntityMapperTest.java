package com.kamil.exampleapp.mappers;

import com.kamil.exampleapp.datasource.api.mapper.HierarchyTreeEntityMapper;
import com.kamil.exampleapp.datasource.api.model.HierarchyNodeEntity;
import com.kamil.exampleapp.datasource.api.model.HierarchyTreeEntity;
import com.kamil.exampleapp.screen.main.model.HierarchyTree;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class HierarchyTreeEntityMapperTest {

    private HierarchyTreeEntityMapper mapper;

    @Before
    public void setUp() {
        mapper = new HierarchyTreeEntityMapper();
    }

    @Test
    public void shouldNotCreateHierarchyTreeFromNullEntity() {
        HierarchyTreeEntity entity = null;
        HierarchyTree tree = mapper.from(entity);

        assertTrue(tree == null);
    }

    @Test
    public void shouldNotCreateHierarchyTreeFromEntityWithNullNodes() {
        HierarchyTreeEntity entity = new HierarchyTreeEntity();
        entity.nodeEntities = null;
        HierarchyTree tree = mapper.from(entity);

        assertTrue(tree == null);
    }

    @Test
    public void shouldNotCreateHierarchyTreeFromEntityWithEmptyNodes() {
        HierarchyTreeEntity entity = new HierarchyTreeEntity();
        entity.nodeEntities = new ArrayList<>();
        HierarchyTree tree = mapper.from(entity);

        assertTrue(tree == null);
    }

    @Test
    public void shouldCreateHierarchyTreeFromEntityWithNotEmptyNodes() {
        HierarchyTreeEntity entity = new HierarchyTreeEntity();
        entity.nodeEntities = new ArrayList<>();
        HierarchyNodeEntity entityA = new HierarchyNodeEntity();
        entityA.id = 1L;
        entityA.name = "test";
        entityA.parentId = 10L;
        entity.nodeEntities.add(entityA);

        HierarchyTree tree = mapper.from(entity);

        assertTrue(tree != null);
    }

}
