package com.kamil.exampleapp.mappers;

import com.kamil.exampleapp.datasource.api.mapper.HierarchyNodeEntityMapper;
import com.kamil.exampleapp.datasource.api.model.HierarchyNodeEntity;
import com.kamil.exampleapp.screen.main.model.HierarchyNode;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;


@RunWith(JUnit4.class)
public class HierarchyNodeEntityMapperTest {

    private HierarchyNodeEntityMapper mapper;

    @Before
    public void setUp() {
        mapper = new HierarchyNodeEntityMapper();
    }

    @Test
    public void shouldNotCreateHierarchyNodeWhenEntityIsNull() {
        HierarchyNode node = mapper.from(null);
        assertTrue(node == null);
    }

    @Test
    public void shouldNotCreateHierarchyNodeWhenEntityIdIsNull() {
        HierarchyNodeEntity entity = new HierarchyNodeEntity();
        entity.id = null;
        entity.name = "test";
        entity.parentId = 1L;
        HierarchyNode node = mapper.from(entity);

        assertTrue(node == null);
    }

    @Test
    public void shouldNotCreateHierarchyNodeWhenEntityNameIsNull() {
        HierarchyNodeEntity entity = new HierarchyNodeEntity();
        entity.id = 1L;
        entity.name = null;
        entity.parentId = 10L;
        HierarchyNode node = mapper.from(entity);

        assertTrue(node == null);
    }

    @Test
    public void shouldNotCreateHierarchyNodeWhenEntityIdAndParentIdAreEqual() {
        HierarchyNodeEntity entity = new HierarchyNodeEntity();
        entity.id = 1L;
        entity.name = "test";
        entity.parentId = 1L;
        HierarchyNode node = mapper.from(entity);

        assertTrue(node == null);
    }

    @Test
    public void shouldCreateRootNode() {
        HierarchyNodeEntity entity = new HierarchyNodeEntity();
        entity.id = 1L;
        entity.name = "test";
        entity.parentId = null;
        HierarchyNode node = mapper.from(entity);

        assertTrue(node != null);
    }

    @Test
    public void shouldCreateChildNode() {
        HierarchyNodeEntity entity = new HierarchyNodeEntity();
        entity.id = 1L;
        entity.name = "test";
        entity.parentId = 10L;
        HierarchyNode node = mapper.from(entity);

        assertTrue(node != null);
    }

    @Test
    public void shouldValuesAreMappedProperly() {
        HierarchyNodeEntity entity = new HierarchyNodeEntity();
        entity.id = 1L;
        entity.name = "name";
        entity.parentId = 10L;

        HierarchyNode node = mapper.from(entity);
        assertTrue(node.getId().equals(1L));
        assertTrue(node.getName().equals("name"));
        assertTrue(node.getParentId().equals(10L));
    }

}
