package com.kamil.exampleapp.testdata;

import com.kamil.exampleapp.screen.browseproducts.model.ProductItem;
import com.kamil.exampleapp.screen.browseproducts.model.InFavoritesState;
import com.kamil.exampleapp.screen.browseproducts.model.RemovalState;
import com.kamil.exampleapp.screen.main.model.ExampleHierarchyNode;
import com.kamil.exampleapp.screen.main.model.ExampleHierarchyTree;
import com.kamil.exampleapp.screen.main.model.HierarchyNode;
import com.kamil.exampleapp.screen.main.model.HierarchyTree;

import java.util.ArrayList;
import java.util.List;

public class ExampleSqliteDatabaseClientTestData {

    public final HierarchyNode rootClothes = new ExampleHierarchyNode(1L, "CLOTHES", null);
    public final HierarchyNode forMen = new ExampleHierarchyNode(2L, "FOR MEN", 1L);
    public final HierarchyNode forWomen = new ExampleHierarchyNode(3L, "FOR WOMEN", 1L);
    public final HierarchyNode skirts = new ExampleHierarchyNode(4L, "SKIRTS", 3L);
    public final HierarchyNode gloves = new ExampleHierarchyNode(5L, "GLOVES", 2L);
    public final HierarchyNode miniSkirts = new ExampleHierarchyNode(6L, "MINI SKIRTS", 4L);
    public final HierarchyNode leatherGloves = new ExampleHierarchyNode(7L, "LEATHER GLOVES", 5L);
    public final List<HierarchyNode> clothesNodes = new ArrayList<>();
    public final HierarchyNode updatedSkirts = new ExampleHierarchyNode(4L, "UPDATED SKIRTS", 3L);
    public final HierarchyNode updatedGloves = new ExampleHierarchyNode(5L, "UPDATED GLOVES", 2L);
    public final HierarchyNode insertedMediumSkirts = new ExampleHierarchyNode(8L, "INSERTED MEDIUM SKIRTS", 4L);
    public final HierarchyNode insertedLongSkirts = new ExampleHierarchyNode(9L, "INSERTED LONG SKIRTS", 4L);
    public final List<HierarchyNode> refreshedClothesNodes = new ArrayList<>();

    public final List<HierarchyTree> clothesHierarchyTrees = new ArrayList<>();
    public final List<HierarchyTree> updatedClothesHierarchyTrees = new ArrayList<>();

    public ProductItem firstProductItem;
    public ProductItem secondProductItem;
    public ProductItem thirdProductItem;

    public ExampleSqliteDatabaseClientTestData() {
        prepareClothNodes();
        prepareRefreshedClothNodes();
        prepareHierarchyTrees();
        prepareProductItems();
    }

    private void prepareClothNodes() {
        clothesNodes.add(rootClothes);
        clothesNodes.add(forMen);
        clothesNodes.add(forWomen);
        clothesNodes.add(skirts);
        clothesNodes.add(gloves);
        clothesNodes.add(miniSkirts);
        clothesNodes.add(leatherGloves);
    }

    private void prepareRefreshedClothNodes() {
        refreshedClothesNodes.add(rootClothes);
        refreshedClothesNodes.add(forMen);
        refreshedClothesNodes.add(forWomen);
        refreshedClothesNodes.add(updatedSkirts);
        refreshedClothesNodes.add(updatedGloves);
        refreshedClothesNodes.add(miniSkirts);
        refreshedClothesNodes.add(leatherGloves);
        refreshedClothesNodes.add(insertedMediumSkirts);
        refreshedClothesNodes.add(insertedLongSkirts);
    }

    private void prepareHierarchyTrees() {
        HierarchyTree clothes = new ExampleHierarchyTree(clothesNodes);
        clothesHierarchyTrees.add(clothes);

        HierarchyTree updatedClothes = new ExampleHierarchyTree(refreshedClothesNodes);
        updatedClothesHierarchyTrees.add(updatedClothes);
    }

    private void prepareProductItems() {
        List<Long> firstAssignedHierarchies = new ArrayList<>();
        firstAssignedHierarchies.add(rootClothes.getId());
        firstAssignedHierarchies.add(forWomen.getId());
        firstAssignedHierarchies.add(skirts.getId());
        firstAssignedHierarchies.add(miniSkirts.getId());
        firstProductItem = new ProductItem.Builder()
                .id(1L)
                .name("mini skirt A")
                .brand("versace")
                .price("100.99")
                .inFavoriteState(InFavoritesState.NOT_ADDED)
                .removalState(RemovalState.NOT_REMOVED)
                .assignedHierarchies(firstAssignedHierarchies)
                .build();

        List<Long> secondAssignedHierarchies = new ArrayList<>();
        secondAssignedHierarchies.add(rootClothes.getId());
        secondAssignedHierarchies.add(forWomen.getId());
        secondAssignedHierarchies.add(skirts.getId());
        secondAssignedHierarchies.add(miniSkirts.getId());
        secondProductItem = new ProductItem.Builder()
                .id(2L)
                .name("mini skirt B")
                .brand("super versace")
                .price("200.99")
                .inFavoriteState(InFavoritesState.NOT_ADDED)
                .removalState(RemovalState.NOT_REMOVED)
                .assignedHierarchies(secondAssignedHierarchies)
                .build();

        List<Long> thirdAssignedHierarchies = new ArrayList<>();
        thirdAssignedHierarchies.add(rootClothes.getId());
        thirdAssignedHierarchies.add(forMen.getId());
        thirdAssignedHierarchies.add(gloves.getId());
        thirdAssignedHierarchies.add(leatherGloves.getId());
        thirdProductItem = new ProductItem.Builder()
                .id(3L)
                .name("leather gloves")
                .price("300.99")
                .inFavoriteState(InFavoritesState.NOT_ADDED)
                .removalState(RemovalState.NOT_REMOVED)
                .assignedHierarchies(thirdAssignedHierarchies)
                .build();
    }

}
