package com.kamil.exampleapp.testdata;

import com.kamil.exampleapp.screen.main.model.ExampleHierarchyNode;
import com.kamil.exampleapp.screen.main.model.HierarchyNode;

import java.util.ArrayList;
import java.util.List;

public class ExampleHierarchyTreeTestData {

    private final HierarchyNode rootClothes = new ExampleHierarchyNode(1L, "CLOTHES", null);
    private final HierarchyNode forMen = new ExampleHierarchyNode(2L, "FOR MEN", 1L);
    private final HierarchyNode forWomen = new ExampleHierarchyNode(3L, "FOR WOMEN", 1L);
    private final HierarchyNode belts = new ExampleHierarchyNode(4L, "belts", 2L);
    private final HierarchyNode skirts = new ExampleHierarchyNode(5L, "skirts", 3L);
    private final HierarchyNode dresses = new ExampleHierarchyNode(6L, "dresses", 3L);
    private final HierarchyNode miniSkirts = new ExampleHierarchyNode(7L, "MINI skirts", 5L);
    private final HierarchyNode mediumSkirts = new ExampleHierarchyNode(8L, "MEDIUM skirts", 5L);
    private final HierarchyNode longSkirts = new ExampleHierarchyNode(9L, "LONG skirts", 5L);
    private final HierarchyNode ultraLongSkirts = new ExampleHierarchyNode(10L, "ULTRA LONG skirts", 5L);
    private final HierarchyNode gloves = new ExampleHierarchyNode(11L, "gloves", 2L);
    private final HierarchyNode leatherGloves = new ExampleHierarchyNode(12L, "LEATHER gloves", 11L);
    private final HierarchyNode cottonGloves = new ExampleHierarchyNode(13L, "COTTON gloves", 11L);
    private final HierarchyNode intruderNode = new ExampleHierarchyNode(20L, "INTRUDER", 35L);
    private final List<HierarchyNode> clothNodes = new ArrayList<>();
    private final HierarchyNode rootError = new ExampleHierarchyNode(1L, "ROOT ERROR", null);
    private final HierarchyNode errorOne = new ExampleHierarchyNode(2L, "ERROR ONE", null);
    private final HierarchyNode errorTwo = new ExampleHierarchyNode(3L, "ERROR TWO", 10L);
    private final HierarchyNode errorThree = new ExampleHierarchyNode(4L, "ERROR THREE", 30L);
    private final List<HierarchyNode> badNodes = new ArrayList<>();

    public ExampleHierarchyTreeTestData() {
        clothNodes.add(rootClothes);
        clothNodes.add(forMen);
        clothNodes.add(forWomen);
        clothNodes.add(belts);
        clothNodes.add(skirts);
        clothNodes.add(dresses);
        clothNodes.add(miniSkirts);
        clothNodes.add(mediumSkirts);
        clothNodes.add(longSkirts);
        clothNodes.add(ultraLongSkirts);
        clothNodes.add(gloves);
        clothNodes.add(leatherGloves);
        clothNodes.add(cottonGloves);

        badNodes.add(rootError);
        badNodes.add(errorOne);
        badNodes.add(errorTwo);
        badNodes.add(errorThree);
    }

    public HierarchyNode rootClothes() {
        return rootClothes;
    }

    public HierarchyNode forMen() {
        return forMen;
    }

    public HierarchyNode forWomen() {
        return forWomen;
    }

    public HierarchyNode belts() {
        return belts;
    }

    public HierarchyNode skirts() {
        return skirts;
    }

    public HierarchyNode dresses() {
        return dresses;
    }

    public HierarchyNode miniSkirts() {
        return miniSkirts;
    }

    public HierarchyNode mediumSkirts() {
        return mediumSkirts;
    }

    public HierarchyNode longSkirts() {
        return longSkirts;
    }

    public HierarchyNode ultraLongSkirts() {
        return ultraLongSkirts;
    }

    public HierarchyNode gloves() {
        return gloves;
    }

    public HierarchyNode leatherGloves() {
        return leatherGloves;
    }

    public HierarchyNode cottonGloves() {
        return cottonGloves;
    }

    public HierarchyNode intruderNode() {
        return intruderNode;
    }

    public List<HierarchyNode> clothNodes() {
        return clothNodes;
    }

    public HierarchyNode rootError() {
        return rootError;
    }

    public HierarchyNode errorOne() {
        return errorOne;
    }

    public HierarchyNode errorTwo() {
        return errorTwo;
    }

    public HierarchyNode errorThree() {
        return errorThree;
    }

    public List<HierarchyNode> badNodes() {
        return badNodes;
    }
}
