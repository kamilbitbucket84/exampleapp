package com.kamil.exampleapp.testdata;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProductItemTestData {

    public Long goodId = 1L;
    public String goodName = "Good Product";
    public String goodBrand = "Good Brand";
    public String goodPrice = "24.55";
    public List<Long> goodHierarchiesId = Arrays.asList(1L, 2L, 3L, 4L, 5L, 6L);
    public Long badZeroId = 0L;
    public String badPriceFormat = "24.55gdfd";
    public String empty_string = "";
    public List<Long> badEmptyHierarchiesId = new ArrayList<>();

}
