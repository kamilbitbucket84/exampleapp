package com.kamil.exampleapp.testdata;

import com.kamil.exampleapp.screen.main.model.ExampleHierarchyNode;
import com.kamil.exampleapp.screen.main.model.HierarchyNode;

import java.util.ArrayList;
import java.util.List;

public class HierarchyTreeInputValidatorTestData {

    private final List<HierarchyNode> errorNodesNoRoot = new ArrayList<>();
    private final List<HierarchyNode> errorNodesTwoRoots = new ArrayList<>();
    private final List<HierarchyNode> errorNodesNotAllConnected = new ArrayList<>();
    private final List<HierarchyNode> correctNodesOnlyOneRoot = new ArrayList<>();
    private final List<HierarchyNode> correctNodesAllConnected = new ArrayList<>();
    private final List<HierarchyNode> errorNodesOneIdRepeated = new ArrayList<>();

    public HierarchyTreeInputValidatorTestData() {
        errorNodesNoRoot.add(new ExampleHierarchyNode(1L, "A", 4L));
        errorNodesNoRoot.add(new ExampleHierarchyNode(2L, "B", 1L));
        errorNodesNoRoot.add(new ExampleHierarchyNode(3L, "C", 1L));
        errorNodesNoRoot.add(new ExampleHierarchyNode(4L, "D", 1L));

        errorNodesTwoRoots.add(new ExampleHierarchyNode(1L, "A", null));
        errorNodesTwoRoots.add(new ExampleHierarchyNode(2L, "B", null));
        errorNodesTwoRoots.add(new ExampleHierarchyNode(3L, "C", 1L));
        errorNodesTwoRoots.add(new ExampleHierarchyNode(4L, "D", 1L));

        errorNodesNotAllConnected.add(new ExampleHierarchyNode(1L, "A", null));
        errorNodesNotAllConnected.add(new ExampleHierarchyNode(2L, "B", 1L));
        errorNodesNotAllConnected.add(new ExampleHierarchyNode(3L, "C", 1L));
        errorNodesNotAllConnected.add(new ExampleHierarchyNode(4L, "D", 10L));
        errorNodesNotAllConnected.add(new ExampleHierarchyNode(5L, "E", 20L));

        correctNodesOnlyOneRoot.add(new ExampleHierarchyNode(1L, "A", null));
        correctNodesOnlyOneRoot.add(new ExampleHierarchyNode(2L, "B", 1L));
        correctNodesOnlyOneRoot.add(new ExampleHierarchyNode(3L, "C", 1L));
        correctNodesOnlyOneRoot.add(new ExampleHierarchyNode(4L, "D", 1L));

        correctNodesAllConnected.add(new ExampleHierarchyNode(1L, "A", null));
        correctNodesAllConnected.add(new ExampleHierarchyNode(2L, "B", 1L));
        correctNodesAllConnected.add(new ExampleHierarchyNode(3L, "C", 1L));
        correctNodesAllConnected.add(new ExampleHierarchyNode(4L, "D", 2L));
        correctNodesAllConnected.add(new ExampleHierarchyNode(5L, "E", 2L));
        correctNodesAllConnected.add(new ExampleHierarchyNode(6L, "F", 3L));
        correctNodesAllConnected.add(new ExampleHierarchyNode(7L, "F", 4L));

        errorNodesOneIdRepeated.add(new ExampleHierarchyNode(1L, "A", null));
        errorNodesOneIdRepeated.add(new ExampleHierarchyNode(2L, "B", 1L));
        errorNodesOneIdRepeated.add(new ExampleHierarchyNode(3L, "C", 1L));
        errorNodesOneIdRepeated.add(new ExampleHierarchyNode(3L, "D", 2L));
    }

    public List<HierarchyNode> getErrorNodesNoRoot() {
        return errorNodesNoRoot;
    }

    public List<HierarchyNode> getErrorNodesTwoRoots() {
        return errorNodesTwoRoots;
    }

    public List<HierarchyNode> getErrorNodesNotAllConnected() {
        return errorNodesNotAllConnected;
    }

    public List<HierarchyNode> getCorrectNodesOnlyOneRoot() {
        return correctNodesOnlyOneRoot;
    }

    public List<HierarchyNode> getCorrectNodesAllConnected() {
        return correctNodesAllConnected;
    }

    public List<HierarchyNode> getErrorNodesOneIdRepeated() {
        return errorNodesOneIdRepeated;
    }

}
