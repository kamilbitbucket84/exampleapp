package com.kamil.exampleapp.datasource.database;


import android.database.Cursor;

import com.kamil.exampleapp.datasource.database.model.HierarchyDbEntity;
import com.kamil.exampleapp.datasource.database.model.ProductDbEntity;

final class CursorUtils {

    private CursorUtils() {
    }

    static ProductDbEntity createProductDbEntityFromCursor(Cursor cursor) {
        return new ProductDbEntity(
                cursor.getLong(cursor.getColumnIndex(ProductDbEntity._ID)),
                cursor.getString(cursor.getColumnIndex(ProductDbEntity.NAME)),
                cursor.getString(cursor.getColumnIndex(ProductDbEntity.BRAND)),
                cursor.getString(cursor.getColumnIndex(ProductDbEntity.PRICE)),
                cursor.getString(cursor.getColumnIndex(ProductDbEntity.IN_FAVORITES))
        );
    }

    static HierarchyDbEntity createHierarchyDbEntityFromCursor(Cursor cursor) {
        return new HierarchyDbEntity(
                cursor.getLong(cursor.getColumnIndex(HierarchyDbEntity._ID)),
                cursor.getString(cursor.getColumnIndex(HierarchyDbEntity.NAME)),
                cursor.getLong(cursor.getColumnIndex(HierarchyDbEntity.ID_PARENT))
        );
    }
}
