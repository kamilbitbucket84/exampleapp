package com.kamil.exampleapp.datasource.database.model;

import java.util.ArrayList;
import java.util.List;

public class HierarchyDbEntityRefresh {

    private final List<HierarchyDbEntity> updatedEntities = new ArrayList<>();
    private final List<HierarchyDbEntity> insertedEntities = new ArrayList<>();

    public HierarchyDbEntityRefresh(List<HierarchyDbEntity> updatedEntities, List<HierarchyDbEntity> insertedEntities) {
        if(updatedEntities != null && !updatedEntities.isEmpty()) {
            this.updatedEntities.addAll(updatedEntities);
        }

        if(insertedEntities != null && !insertedEntities.isEmpty()) {
            this.insertedEntities.addAll(insertedEntities);
        }
    }

    public List<HierarchyDbEntity> getUpdatedEntities() {
        return updatedEntities;
    }

    public List<HierarchyDbEntity> getInsertedEntities() {
        return insertedEntities;
    }

    public int getNumberOfAffectedRows() {
        return updatedEntities.size() + insertedEntities.size();
    }
}
