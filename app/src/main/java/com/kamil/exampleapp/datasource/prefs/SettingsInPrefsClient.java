package com.kamil.exampleapp.datasource.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.kamil.exampleapp.service.event.launcher.EventLaunchParams;
import com.kamil.exampleapp.service.event.launcher.ProximityEventLaunchParams;
import com.kamil.exampleapp.service.event.launcher.TimeEventLaunchParams;

import java.util.concurrent.TimeUnit;

import rx.Observable;

public class SettingsInPrefsClient implements SettingsClient {

    private static final String PREFERENCES_NAME = "EXAMPLE_PREFERENCES";

    private static final String TIME_UNIT_SEC = "SECONDS";
    private static final String TIME_UNIT_MIN = "MINUTES";
    private static final String KEY_TIME_LAUNCHER_STATE = "TIME_LAUNCHER_STATE";
    private static final String KEY_TIME_DELAY = "TIME_DELAY";
    private static final String KEY_TIME_PERIOD = "TIME_PERIOD";
    private static final String KEY_TIME_UNIT = "TIME_UNIT";
    private static final String KEY_PROXIMITY_LAUNCHER_STATE = "PROXIMITY_LAUNCHER_STATE";
    private static final String KEY_PROXIMITY_EVENTS_TO_LAUNCH = "PROXIMITY_EVENT_TO_LAUNCH";

    private static final boolean DEFAULT_TIME_LAUNCHER_STATE = false;
    private static final int DEFAULT_TIME_LAUNCHER_DELAY = 1;
    private static final int DEFAULT_TIME_LAUNCHER_PERIOD = 1;
    private static final String DEFAULT_TIME_LAUNCHER_UNIT = TIME_UNIT_MIN;
    private static final boolean DEFAULT_PROXIMITY_LAUNCHER_STATE = false;
    private static final int DEFAULT_PROXIMITY_EVENTS_TO_LAUNCH = 1;

    private final SharedPreferences settings;

    public SettingsInPrefsClient(Context context) {
        settings = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    @Override
    public Observable<EventLaunchParams> loadEventLaunchParams() {
        return Observable.fromCallable(() -> {
            boolean timeLauncherState = settings.getBoolean(KEY_TIME_LAUNCHER_STATE, false);
            boolean proximityLauncherState = settings.getBoolean(KEY_PROXIMITY_LAUNCHER_STATE, false);

            TimeEventLaunchParams timeEventLaunchParams = new TimeEventLaunchParams(
                    settings.getInt(KEY_TIME_DELAY, 0),
                    settings.getInt(KEY_TIME_PERIOD, 0),
                    convertStringToTimeUnit(settings.getString(KEY_TIME_UNIT, TIME_UNIT_SEC))
            );

            ProximityEventLaunchParams proximityEventLaunchParams = new ProximityEventLaunchParams(
                    settings.getInt(KEY_PROXIMITY_EVENTS_TO_LAUNCH, 1)
            );

            return new EventLaunchParams(
                    timeLauncherState,
                    proximityLauncherState,
                    timeEventLaunchParams,
                    proximityEventLaunchParams);
        });
    }

    @Override
    public Observable<Boolean> setTimeLauncherOn() {
        return Observable.fromCallable(() -> {
            return settings.edit().putBoolean(KEY_TIME_LAUNCHER_STATE, true).commit();
        });
    }

    @Override
    public Observable<Boolean> setTimeLauncherOff() {
        return Observable.fromCallable(() -> {
            return settings.edit().putBoolean(KEY_TIME_LAUNCHER_STATE, false).commit();
        });
    }

    @Override
    public Observable<Boolean> setTimeEventLaunchParams(TimeEventLaunchParams timeEventLaunchParams) {
        return Observable.fromCallable(() -> {
            Editor editor = settings.edit();
            editor.putInt(KEY_TIME_DELAY, timeEventLaunchParams.delay);
            editor.putInt(KEY_TIME_PERIOD, timeEventLaunchParams.eventPeriod);
            editor.putString(KEY_TIME_UNIT, convertTimeUnitToString(timeEventLaunchParams.timeUnit));
            return editor.commit();
        });
    }

    @Override
    public Observable<Boolean> setProximityLauncherOn() {
        return Observable.fromCallable(() -> {
            return settings.edit().putBoolean(KEY_PROXIMITY_LAUNCHER_STATE, true).commit();
        });
    }

    @Override
    public Observable<Boolean> setProximityLauncherOff() {
        return Observable.fromCallable(() -> {
            return settings.edit().putBoolean(KEY_PROXIMITY_LAUNCHER_STATE, false).commit();
        });
    }

    @Override
    public Observable<Boolean> setProximityEventLaunchParams(ProximityEventLaunchParams proximityEventLaunchParams) {
        return Observable.fromCallable(() -> {
            return settings.edit().putInt(KEY_PROXIMITY_EVENTS_TO_LAUNCH,
                    proximityEventLaunchParams.numberOfEventsToLaunch).commit();
        });
    }

    @Override
    public Observable<Boolean> goBackToDefaultEventSettings() {
        return Observable.fromCallable(() -> {
            Editor editor = settings.edit();
            editor.putBoolean(KEY_TIME_LAUNCHER_STATE, DEFAULT_TIME_LAUNCHER_STATE);
            editor.putInt(KEY_TIME_DELAY, DEFAULT_TIME_LAUNCHER_DELAY);
            editor.putInt(KEY_TIME_PERIOD, DEFAULT_TIME_LAUNCHER_PERIOD);
            editor.putString(KEY_TIME_UNIT, DEFAULT_TIME_LAUNCHER_UNIT);
            editor.putBoolean(KEY_PROXIMITY_LAUNCHER_STATE, DEFAULT_PROXIMITY_LAUNCHER_STATE);
            editor.putInt(KEY_PROXIMITY_EVENTS_TO_LAUNCH, DEFAULT_PROXIMITY_EVENTS_TO_LAUNCH);
            return editor.commit();
        });
    }

    ////////////////////////////////////////////////////////////////////////////////
    //  helper converter methods
    ////////////////////////////////////////////////////////////////////////////////

    private TimeUnit convertStringToTimeUnit(String timeUnitText) {
        //  not all units are concerned because not all will be allowed
        if (timeUnitText.equals(TIME_UNIT_SEC)) {
            return TimeUnit.SECONDS;
        } else {
            return TimeUnit.MINUTES;
        }
    }

    private String convertTimeUnitToString(TimeUnit timeUnit) {
        //  not all units are concerned because not all will be allowed
        if (timeUnit.equals(TimeUnit.SECONDS)) {
            return TIME_UNIT_SEC;
        } else {
            return TIME_UNIT_MIN;
        }
    }

}
