package com.kamil.exampleapp.datasource.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.kamil.exampleapp.R;

public class ExampleSQLiteHelper extends SQLiteOpenHelper {

    private static final String EXAMPLE_DB_NAME = "example.db";
    private static final int EXAMPLE_DB_V1 = 1;
    private final String EXAMPLE_DB_DDL_V1;

    public ExampleSQLiteHelper(Context context) {
        super(context, EXAMPLE_DB_NAME, null, EXAMPLE_DB_V1);
        EXAMPLE_DB_DDL_V1 = context.getResources().getString(R.string.example_sql_ddl_v1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        SQLUtils.executeMultipleSQLStatements(EXAMPLE_DB_DDL_V1, db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}

