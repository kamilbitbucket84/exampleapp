package com.kamil.exampleapp.datasource.database.model;

import com.kamil.exampleapp.screen.browseproducts.model.InFavoritesState;

public class ProductDbEntity {

    public static final String TABLE_NAME = "ProductDbEntity";
    public static final String _ID = "_ID";
    public static final String NAME = "NAME";
    public static final String BRAND = "BRAND";
    public static final String PRICE = "PRICE";
    public static final String IN_FAVORITES = "IN_FAVORITES";
    public static final String IN_FAVORITES_YES = "Y";
    public static final String IN_FAVORITES_NO = "N";

    private final Long id;
    private final String name;
    private final String brand;
    private final String price;

    private final String inFavorites;

    public ProductDbEntity(Long id, String name, String brand, String price, String inFavorites) {
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.price = price;
        this.inFavorites = inFavorites;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getBrand() {
        return brand;
    }

    public String getPrice() {
        return price;
    }

    public String getInFavorites() {
        return inFavorites;
    }

    public InFavoritesState getInFavoritesState() {
        return inFavorites.equals(IN_FAVORITES_YES) ?
                InFavoritesState.ADDED : InFavoritesState.NOT_ADDED;
    }

    public static String getInFavoritesString(InFavoritesState state) {
        if (state.equals(InFavoritesState.ADDED)) {
            return ProductDbEntity.IN_FAVORITES_YES;
        } else {
            return ProductDbEntity.IN_FAVORITES_NO;
        }
    }
}
