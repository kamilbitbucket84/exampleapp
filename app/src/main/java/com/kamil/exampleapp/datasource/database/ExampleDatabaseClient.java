package com.kamil.exampleapp.datasource.database;

import com.kamil.exampleapp.datasource.database.model.HierarchyDbEntity;
import com.kamil.exampleapp.datasource.database.model.HierarchyDbEntityRefresh;
import com.kamil.exampleapp.datasource.database.model.ProductDbEntity;
import com.kamil.exampleapp.screen.browseproducts.model.ProductItem;
import com.kamil.exampleapp.screen.main.model.HierarchyTree;

import java.io.File;
import java.util.List;

import rx.Observable;

public interface ExampleDatabaseClient {

    Observable<List<ProductItem>> selectProductItemsWithHierarchyId(Long hierarchyId);

    Observable<List<ProductItem>> selectProductItemsWithHierarchyIdAndQuery(Long hierarchyId, String query);

    Observable<ProductItem> insertProductItemWithAssignedHierarchies(ProductItem productItem);

    Observable<List<ProductDbEntity>> selectAllProductDbEntities();

    Observable<List<HierarchyDbEntity>> selectAllHierarchyDbEntities();

    Observable<Integer> deleteAllProductDbEntities();

    Observable<Integer> deleteAllHierarchyDbEntities();

    Observable<HierarchyDbEntityRefresh> refreshDbHierarchies(List<HierarchyTree> hierarchyTrees);

    Observable<Boolean> addToFavorites(ProductItem productItem);

    Observable<Boolean> removeFromFavorites(ProductItem productItem);

    Observable<Boolean> removeProductItem(ProductItem productItem);
}
