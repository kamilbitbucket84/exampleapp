package com.kamil.exampleapp.datasource.api;

import com.kamil.exampleapp.datasource.api.model.HierarchyResponseEntity;

import rx.Observable;

public class FirebaseRetrofitApiClient implements FirebaseApiClient {

    private final FirebaseService firebaseService;

    public FirebaseRetrofitApiClient(FirebaseService service) {
        this.firebaseService = service;
    }

    @Override
    public Observable<HierarchyResponseEntity> getHierarchies() {
        return firebaseService.getHierarchiesResponse();
    }
}
