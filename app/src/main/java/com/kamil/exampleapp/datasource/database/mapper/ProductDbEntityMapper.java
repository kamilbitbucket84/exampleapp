package com.kamil.exampleapp.datasource.database.mapper;

import com.kamil.exampleapp.common.mapper.Mapper;
import com.kamil.exampleapp.datasource.database.model.ProductDbEntity;
import com.kamil.exampleapp.screen.browseproducts.model.ProductItem;

public class ProductDbEntityMapper implements Mapper<ProductItem, ProductDbEntity> {

    @Override
    public ProductDbEntity from(ProductItem from) {
        return new ProductDbEntity(
                from.getId(),
                from.getName(),
                from.getBrand(),
                from.getPrice(),
                ProductDbEntity.getInFavoritesString(from.getInFavoritesState()));
    }

}
