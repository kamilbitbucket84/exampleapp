package com.kamil.exampleapp.datasource.api.mapper;

import com.kamil.exampleapp.common.mapper.Mapper;
import com.kamil.exampleapp.datasource.api.model.HierarchyNodeEntity;
import com.kamil.exampleapp.datasource.api.model.HierarchyTreeEntity;
import com.kamil.exampleapp.screen.main.model.ExampleHierarchyTree;
import com.kamil.exampleapp.screen.main.model.HierarchyNode;
import com.kamil.exampleapp.screen.main.model.HierarchyTree;

import java.util.ArrayList;
import java.util.List;

public class HierarchyTreeEntityMapper implements Mapper<HierarchyTreeEntity, HierarchyTree> {

    @Override
    public HierarchyTree from(HierarchyTreeEntity from) {
        if (from != null && from.nodeEntities != null && !from.nodeEntities.isEmpty()) {
            HierarchyNodeEntityMapper nodeEntityMapper = new HierarchyNodeEntityMapper();
            List<HierarchyNode> hierarchyNodes = new ArrayList<>();

            HierarchyNode hierarchyNode;
            for (HierarchyNodeEntity entity : from.nodeEntities) {
                hierarchyNode = nodeEntityMapper.from(entity);

                if (hierarchyNode != null) {
                    hierarchyNodes.add(hierarchyNode);
                }
            }

            return hierarchyNodes.isEmpty() ? null : new ExampleHierarchyTree(hierarchyNodes);
        } else {
            return null;
        }
    }
}
