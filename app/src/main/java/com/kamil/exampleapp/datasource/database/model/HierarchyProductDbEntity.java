package com.kamil.exampleapp.datasource.database.model;

public class HierarchyProductDbEntity {
    
    public static final String TABLE_NAME = "HierarchyProductDbEntity";
    public static final String _ID = "_ID";
    public static final String ID_HIERARCHY = "ID_HIERARCHY";
    public static final String ID_PRODUCT = "ID_PRODUCT";

    private final Long id;
    private final Long idHierarchy;
    private final Long idProduct;

    public HierarchyProductDbEntity(Long id, Long idHierarchy, Long idProduct) {
        this.id = id;
        this.idHierarchy = idHierarchy;
        this.idProduct = idProduct;
    }

    public Long getId() {
        return id;
    }

    public Long getIdHierarchy() {
        return idHierarchy;
    }

    public Long getIdProduct() {
        return idProduct;
    }
}



