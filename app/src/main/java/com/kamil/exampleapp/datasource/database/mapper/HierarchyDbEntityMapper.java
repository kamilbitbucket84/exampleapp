package com.kamil.exampleapp.datasource.database.mapper;

import com.kamil.exampleapp.common.mapper.Mapper;
import com.kamil.exampleapp.datasource.database.model.HierarchyDbEntity;
import com.kamil.exampleapp.screen.main.model.HierarchyNode;

public class HierarchyDbEntityMapper implements Mapper<HierarchyNode, HierarchyDbEntity> {

    @Override
    public HierarchyDbEntity from(HierarchyNode from) {
        return new HierarchyDbEntity(from.getId(), from.getName(), from.getParentId());
    }
}
