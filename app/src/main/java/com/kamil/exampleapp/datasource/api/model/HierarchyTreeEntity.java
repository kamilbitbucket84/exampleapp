package com.kamil.exampleapp.datasource.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public final class HierarchyTreeEntity {
    @SerializedName("nodes") public List<HierarchyNodeEntity> nodeEntities;
}
