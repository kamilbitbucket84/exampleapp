package com.kamil.exampleapp.datasource.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kamil.exampleapp.R;
import com.kamil.exampleapp.datasource.database.mapper.HierarchyDbEntityMapper;
import com.kamil.exampleapp.datasource.database.model.HierarchyDbEntity;
import com.kamil.exampleapp.datasource.database.model.HierarchyDbEntityRefresh;
import com.kamil.exampleapp.datasource.database.model.HierarchyProductDbEntity;
import com.kamil.exampleapp.datasource.database.model.ProductDbEntity;
import com.kamil.exampleapp.screen.browseproducts.model.ProductItem;
import com.kamil.exampleapp.screen.browseproducts.model.RemovalState;
import com.kamil.exampleapp.screen.main.model.HierarchyNode;
import com.kamil.exampleapp.screen.main.model.HierarchyTree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;

import rx.Observable;


public class ExampleSQLiteDatabaseClient implements ExampleDatabaseClient {

    private final SQLiteDatabase db;
    private final String SELECT_ALL_PRODUCTS_WITH_HIERARCHIES;

    public ExampleSQLiteDatabaseClient(Context context) {
        ExampleSQLiteHelper helper = new ExampleSQLiteHelper(context);
        db = helper.getWritableDatabase();
        db.setForeignKeyConstraintsEnabled(true);
        SELECT_ALL_PRODUCTS_WITH_HIERARCHIES = context.getResources().getString(R.string.select_all_products_with_hierarchies);
    }

    @Override
    public Observable<List<ProductDbEntity>> selectAllProductDbEntities() {
        return Observable.fromCallable(() -> {
            List<ProductDbEntity> productDbEntities = new ArrayList<>();
            Cursor cursor = db.query(ProductDbEntity.TABLE_NAME, null, null, null, null, null, null);

            ProductDbEntity productDbEntity;
            try {
                while (cursor != null && cursor.moveToNext()) {
                    productDbEntity = CursorUtils.createProductDbEntityFromCursor(cursor);
                    productDbEntities.add(productDbEntity);
                }
            } finally {
                if (cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }
            }

            return productDbEntities;
        });
    }

    @Override
    public Observable<List<HierarchyDbEntity>> selectAllHierarchyDbEntities() {
        return Observable.fromCallable(() -> {
            List<HierarchyDbEntity> hierarchyDbEntities = new ArrayList<>();
            Cursor cursor = db.query(HierarchyDbEntity.TABLE_NAME, null, null, null, null, null, null);

            HierarchyDbEntity hierarchyDbEntity;
            try {
                while (cursor != null && cursor.moveToNext()) {
                    hierarchyDbEntity = CursorUtils.createHierarchyDbEntityFromCursor(cursor);
                    hierarchyDbEntities.add(hierarchyDbEntity);
                }
            } finally {
                if (cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }
            }

            return hierarchyDbEntities;
        });
    }

    @Override
    public Observable<List<ProductItem>> selectProductItemsWithHierarchyId(Long hierarchyId) {
        return Observable.fromCallable(() -> {
            List<ProductDbEntity> productDbEntities = new ArrayList<>();
            HashMap<Long, List<Long>> hierarchyMap = new HashMap<>();
            Cursor cursor = db.rawQuery(SELECT_ALL_PRODUCTS_WITH_HIERARCHIES, null);

            try {
                while (cursor != null && cursor.moveToNext()) {
                    Long rowProductId = cursor.getLong(cursor.getColumnIndex(ProductDbEntity._ID));
                    Long rowHierarchyId = cursor.getLong(cursor.getColumnIndex(HierarchyProductDbEntity.ID_HIERARCHY));
                    assignHierarchyIdToProductIdInMap(hierarchyMap, rowProductId, rowHierarchyId);

                    if (rowHierarchyId.equals(hierarchyId)) {
                        ProductDbEntity productDbEntity = CursorUtils.createProductDbEntityFromCursor(cursor);
                        productDbEntities.add(productDbEntity);
                    }
                }
            } finally {
                if (cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }
            }

            return createListOfProductItems(productDbEntities, hierarchyMap);
        });
    }

    @Override
    public Observable<List<ProductItem>> selectProductItemsWithHierarchyIdAndQuery(Long hierarchyId, String query) {
        return Observable.fromCallable(() -> {
            List<ProductDbEntity> productDbEntities = new ArrayList<>();
            HashMap<Long, List<Long>> hierarchyMap = new HashMap<>();
            Cursor cursor = db.rawQuery(SELECT_ALL_PRODUCTS_WITH_HIERARCHIES, null);
            ProductDbEntity productDbEntity;

            try {
                while (cursor != null && cursor.moveToNext()) {
                    Long rowProductId = cursor.getLong(cursor.getColumnIndex(ProductDbEntity._ID));
                    Long rowHierarchyId = cursor.getLong(cursor.getColumnIndex(HierarchyProductDbEntity.ID_HIERARCHY));
                    assignHierarchyIdToProductIdInMap(hierarchyMap, rowProductId, rowHierarchyId);
                    productDbEntity = CursorUtils.createProductDbEntityFromCursor(cursor);

                    if (rowHierarchyId.equals(hierarchyId) && checkQueryPresenceInProductDbEntity(productDbEntity, query)) {
                        productDbEntities.add(productDbEntity);
                    }
                }
            } finally {
                if (cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }
            }

            return createListOfProductItems(productDbEntities, hierarchyMap);
        });
    }

    private boolean checkQueryPresenceInProductDbEntity(ProductDbEntity productDbEntity, String query) {
        return productDbEntity.getName().toLowerCase().contains(query.toLowerCase()) ||
                productDbEntity.getBrand().toLowerCase().contains(query.toLowerCase());
    }

    private void assignHierarchyIdToProductIdInMap(HashMap<Long, List<Long>> map, Long productId, Long hierarchyId) {
        if (map.containsKey(productId)) {
            List<Long> productHierarchies = map.get(productId);
            productHierarchies.add(hierarchyId);
        } else {
            List<Long> productHierarchies = new ArrayList<>();
            productHierarchies.add(hierarchyId);
            map.put(productId, productHierarchies);
        }
    }

    private List<ProductItem> createListOfProductItems(List<ProductDbEntity> productDbEntities,
                                                       HashMap<Long, List<Long>> hierarchyMap) {
        List<ProductItem> productItems = new ArrayList<>();
        ProductItem productItem;

        for (ProductDbEntity entity : productDbEntities) {
            productItem = new ProductItem.Builder()
                    .id(entity.getId())
                    .name(entity.getName())
                    .brand(entity.getBrand())
                    .price(entity.getPrice())
                    .inFavoriteState(entity.getInFavoritesState())
                    .removalState(RemovalState.NOT_REMOVED)
                    .assignedHierarchies(hierarchyMap.get(entity.getId()))
                    .build();

            productItems.add(productItem);
        }

        return productItems;
    }

    @Override
    public Observable<ProductItem> insertProductItemWithAssignedHierarchies(ProductItem productItem) {
        return Observable.fromCallable(() -> {
            db.beginTransaction();

            //  insert ProductDbentity to ProductDbEntity table
            ContentValues productValues = new ContentValues();
            productValues.put(ProductDbEntity._ID, productItem.getId());
            productValues.put(ProductDbEntity.NAME, productItem.getName());
            productValues.put(ProductDbEntity.BRAND, productItem.getBrand());
            productValues.put(ProductDbEntity.PRICE, productItem.getPrice());
            productValues.put(ProductDbEntity.IN_FAVORITES, ProductDbEntity.IN_FAVORITES_NO);
            db.insertOrThrow(ProductDbEntity.TABLE_NAME, null, productValues);

            //  assign new productDbEntity to given hierarchies (insert hierarchyproductdb entity)
            for (Long hierarchyId : productItem.getAssignedHierarchies()) {
                ContentValues values = new ContentValues();
                values.put(HierarchyProductDbEntity.ID_HIERARCHY, hierarchyId);
                values.put(HierarchyProductDbEntity.ID_PRODUCT, productItem.getId());
                db.insertOrThrow(HierarchyProductDbEntity.TABLE_NAME, null, values);
            }

            db.setTransactionSuccessful();
            db.endTransaction();
            return productItem;
        });
    }

    @Override
    public Observable<Integer> deleteAllProductDbEntities() {
        return Observable.fromCallable(() -> {
            return db.delete(ProductDbEntity.TABLE_NAME, null, null);
        });
    }

    @Override
    public Observable<Integer> deleteAllHierarchyDbEntities() {
        return Observable.fromCallable(() -> {
            return db.delete(HierarchyDbEntity.TABLE_NAME, null, null);
        });
    }

    @Override
    public Observable<HierarchyDbEntityRefresh> refreshDbHierarchies(List<HierarchyTree> hierarchyTrees) {
        return Observable.fromCallable(() -> {
            db.beginTransaction();

            List<HierarchyDbEntity> updatedHierarchies = new ArrayList<>();
            List<HierarchyDbEntity> insertedHierarchies = new ArrayList<>();

            HierarchyDbEntityMapper mapper = new HierarchyDbEntityMapper();
            for (HierarchyTree tree : hierarchyTrees) {
                for (HierarchyNode node : tree.getAllNodes()) {
                    HierarchyDbEntity entity = selectHierarchyDbEntityWithId(node.getId());
                    if (entity == null) {
                        insertHierarchyDbEntity(mapper.from(node));
                        insertedHierarchies.add(mapper.from(node));
                    } else if (!entity.equals(mapper.from(node))) {
                        updateHierarchyDbEntity(mapper.from(node));
                        updatedHierarchies.add(mapper.from(node));
                    }
                }
            }

            db.setTransactionSuccessful();
            db.endTransaction();

            return new HierarchyDbEntityRefresh(updatedHierarchies, insertedHierarchies);
        });
    }

    @Override
    public Observable<Boolean> addToFavorites(ProductItem productItem) {
        return Observable.fromCallable(() -> {
            ContentValues values = new ContentValues();
            values.put(ProductDbEntity.IN_FAVORITES, ProductDbEntity.IN_FAVORITES_YES);

            db.update(ProductDbEntity.TABLE_NAME,
                    values,
                    ProductDbEntity._ID + " = ?",
                    new String[]{"" + productItem.getId()}
            );

            return true;
        });
    }

    @Override
    public Observable<Boolean> removeFromFavorites(ProductItem productItem) {
        return Observable.fromCallable(() -> {
            ContentValues values = new ContentValues();
            values.put(ProductDbEntity.IN_FAVORITES, ProductDbEntity.IN_FAVORITES_NO);

            db.update(ProductDbEntity.TABLE_NAME,
                    values,
                    ProductDbEntity._ID + " = ?",
                    new String[]{"" + productItem.getId()}
            );

            return true;
        });
    }

    @Override
    public Observable<Boolean> removeProductItem(ProductItem productItem) {
        return Observable.fromCallable(() -> {
            db.delete(ProductDbEntity.TABLE_NAME, "_ID = ?", new String[]{String.valueOf(productItem.getId())});
            return true;
        });
    }

    private HierarchyDbEntity selectHierarchyDbEntityWithId(Long id) {
        Cursor cursor = db.query(HierarchyDbEntity.TABLE_NAME, null, "_ID= ?",
                new String[]{"" + id},
                null,
                null,
                null);

        HierarchyDbEntity hierarchyDbEntity = null;
        try {
            if (cursor != null && cursor.moveToNext()) {
                hierarchyDbEntity = CursorUtils.createHierarchyDbEntityFromCursor(cursor);
            }
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }

        return hierarchyDbEntity;
    }

    private void insertHierarchyDbEntity(HierarchyDbEntity entity) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(HierarchyDbEntity._ID, entity.getId());
        contentValues.put(HierarchyDbEntity.NAME, entity.getName());
        contentValues.put(HierarchyDbEntity.ID_PARENT, entity.getIdParent());

        db.insertOrThrow(HierarchyDbEntity.TABLE_NAME, null, contentValues);
    }

    private void updateHierarchyDbEntity(HierarchyDbEntity entity) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(HierarchyDbEntity.NAME, entity.getName());
        contentValues.put(HierarchyDbEntity.ID_PARENT, entity.getIdParent());

        db.update(HierarchyDbEntity.TABLE_NAME,
                contentValues,
                HierarchyDbEntity._ID + " = ?",
                new String[]{"" + entity.getId()});
    }
}
