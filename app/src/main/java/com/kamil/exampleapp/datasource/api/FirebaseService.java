package com.kamil.exampleapp.datasource.api;

import com.kamil.exampleapp.datasource.api.model.HierarchyResponseEntity;

import retrofit2.http.GET;
import rx.Observable;


public interface FirebaseService {
    String BASE_ENDPOINT = "https://exampleapp-b75d5.firebaseio.com/";

    @GET("hierarchies.json")
    Observable<HierarchyResponseEntity> getHierarchiesResponse();
}
