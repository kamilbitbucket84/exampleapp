package com.kamil.exampleapp.datasource.prefs;

import com.kamil.exampleapp.service.event.launcher.EventLaunchParams;
import com.kamil.exampleapp.service.event.launcher.ProximityEventLaunchParams;
import com.kamil.exampleapp.service.event.launcher.TimeEventLaunchParams;

import rx.Observable;

public interface SettingsClient {

    Observable<EventLaunchParams> loadEventLaunchParams();

    Observable<Boolean> setTimeLauncherOn();

    Observable<Boolean> setTimeLauncherOff();

    Observable<Boolean> setTimeEventLaunchParams(TimeEventLaunchParams timeEventLaunchParams);

    Observable<Boolean> setProximityLauncherOn();

    Observable<Boolean> setProximityLauncherOff();

    Observable<Boolean> setProximityEventLaunchParams(ProximityEventLaunchParams proximityEventLaunchParams);

    Observable<Boolean> goBackToDefaultEventSettings();
}
