package com.kamil.exampleapp.datasource.api.mapper;

import com.kamil.exampleapp.common.mapper.Mapper;
import com.kamil.exampleapp.datasource.api.model.HierarchyNodeEntity;
import com.kamil.exampleapp.screen.main.model.HierarchyNode;

public class HierarchyNodeMapper implements Mapper<HierarchyNode, HierarchyNodeEntity> {

    @Override
    public HierarchyNodeEntity from(HierarchyNode from) {
        HierarchyNodeEntity entity = new HierarchyNodeEntity();
        entity.id = from.getId();
        entity.name = from.getName();
        entity.parentId = from.getParentId();
        return entity;
    }
}
