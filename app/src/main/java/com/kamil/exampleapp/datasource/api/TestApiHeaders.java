package com.kamil.exampleapp.datasource.api;

public enum TestApiHeaders {

    INSTANCE(new TestApiHeadersInterceptor());

    private final TestApiHeadersInterceptor apiHeadersInterceptor;

    TestApiHeaders(TestApiHeadersInterceptor apiHeadersInterceptor) {
        this.apiHeadersInterceptor = apiHeadersInterceptor;
    }

    public TestApiHeadersInterceptor getApiHeadersInterceptor() {
        return apiHeadersInterceptor;
    }
}
