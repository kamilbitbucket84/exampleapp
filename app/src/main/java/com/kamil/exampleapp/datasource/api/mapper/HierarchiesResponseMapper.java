package com.kamil.exampleapp.datasource.api.mapper;

import com.kamil.exampleapp.common.mapper.Mapper;
import com.kamil.exampleapp.datasource.api.model.HierarchyResponseEntity;
import com.kamil.exampleapp.datasource.api.model.HierarchyTreeEntity;
import com.kamil.exampleapp.screen.main.model.HierarchyTree;

import java.util.ArrayList;
import java.util.List;

public class HierarchiesResponseMapper implements Mapper<HierarchyResponseEntity, List<HierarchyTree>> {

    @Override
    public List<HierarchyTree> from(HierarchyResponseEntity from) {
        List<HierarchyTree> hierarchyTrees = new ArrayList<>();

        if (from != null && from.treeEntities != null && !from.treeEntities.isEmpty()) {
            HierarchyTreeEntityMapper hierarchyTreeEntityMapper = new HierarchyTreeEntityMapper();
            HierarchyTree hierarchyTree;
            for (HierarchyTreeEntity entity : from.treeEntities) {
                hierarchyTree = hierarchyTreeEntityMapper.from(entity);

                if (hierarchyTree != null && hierarchyTree.isValid()) {
                    hierarchyTrees.add(hierarchyTree);
                }
            }
        }

        return hierarchyTrees;
    }
}
