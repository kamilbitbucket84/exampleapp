package com.kamil.exampleapp.datasource.api.mapper;

import com.kamil.exampleapp.common.mapper.Mapper;
import com.kamil.exampleapp.datasource.api.model.HierarchyNodeEntity;
import com.kamil.exampleapp.screen.main.model.ExampleHierarchyNode;
import com.kamil.exampleapp.screen.main.model.HierarchyNode;

public class HierarchyNodeEntityMapper implements Mapper<HierarchyNodeEntity, HierarchyNode> {

    @Override
    public HierarchyNode from(HierarchyNodeEntity from) {
        if (from != null && from.id != null && from.name != null && !from.id.equals(from.parentId)) {
            return new ExampleHierarchyNode(from.id, from.name, from.parentId);
        } else {
            return null;
        }
    }
}
