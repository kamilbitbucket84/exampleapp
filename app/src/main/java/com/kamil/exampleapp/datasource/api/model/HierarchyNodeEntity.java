package com.kamil.exampleapp.datasource.api.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
public final class HierarchyNodeEntity {
    @SerializedName("id") public Long id;
    @SerializedName("name") public String name;
    @SerializedName("parent_id") public Long parentId;
}