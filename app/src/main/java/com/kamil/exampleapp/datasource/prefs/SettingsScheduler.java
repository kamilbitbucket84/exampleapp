package com.kamil.exampleapp.datasource.prefs;

import java.util.concurrent.Executors;

import rx.Scheduler;
import rx.schedulers.Schedulers;

public enum SettingsScheduler {

    INSTANCE(Schedulers.from(Executors.newSingleThreadExecutor()));

    private final Scheduler settingsScheduler;

    SettingsScheduler(Scheduler settingsScheduler) {
        this.settingsScheduler = settingsScheduler;
    }

    public Scheduler getScheduler() {
        return settingsScheduler;
    }
}
