package com.kamil.exampleapp.datasource.database;

import android.database.sqlite.SQLiteDatabase;

import java.util.StringTokenizer;

final class SQLUtils {

    private SQLUtils() {
    }

    static void executeMultipleSQLStatements(String ddlScript, SQLiteDatabase db) {
        StringTokenizer tokenizer = new StringTokenizer(ddlScript, ";");
        while (tokenizer.hasMoreElements()) {
            db.execSQL(tokenizer.nextToken());
        }
    }
}

