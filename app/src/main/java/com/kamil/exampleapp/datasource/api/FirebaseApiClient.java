package com.kamil.exampleapp.datasource.api;

import com.kamil.exampleapp.datasource.api.model.HierarchyResponseEntity;

import rx.Observable;

public interface FirebaseApiClient {

    Observable<HierarchyResponseEntity> getHierarchies();
}
