package com.kamil.exampleapp.datasource.api;

import com.kamil.exampleapp.common.util.LogUtils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class TestApiHeadersInterceptor implements Interceptor {

    private static final String AUTHORIZATION = "Authorization";
    private static final String CUSTOM = "Custom";
    private String authorizationToken;
    private String custom;

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        Request.Builder builder = original.newBuilder();

        if (authorizationToken != null) {
            builder.addHeader(AUTHORIZATION, authorizationToken);
        }

        if (custom != null) {
            builder.addHeader(CUSTOM, custom);
        }

        Request changed = builder.build();
        logRequest(changed, false);
        return chain.proceed(changed);
    }

    public void setAuthorizationToken(String token) {
        this.authorizationToken = token;
    }

    public void setCustom(String custom) {
        this.custom = custom;
    }

    public void clearAuthorizationToken() {
        authorizationToken = null;
    }

    public void clearCustom() {
        custom = null;
    }

    public void clearAllHeaders() {
        authorizationToken = null;
        custom = null;
    }

    private void logRequest(Request request, boolean shouldLog) {
        if (shouldLog) {
            LogUtils.d("**********************       REQUEST START     **********************");
            LogUtils.d("REQUEST URL -> " + request.url().toString());
            LogUtils.d("REQUEST HEADERS -> ");
            LogUtils.d(request.headers().toString());
            LogUtils.d("REQUEST BODY -> " + request.body());
            LogUtils.d("**********************       REQUEST END     **********************");
        }
    }
}
