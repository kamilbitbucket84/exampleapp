package com.kamil.exampleapp.datasource.database;

import java.util.concurrent.Executors;

import rx.Scheduler;
import rx.schedulers.Schedulers;

public enum DatabaseScheduler {

    INSTANCE(Schedulers.from(Executors.newSingleThreadExecutor()));

    private final Scheduler scheduler;

    DatabaseScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    public Scheduler getScheduler() {
        return scheduler;
    }
}
