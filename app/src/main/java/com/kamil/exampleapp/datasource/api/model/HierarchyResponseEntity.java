package com.kamil.exampleapp.datasource.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public final class HierarchyResponseEntity {
    @SerializedName("hierarchy_trees") public List<HierarchyTreeEntity> treeEntities;
}
