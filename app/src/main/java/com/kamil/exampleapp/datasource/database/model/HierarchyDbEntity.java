package com.kamil.exampleapp.datasource.database.model;

public class HierarchyDbEntity {

    public static final String TABLE_NAME = "HierarchyDbEntity";
    public static final String _ID = "_ID";
    public static final String NAME = "NAME";
    public static final String ID_PARENT = "ID_PARENT";

    private final Long id;
    private final String name;
    private final Long idParent;


    public HierarchyDbEntity(Long id, String name, Long idParent) {
        this.id = id;
        this.name = name;
        this.idParent = idParent;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Long getIdParent() {
        return idParent;
    }

    @Override
    public boolean equals(Object o) {
        if(this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HierarchyDbEntity entity = (HierarchyDbEntity) o;

        if (id != null ? !id.equals(entity.id) : entity.id != null) return false;
        if (name != null ? !name.equals(entity.name) : entity.name != null) return false;

        if(idParent == 0 && entity.idParent == null) {
            return true;
        }

        return idParent != null ? idParent.equals(entity.idParent) : entity.idParent == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (idParent != null ? idParent.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "HierarchyDbEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", idParent=" + idParent +
                '}';
    }
}