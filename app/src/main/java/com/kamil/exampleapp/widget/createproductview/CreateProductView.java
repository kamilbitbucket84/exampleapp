package com.kamil.exampleapp.widget.createproductview;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.LinearLayout;

import com.kamil.exampleapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CreateProductView extends LinearLayout {

    @BindView(R.id.view_create_product_brand_input) TextInputEditText brandInput;
    @BindView(R.id.view_create_product_name_input) TextInputEditText nameInput;
    @BindView(R.id.view_create_product_price_input) TextInputEditText priceInput;
    @BindView(R.id.view_create_product_create_button) Button createButton;
    @BindView(R.id.view_create_product_cancel_button) Button cancelButton;

    private OnCreateProductViewListener onCreateProductViewListener = OnCreateProductViewListener.EMPTY;

    public CreateProductView(Context context) {
        this(context, null);
    }

    public CreateProductView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CreateProductView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CreateProductView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs, defStyleAttr);
    }

    private void init(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        inflate(context, R.layout.view_create_product, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (isInEditMode()) {
            return;
        }
        ButterKnife.bind(this);
        setUpView();
    }

    private void setUpView() {
        setBackgroundColor(ContextCompat.getColor(getContext(), R.color.view_create_product_background));
        setOrientation(VERTICAL);
        setClickable(true);
    }

    @OnClick(R.id.view_create_product_cancel_button)
    protected void cancelClicked() {
        onCreateProductViewListener.onCreateProductCancelClicked();
    }

    @OnClick(R.id.view_create_product_create_button)
    protected void createClicked() {
        onCreateProductViewListener.onCreateProductCreateClicked();
    }

    public String getNameInput() {
        return nameInput.getText().toString().trim();
    }

    public String getBrandInput() {
        return brandInput.getText().toString().trim();
    }

    public String getPriceInput() {
        return priceInput.getText().toString().trim();
    }

    public void setOnCreateProductViewListener(OnCreateProductViewListener onCreateProductViewListener) {
        this.onCreateProductViewListener = onCreateProductViewListener;
    }
}
