package com.kamil.exampleapp.widget.hierarchyview;


import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kamil.exampleapp.R;
import com.kamil.exampleapp.screen.main.model.HierarchyNode;

public class HierarchyNodeView extends LinearLayout implements View.OnClickListener {

    public enum NODE_TYPE {
        ROOT_COLLAPSED,
        ROOT_EXPANDED,
        GO_BACK,
        GO_FORWARD,
        CURRENT,
        LEAF;
    }

    private TextView label;
    private ImageView backArrow;
    private ImageView forwardArrow;
    private HierarchyNode hierarchyNode;
    private NODE_TYPE nodeType;

    private OnHierarchyViewClickListener onHierarchyViewClickListener = OnHierarchyViewClickListener.EMPTY;

    public HierarchyNodeView(Context context) {
        this(context, null);
    }

    public HierarchyNodeView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HierarchyNodeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public HierarchyNodeView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.view_hierarchy_node, this);
        label = (TextView) findViewById(R.id.view_hierarchy_node_label);
        backArrow = (ImageView) findViewById(R.id.view_hierarchy_node_back);
        forwardArrow = (ImageView) findViewById(R.id.view_hierarchy_node_forward);

        setOnClickListener(this);
    }

    public void setOnHierarchyViewClickListener(OnHierarchyViewClickListener listener) {
        this.onHierarchyViewClickListener = listener;
    }

    public void prepareUI(HierarchyNode hierarchynode, NODE_TYPE type) {
        bindData(hierarchynode, type);

        switch (type) {
            case ROOT_COLLAPSED:
                modifyUIOfRootCollapsedView();
                break;
            case ROOT_EXPANDED:
                modifyUIOfRootExpandedView();
                break;
            case GO_BACK:
                modifyUIOfGoBackView();
                break;
            case GO_FORWARD:
                modifyUIOfGoForwardView();
                break;
            case CURRENT:
                modifyUIOfCurrentView();
                break;
            case LEAF:
                modifyUIOFLeafView();
                break;
        }
    }

    private void bindData(HierarchyNode hierarchynode, NODE_TYPE type) {
        this.hierarchyNode = hierarchynode;
        this.nodeType = type;
        label.setText(hierarchynode.getName());
    }

    private void modifyUIOfRootCollapsedView() {
        backArrow.setVisibility(GONE);
        forwardArrow.setVisibility(VISIBLE);
    }

    private void modifyUIOfRootExpandedView() {
        backArrow.setVisibility(VISIBLE);
        forwardArrow.setVisibility(GONE);
    }

    private void modifyUIOfGoBackView() {
        backArrow.setVisibility(VISIBLE);
        forwardArrow.setVisibility(GONE);
    }

    private void modifyUIOfGoForwardView() {
        backArrow.setVisibility(GONE);
        forwardArrow.setVisibility(VISIBLE);
    }

    private void modifyUIOfCurrentView() {
        backArrow.setVisibility(GONE);
        forwardArrow.setVisibility(GONE);
        label.setTextColor(ContextCompat.getColor(getContext(), R.color.hierarchy_node_view_current_node_label));
    }

    private void modifyUIOFLeafView() {
        backArrow.setVisibility(GONE);
        forwardArrow.setVisibility(GONE);
    }

    @Override
    public void onClick(View v) {
        switch (nodeType) {
            case ROOT_COLLAPSED:
                onHierarchyViewClickListener.onRootExpandClicked();
                break;
            case ROOT_EXPANDED:
                onHierarchyViewClickListener.onRootCollapseClicked();
                break;
            case GO_BACK:
                onHierarchyViewClickListener.onSwitchToNodeClicked(hierarchyNode);
                break;
            case GO_FORWARD:
                onHierarchyViewClickListener.onSwitchToNodeClicked(hierarchyNode);
                break;
            case CURRENT:
                onHierarchyViewClickListener.onBrowseNodeClicked(hierarchyNode);
                break;
            case LEAF:
                onHierarchyViewClickListener.onBrowseNodeClicked(hierarchyNode);
                break;
        }
    }
}
