package com.kamil.exampleapp.widget.draweroptionview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kamil.exampleapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

public class DrawerOptionView extends LinearLayout {

    @BindView(R.id.view_drawer_option_image) ImageView iconImageView;
    @BindView(R.id.view_drawer_option_name) TextView nameTextView;

    private int optionIconResId;
    private int optionIconWidthPx;
    private int optionIconHeightPx;
    private String optionName;
    private String optionfontPath;
    private int optionfontColorResId;
    private int optionfontSizePx;

    public DrawerOptionView(Context context) {
        this(context, null);
    }

    public DrawerOptionView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DrawerOptionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public DrawerOptionView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs, defStyleAttr);
    }

    private void init(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        final int defaultIconSize = getResources()
                .getDimensionPixelSize(R.dimen.view_drawer_option_image_size);
        final int defaultFontSize = getResources()
                .getDimensionPixelSize(R.dimen.view_drawer_option_text_size);
        final TypedArray typedArray = context.obtainStyledAttributes
                (attrs, R.styleable.DrawerOptionView, defStyleAttr, 0);

        try {
            optionIconResId = typedArray.getResourceId(R.styleable.DrawerOptionView_option_icon, 0);
            optionIconWidthPx = typedArray.getDimensionPixelSize
                    (R.styleable.DrawerOptionView_option_icon_width, defaultIconSize);
            optionIconHeightPx = typedArray.getDimensionPixelSize
                    (R.styleable.DrawerOptionView_option_icon_height, defaultIconSize);

            optionfontPath = typedArray.getString(R.styleable.DrawerOptionView_option_font_path);
            optionName = typedArray.getString(R.styleable.DrawerOptionView_option_name);
            optionfontColorResId = typedArray.getColor
                    (R.styleable.DrawerOptionView_option_font_color, Color.BLACK);
            optionfontSizePx = typedArray.getDimensionPixelSize
                    (R.styleable.DrawerOptionView_option_font_size, defaultFontSize);
        } finally {
            if (typedArray != null) typedArray.recycle();
        }

        inflate(context, R.layout.view_drawer_option, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (isInEditMode()) {
            return;
        }
        ButterKnife.bind(this);
        setupView();
    }

    private void setupView() {
        nameTextView.setText(optionName);
        nameTextView.setTypeface(TypefaceUtils.load(getContext().getAssets(), optionfontPath));
        nameTextView.setTextColor(optionfontColorResId);
        nameTextView.setTextSize(optionfontSizePx);

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) iconImageView.getLayoutParams();
        params.width = optionIconWidthPx;
        params.height = optionIconHeightPx;
        iconImageView.setLayoutParams(params);
        iconImageView.setImageResource(optionIconResId);
    }
}
