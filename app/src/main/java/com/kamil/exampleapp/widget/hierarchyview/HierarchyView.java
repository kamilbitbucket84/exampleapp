package com.kamil.exampleapp.widget.hierarchyview;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.kamil.exampleapp.screen.main.model.HierarchyNode;
import com.kamil.exampleapp.screen.main.model.HierarchyTree;

import java.util.List;

import static com.kamil.exampleapp.widget.hierarchyview.HierarchyNodeView.NODE_TYPE.CURRENT;
import static com.kamil.exampleapp.widget.hierarchyview.HierarchyNodeView.NODE_TYPE.GO_BACK;
import static com.kamil.exampleapp.widget.hierarchyview.HierarchyNodeView.NODE_TYPE.GO_FORWARD;
import static com.kamil.exampleapp.widget.hierarchyview.HierarchyNodeView.NODE_TYPE.LEAF;
import static com.kamil.exampleapp.widget.hierarchyview.HierarchyNodeView.NODE_TYPE.ROOT_COLLAPSED;
import static com.kamil.exampleapp.widget.hierarchyview.HierarchyNodeView.NODE_TYPE.ROOT_EXPANDED;

public class HierarchyView extends LinearLayout {

    private OnHierarchyViewClickListener onHierarchyViewClickListener = OnHierarchyViewClickListener.EMPTY;

    public HierarchyView(Context context) {
        this(context, null);
    }

    public HierarchyView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HierarchyView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public HierarchyView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        setOrientation(VERTICAL);
    }

    public void setOnHierarchyViewClickListener(OnHierarchyViewClickListener listener) {
        this.onHierarchyViewClickListener = listener;
    }

    public void recreateView(HierarchyTree hierarchyTree) {
        removeAllViews();
        addPreviousNodeViews(hierarchyTree);
        addCurrentNodeView(hierarchyTree);
        addChildrenNodeViews(hierarchyTree);
    }

    private void addPreviousNodeViews(HierarchyTree hierarchyTree) {
        HierarchyNode currentNode = hierarchyTree.getCurrentNode();
        List<HierarchyNode> previousNodes = currentNode.getPreviousNodes();

        if (!previousNodes.isEmpty()) {
            for (HierarchyNode node : previousNodes) {
                addNodeView(node, GO_BACK);
            }
        }
    }

    private void addCurrentNodeView(HierarchyTree hierarchyTree) {
        HierarchyNode currentNode = hierarchyTree.getCurrentNode();

        if (currentNode.isRoot() && hierarchyTree.isRootExpanded()) {
            addNodeView(currentNode, ROOT_EXPANDED);
        } else if (currentNode.isRoot() && !hierarchyTree.isRootExpanded() && currentNode.hasChildren()) {
            addNodeView(currentNode, ROOT_COLLAPSED);
        } else if (currentNode.isRoot() && !hierarchyTree.isRootExpanded() && !currentNode.hasChildren()) {
            addNodeView(currentNode, LEAF);
        } else {
            addNodeView(currentNode, CURRENT);
        }
    }

    private void addChildrenNodeViews(HierarchyTree hierarchyTree) {
        HierarchyNode currentNode = hierarchyTree.getCurrentNode();
        List<HierarchyNode> children = currentNode.getChildren();

        if (!currentNode.isRoot() || (currentNode.isRoot() && hierarchyTree.isRootExpanded())) {
            for (HierarchyNode node : children) {
                if (node.hasChildren()) {
                    addNodeView(node, GO_FORWARD);
                } else {
                    addNodeView(node, LEAF);
                }
            }
        }
    }

    private void addNodeView(HierarchyNode node, HierarchyNodeView.NODE_TYPE type) {
        HierarchyNodeView view = new HierarchyNodeView(getContext());
        view.prepareUI(node, type);
        view.setOnHierarchyViewClickListener(onHierarchyViewClickListener);
        addView(view);
    }
}
