package com.kamil.exampleapp.widget.hierarchyview;


import com.kamil.exampleapp.screen.main.model.HierarchyNode;

public interface OnHierarchyViewClickListener {

    void onRootExpandClicked();

    void onRootCollapseClicked();

    void onSwitchToNodeClicked(HierarchyNode hierarchyNode);

    void onBrowseNodeClicked(HierarchyNode hierarchyNode);

    OnHierarchyViewClickListener EMPTY = new OnHierarchyViewClickListener() {
        @Override
        public void onRootExpandClicked() {
        }

        @Override
        public void onRootCollapseClicked() {
        }

        @Override
        public void onSwitchToNodeClicked(HierarchyNode hierarchyNode) {

        }

        @Override
        public void onBrowseNodeClicked(HierarchyNode hierarchyNode) {
        }
    };
}
