package com.kamil.exampleapp.widget.drawerview;

import static com.kamil.exampleapp.widget.drawerview.DrawerView.*;

public interface OnDrawerViewItemClickListener {

    void onDrawerViewItemClicked(Option option);

    OnDrawerViewItemClickListener EMPTY = option -> {
    };
}
