package com.kamil.exampleapp.widget.drawerview;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.ScrollView;

import com.kamil.exampleapp.R;
import com.kamil.exampleapp.widget.draweroptionview.DrawerOptionView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DrawerView extends ScrollView {

    public enum Option {
        TUTORIAL,
        STYLING,
        EXPERIMENT,
        MULTIFRAGMENTS,
        MULTITAB
    }

    @BindView(R.id.view_drawer_tutorial) DrawerOptionView tutorialItem;
    @BindView(R.id.view_drawer_styling) DrawerOptionView stylingItem;
    @BindView(R.id.view_drawer_experiment) DrawerOptionView experimentItem;
    @BindView(R.id.view_drawer_multifragments) DrawerOptionView loginItem;

    private OnDrawerViewItemClickListener drawerViewItemClickListener = OnDrawerViewItemClickListener.EMPTY;

    public DrawerView(Context context) {
        this(context, null);
    }

    public DrawerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DrawerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public DrawerView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.view_drawer, this);
    }

    public void setOnDrawerViewItemClickListener(OnDrawerViewItemClickListener listener) {
        this.drawerViewItemClickListener = listener;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (isInEditMode()) {
            return;
        }
        ButterKnife.bind(this);
        setClickable(true);
        setBackgroundColor(ContextCompat.getColor(this.getContext(), R.color.view_drawer_background));
    }

    @OnClick(R.id.view_drawer_tutorial)
    protected void tutorialItemClicked() {
        drawerViewItemClickListener.onDrawerViewItemClicked(Option.TUTORIAL);
    }

    @OnClick(R.id.view_drawer_styling)
    protected void stylingItemClicked() {
        drawerViewItemClickListener.onDrawerViewItemClicked(Option.STYLING);
    }

    @OnClick(R.id.view_drawer_experiment)
    protected void experimentClicked() {
        drawerViewItemClickListener.onDrawerViewItemClicked(Option.EXPERIMENT);
    }

    @OnClick(R.id.view_drawer_multifragments)
    protected void multiFragmentClicked() {
        drawerViewItemClickListener.onDrawerViewItemClicked(Option.MULTIFRAGMENTS);
    }

    @OnClick(R.id.view_drawer_multitabs)
    protected void multiTabClicked() {
        drawerViewItemClickListener.onDrawerViewItemClicked(Option.MULTITAB);
    }
}
