package com.kamil.exampleapp.widget.createproductview;


public interface OnCreateProductViewListener {

    void onCreateProductCancelClicked();
    void onCreateProductCreateClicked();

    OnCreateProductViewListener EMPTY = new OnCreateProductViewListener() {
        @Override
        public void onCreateProductCancelClicked() {
        }

        @Override
        public void onCreateProductCreateClicked() {
        }
    };
}
