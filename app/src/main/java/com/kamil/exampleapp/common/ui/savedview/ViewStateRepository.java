package com.kamil.exampleapp.common.ui.savedview;

import java.util.HashMap;

/**
 * Created by Kamil on 02/10/2017.
 */

public interface ViewStateRepository {

    void saveViewState(String key, HashMap<String, Object> stateMap);

    void clearViewState(String key);

    HashMap<String, Object> getViewState(String key);
}