package com.kamil.exampleapp.common.notification;

/**
 * Created by Kamil on 11/05/2017.
 */

public class TimeEventNotificationData {

    public final int eventId;
    public final int iconResId;
    public final String titleText;
    public final String contentText;

    private TimeEventNotificationData(Builder builder) {
        eventId = builder.eventId;
        iconResId = builder.iconResId;
        titleText = builder.titleText;
        contentText = builder.contentText;
    }

    public static final class Builder {
        private int eventId;
        private int iconResId;
        private String titleText;
        private String contentText;

        public Builder() {
        }

        public Builder eventId(int eventId) {
            this.eventId = eventId;
            return this;
        }

        public Builder iconResId(int iconResId) {
            this.iconResId = iconResId;
            return this;
        }

        public Builder titleText(String titleText) {
            this.titleText = titleText;
            return this;
        }

        public Builder contentText(String contentText) {
            this.contentText = contentText;
            return this;
        }

        public TimeEventNotificationData build() {
            return new TimeEventNotificationData(this);
        }
    }
}
