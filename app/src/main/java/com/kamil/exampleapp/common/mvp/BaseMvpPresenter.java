package com.kamil.exampleapp.common.mvp;

import android.support.annotation.CallSuper;

import java.lang.ref.WeakReference;

import rx.subscriptions.CompositeSubscription;


public abstract class BaseMvpPresenter<V extends MvpView> implements MvpPresenter<V> {

    protected final CompositeSubscription subscriptions = new CompositeSubscription();
    private WeakReference<V> weakView;

    @CallSuper
    @Override
    public void attachView(V view) {
        this.weakView = new WeakReference<>(view);
    }

    @CallSuper
    @Override
    public void detachView() {
        subscriptions.clear();

        if (isViewAttached()) {
            weakView.clear();
            weakView = null;
        }
    }

    @Override
    public final boolean isViewAttached() {
        return weakView != null && weakView.get() != null;
    }

    @Override
    public final V getView() {
        return weakView.get();
    }
}
