package com.kamil.exampleapp.common.notification;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.widget.RemoteViews;

import com.kamil.exampleapp.R;
import com.kamil.exampleapp.screen.experiment.view.ExperimentActivity;
import com.squareup.picasso.Picasso;

/**
 * Created by Kamil on 11/05/2017.
 */

public class ExampleNotificationCreator implements NotificationCreator {

    public static final String PROXIMITY_NOTIFICATION_ACTION_ONE = "PROXIMITY_NOTIFICATION_ACTION_ONE";
    public static final String PROXIMITY_NOTIFICATION_ACTION_TWO = "PROXIMITY_NOTIFICATION_ACTION_TWO";
    public static final String PROXIMITY_NOTIFICATION_ACTION_THREE = "PROXIMITY_NOTIFICATION_ACTION_THREE";
    public static final String PROXIMITY_NOTIFICATION_ACTION_FOUR = "PROXIMITY_NOTIFICATION_ACTION_FOUR";
    private static final int PROXIMITY_NOTIFICATION_REQUEST_CODE_ONE = 10;
    private static final int PROXIMITY_NOTIFICATION_REQUEST_CODE_TWO = 20;
    private static final int PROXIMITY_NOTIFICATION_REQUEST_CODE_THREE = 30;
    private static final int PROXIMITY_NOTIFICATION_REQUEST_CODE_FOUR = 40;

    private Context context;
    private NotificationManagerCompat notificationManagerCompat;
    private NotificationParams notificationParams;
    private NotificationCompat.Builder timeNotificationBuilder;
    private NotificationCompat.Builder proximityNotificationBuilder;

    public ExampleNotificationCreator(Context context, NotificationParams notificationParams) {
        notificationManagerCompat = NotificationManagerCompat.from(context);
        this.context = context;
        this.notificationParams = notificationParams;
    }

    @Override
    public void createOrUpdateTimeEventNotification(TimeEventNotificationData notificationData) {
        if (timeNotificationBuilder == null) {

            Intent intent = ExperimentActivity.getIntent(context, 100L);
            PendingIntent timeNotificationAction = PendingIntent.getActivity
                    (context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

            timeNotificationBuilder = new NotificationCompat.Builder(context)
                    .setPriority(notificationParams.priority)
                    .setAutoCancel(notificationParams.autoCancel)
                    .setContentIntent(timeNotificationAction)
                    .setSmallIcon(R.drawable.new_product)
                    .setOnlyAlertOnce(true)
                    .setContentTitle(notificationData.titleText)
                    .setContentText(notificationData.contentText);
        } else {
            timeNotificationBuilder.setContentText(notificationData.contentText);
        }

        notificationManagerCompat.notify(notificationData.eventId, timeNotificationBuilder.build());
    }

    @Override
    public void createOrUpdateProximityEventNotification(ProximityEventNotificationData notificationData) {
        RemoteViews smallView = createProximitySmallContentView(notificationData);
        RemoteViews bigView = createProximityBigContentView(notificationData);

        if (proximityNotificationBuilder == null) {
            proximityNotificationBuilder = new NotificationCompat.Builder(context)
                    .setPriority(notificationParams.priority)
                    .setOnlyAlertOnce(true)
                    .setSmallIcon(R.drawable.product)
                    .setAutoCancel(notificationParams.autoCancel)
                    .setCustomContentView(smallView)
                    .setCustomBigContentView(bigView);
        }

        Notification notification = proximityNotificationBuilder.build();
        loadImagesIntoProximityNotifications(smallView, bigView, notificationData, notification);
        notificationManagerCompat.notify(notificationData.id, notification);

    }

    private void loadImagesIntoProximityNotifications(RemoteViews smallView,
                                                      RemoteViews bigView,
                                                      ProximityEventNotificationData notificationData,
                                                      Notification notification) {
        Picasso.with(context)
                .load(notificationData.iconUrl)
                .into(smallView, R.id.notification_proximity_small_icon, notificationData.id, notification);

        Picasso.with(context)
                .load(notificationData.iconUrl)
                .into(bigView, R.id.notification_proximity_big_icon, notificationData.id, notification);
    }

    private RemoteViews createProximityBigContentView(ProximityEventNotificationData notificationData) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.notification_proximity_big);
        remoteViews.setTextViewText(R.id.notification_proximity_big_title, notificationData.title);
        remoteViews.setTextViewText(R.id.notification_proximity_big_date, notificationData.dateString);
        remoteViews.setTextViewText(R.id.notification_proximity_big_first_value, notificationData.firstValue);
        remoteViews.setTextViewText(R.id.notification_proximity_big_second_value, notificationData.secondValue);
        remoteViews.setTextViewText(R.id.notification_proximity_big_third_value, notificationData.thirdValue);

        configureProximityBigContentViewClickListeners(remoteViews);

        return remoteViews;
    }

    private void configureProximityBigContentViewClickListeners(RemoteViews remoteViews) {
        Intent actionOne = new Intent(PROXIMITY_NOTIFICATION_ACTION_ONE);
        PendingIntent optionOne = PendingIntent.getBroadcast
                (context, PROXIMITY_NOTIFICATION_REQUEST_CODE_ONE, actionOne, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.notification_proximity_big_option_one, optionOne);

        Intent actionTwo = new Intent(PROXIMITY_NOTIFICATION_ACTION_TWO);
        PendingIntent optionTwo = PendingIntent.getBroadcast
                (context, PROXIMITY_NOTIFICATION_REQUEST_CODE_TWO, actionTwo, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.notification_proximity_big_option_two, optionTwo);


        Intent actionThree = new Intent(PROXIMITY_NOTIFICATION_ACTION_THREE);
        PendingIntent optionThree = PendingIntent.getBroadcast
                (context, PROXIMITY_NOTIFICATION_REQUEST_CODE_THREE, actionThree, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.notification_proximity_big_option_three, optionThree);

        Intent actionFour = new Intent(PROXIMITY_NOTIFICATION_ACTION_FOUR);
        PendingIntent optionFour = PendingIntent.getBroadcast
                (context, PROXIMITY_NOTIFICATION_REQUEST_CODE_FOUR, actionFour, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.notification_proximity_big_option_four, optionFour);
    }

    private RemoteViews createProximitySmallContentView(ProximityEventNotificationData notificationData) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.notification_proximity_small);
        remoteViews.setTextViewText(R.id.notification_proximity_small_title, notificationData.title);
        remoteViews.setTextViewText(R.id.notification_proximity_small_date, notificationData.dateString);
        remoteViews.setTextViewText(R.id.notification_proximity_small_first_value, notificationData.firstValue);
        remoteViews.setTextViewText(R.id.notification_proximity_small_second_value, notificationData.secondValue);
        remoteViews.setTextViewText(R.id.notification_proximity_small_third_value, notificationData.thirdValue);

        return remoteViews;
    }

    @Override
    public void setNotificationParams(NotificationParams notificationParams) {
        this.notificationParams = notificationParams;
    }

    @Override
    public void onDestroy() {
        notificationManagerCompat = null;
        timeNotificationBuilder = null;
        context = null;
    }

    @Override
    public void cancelNotificationWithId(int notificationId) {
        notificationManagerCompat.cancel(notificationId);
    }

    @Override
    public void cancelAll() {
        notificationManagerCompat.cancelAll();
    }
}
