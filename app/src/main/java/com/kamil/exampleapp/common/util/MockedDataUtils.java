package com.kamil.exampleapp.common.util;

import com.kamil.exampleapp.common.list.ListItem;
import com.kamil.exampleapp.screen.browseproducts.model.ProductItem;
import com.kamil.exampleapp.screen.browseproducts.model.InFavoritesState;
import com.kamil.exampleapp.screen.browseproducts.model.RemovalState;
import com.kamil.exampleapp.screen.multifragment.view.ExampleItem;

import java.util.ArrayList;
import java.util.List;

public final class MockedDataUtils {

    private static final String EXAMPLE_PRODUCT_NAME = "example product --- ";
    private static final String EXAMPLE_COMPANY = "example company";
    private static final String EXAMPLE_PRICE = "24.55";
    public static final String EXAMPLE_PHOTO =
            "http://images.halloweencostumes.com/products/15959/1-1/child-silver-mirror-ninja-costume.jpg";
    public static final String EXAMPLE_THUMB = "http://atozvideo.com/wp-content/uploads/2014/01/cardboard-box-icon-512x5122-300x300.png";

    private MockedDataUtils() {
    }

    public static List<ListItem> createMockedProductItems() {
        List<ListItem> productItems = new ArrayList<>();

        ProductItem productItem;
        for (int i = 0; i < 10; i++) {
            productItem = new ProductItem.Builder()
                    .id(System.currentTimeMillis())
                    .name(EXAMPLE_PRODUCT_NAME + i)
                    .brand(EXAMPLE_COMPANY)
                    .price(EXAMPLE_PRICE)
                    .inFavoriteState(InFavoritesState.NOT_ADDED)
                    .removalState(RemovalState.NOT_REMOVED)
                    .build();
            productItems.add(productItem);
        }

        return productItems;
    }

    public static List<ListItem> createMockedExampleItems() {
        List<ListItem> exampleItems = new ArrayList<>();

        exampleItems.add(new ExampleItem("A", "A-input", true, 25));
        exampleItems.add(new ExampleItem("B", "B-input", false, 50));
        exampleItems.add(new ExampleItem("C", "C-input", true, 25));
        exampleItems.add(new ExampleItem("D", "D-input", false, 50));
        exampleItems.add(new ExampleItem("E", "E-input", true, 25));
        exampleItems.add(new ExampleItem("F", "F-input", false, 50));
        exampleItems.add(new ExampleItem("G", "G-input", true, 25));
        exampleItems.add(new ExampleItem("H", "H-input", false, 50));

        return exampleItems;
    }
}
