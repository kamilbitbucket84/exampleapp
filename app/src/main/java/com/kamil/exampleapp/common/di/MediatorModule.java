package com.kamil.exampleapp.common.di;

import com.kamil.exampleapp.service.event.mediator.EventGeneratorMediator;
import com.kamil.exampleapp.service.event.mediator.EventMediator;
import com.kamil.exampleapp.service.event.mediator.EventAdminMediator;
import com.kamil.exampleapp.service.event.mediator.EventReceiverMediator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class MediatorModule {

    @Singleton
    @Provides
    EventMediator provideEventServiceMediator() {
        return new EventAdminMediator();
    }

    @Singleton
    @Provides
    EventGeneratorMediator provideEventGeneratorMediator(EventMediator eventMediator) {
        return new EventGeneratorMediator(eventMediator);
    }

    @Singleton
    @Provides
    EventReceiverMediator provideEventReceiverMediator(EventMediator eventMediator) {
        return new EventReceiverMediator(eventMediator);
    }
}
