package com.kamil.exampleapp.common.util;

import android.content.pm.PackageManager;

public final class PermissionUtils {

    private PermissionUtils() {
    }

    public static boolean checkIfPermissionGranted(int[] grantResults) {
        return grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED;
    }
}
