package com.kamil.exampleapp.common.di;

import android.app.Application;

public class Dagger {

    private static AppComponent appComponent;

    public static void init(Application application) {
        if (appComponent == null) {
            appComponent = DaggerAppComponent.builder()
                    .appModule(new AppModule(application))
                    .serviceModule(new ServiceModule(false))
                    .dataSourceModule(new DataSourceModule())
                    .schedulersModule(new SchedulersModule())
                    .mediatorModule(new MediatorModule())
                    .build();
            appComponent.inject(application);
        }
    }

    public static AppComponent appComponent() {
        return appComponent;
    }

    private Dagger() {
        throw new AssertionError("No instance");
    }
}