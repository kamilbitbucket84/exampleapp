package com.kamil.exampleapp.common.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.text.TextUtils;
import android.widget.ImageView;

import com.kamil.exampleapp.common.ui.ImageRatioScaleTransformation;
import com.squareup.picasso.Picasso;

public class ImageUtils {

    private ImageUtils() {
    }

    public static void loadImageSafelyCenterInside(String imageUrl,
                                                   ImageView imageView) {
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.with(imageView.getContext())
                    .load(imageUrl)
                    .fit()
                    .centerInside()
                    .into(imageView);
        } else {
            imageView.setImageBitmap(null);
        }
    }

    public static void loadImageSafelyCenterInside(int imageResId,
                                                   ImageView imageView) {
        if (imageResId != 0) {
            Picasso.with(imageView.getContext())
                    .load(imageResId)
                    .fit()
                    .centerInside()
                    .into(imageView);
        } else {
            imageView.setImageBitmap(null);
        }
    }

    public static void loadImageSafelyCenterCrop(int imageResId, ImageView imageView) {
        if (imageResId != 0) {
            Picasso.with(imageView.getContext())
                    .load(imageResId)
                    .fit()
                    .centerCrop()
                    .into(imageView);
        } else {
            imageView.setImageBitmap(null);
        }
    }

    public static void loadImageSafelyCenterCrop(String imageUrl, ImageView imageView) {
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.with(imageView.getContext())
                    .load(imageUrl)
                    .fit()
                    .centerCrop()
                    .into(imageView);
        } else {
            imageView.setImageBitmap(null);
        }
    }

    public static void loadImageSafelyWithImageRatioScaleTransformation(int imageViewWidth,
                                                                        String imageUrl,
                                                                        ImageView imageView) {
        if (!TextUtils.isEmpty(imageUrl)) {
            Picasso.with(imageView.getContext())
                    .load(imageUrl)
                    .transform(new ImageRatioScaleTransformation(imageViewWidth, imageUrl))
                    .into(imageView);
        } else {
            imageView.setImageBitmap(null);
        }
    }

    public static void colorDrawable(Context context, Drawable drawable, int colorResId) {
        drawable = DrawableCompat.wrap(drawable);
        drawable.mutate();
        DrawableCompat.setTint(drawable, ContextCompat.getColor(context, colorResId));
    }
}
