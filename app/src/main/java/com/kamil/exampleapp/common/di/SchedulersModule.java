package com.kamil.exampleapp.common.di;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@Module
public class SchedulersModule {

    @Provides
    @Singleton
    Scheduler provideAndroidScheduler() {
        return AndroidSchedulers.mainThread();
    }
}
