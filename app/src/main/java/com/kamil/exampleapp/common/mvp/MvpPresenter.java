package com.kamil.exampleapp.common.mvp;


public interface MvpPresenter<V extends MvpView> {

    void attachView(V view);

    void detachView();

    boolean isViewAttached();

    V getView();
}
