package com.kamil.exampleapp.common.usecase;

import rx.Observable;

public interface UseCase<T> {
    Observable<T> execute();
}
