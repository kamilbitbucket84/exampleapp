package com.kamil.exampleapp.common.util;

import com.kamil.exampleapp.datasource.api.model.HierarchyNodeEntity;
import com.kamil.exampleapp.datasource.api.mapper.HierarchyNodeMapper;
import com.kamil.exampleapp.screen.main.model.HierarchyNode;
import com.kamil.exampleapp.screen.main.model.HierarchyPack;

import java.util.ArrayList;
import java.util.List;

public final class ConvertUtils {

    private ConvertUtils() {
    }

    public static HierarchyPack createHierarchyPack(HierarchyNode currentNode) {
        HierarchyNodeMapper mapper = new HierarchyNodeMapper();

        List<HierarchyNodeEntity> parentEntities = new ArrayList<>();
        for (HierarchyNode node : currentNode.getPreviousNodes()) {
            parentEntities.add(mapper.from(node));
        }

        HierarchyPack hierarchyPack = new HierarchyPack();
        hierarchyPack.parentEntities = parentEntities;
        hierarchyPack.currentEntity = mapper.from(currentNode);

        return hierarchyPack;
    }
}
