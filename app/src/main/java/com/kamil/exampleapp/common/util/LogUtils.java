package com.kamil.exampleapp.common.util;

import android.support.annotation.NonNull;
import android.util.Log;

import com.kamil.exampleapp.BuildConfig;

import java.util.List;

public class LogUtils {
    private static final String TAG = "EXAMPLE_APP_TAG";

    private LogUtils() {
    }

    public static void d(String message) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, message + "\n");
        }
    }

    public static void d(String format, Object... args) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, String.format(format, args));
        }
    }

    public static void e(String message) {
        if (BuildConfig.DEBUG) {
            Log.e(TAG, message);
        }
    }

    public static void e(@NonNull Throwable e, String message) {
        if (BuildConfig.DEBUG) {
            Log.e(TAG, message, e);
        }
    }

    public static void e(@NonNull Throwable e) {
        if (BuildConfig.DEBUG) {
            Log.e(TAG, e.getMessage(), e);
        }
    }

    public static<T> void logList(List<T> objects) {
        for (T object: objects) {
            LogUtils.d(object.toString());
        }
    }
}
