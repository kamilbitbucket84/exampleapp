package com.kamil.exampleapp.common;

import android.app.Application;
import android.content.Intent;

import com.kamil.exampleapp.R;
import com.kamil.exampleapp.common.di.Dagger;
import com.kamil.exampleapp.service.event.EventService;
import com.kamil.exampleapp.common.util.ServiceUtils;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Dagger.init(this);
        configureDefaultFont();
        startExperimentServiceIfNeeded();
    }

    private void configureDefaultFont() {
        CalligraphyConfig.initDefault
                (new CalligraphyConfig.Builder()
                        .setDefaultFontPath(getString(R.string.font_open_sans_regular))
                        .setFontAttrId(R.attr.fontPath)
                        .build()
                );
    }

    private void startExperimentServiceIfNeeded() {
        if (!ServiceUtils.isServiceRunning(this, EventService.class.getName())) {
            Intent intent = new Intent(this, EventService.class);
            startService(intent);
        }
    }
}
