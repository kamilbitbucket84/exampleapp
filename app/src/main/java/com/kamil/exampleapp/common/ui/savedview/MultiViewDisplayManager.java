package com.kamil.exampleapp.common.ui.savedview;

import android.view.View;
import android.view.ViewGroup;

import java.util.HashMap;

/**
 * Created by Kamil on 26/09/2017.
 */

public class MultiViewDisplayManager {

    private HashMap<String, SavedStateView> savableViewsMap = new HashMap<>();
    private ViewGroup viewContainer;

    public MultiViewDisplayManager(ViewGroup viewContainer) {
        this.viewContainer = viewContainer;
    }

    public void clear() {
        viewContainer.removeAllViews();
        viewContainer = null;
        savableViewsMap.clear();
    }

    public void displayView(SavedStateView savableView, String savedStateKey) {
        if (!savableViewsMap.containsKey(savedStateKey)) {
            savableViewsMap.clear();
            viewContainer.removeAllViews();
            savableView.setStateKey(savedStateKey);
            savableViewsMap.put(savedStateKey, savableView);
            View view = (View) savableView;
            viewContainer.addView(view);
        }
    }
}
