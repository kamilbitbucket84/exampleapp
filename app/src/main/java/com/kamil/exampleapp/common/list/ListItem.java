package com.kamil.exampleapp.common.list;

public interface ListItem {

    int SUBCATEGORY = 1;
    int PRODUCT = 2;
    int EXAMPLE = 3;

    int getItemViewType();
}
