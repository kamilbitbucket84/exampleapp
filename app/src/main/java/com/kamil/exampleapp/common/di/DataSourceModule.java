package com.kamil.exampleapp.common.di;

import android.content.Context;

import com.kamil.exampleapp.datasource.api.FirebaseApiClient;
import com.kamil.exampleapp.datasource.api.FirebaseRetrofitApiClient;
import com.kamil.exampleapp.datasource.api.FirebaseService;
import com.kamil.exampleapp.datasource.database.ExampleDatabaseClient;
import com.kamil.exampleapp.datasource.database.ExampleSQLiteDatabaseClient;
import com.kamil.exampleapp.datasource.prefs.SettingsClient;
import com.kamil.exampleapp.datasource.prefs.SettingsInPrefsClient;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class DataSourceModule {

    @Singleton
    @Provides
    FirebaseApiClient provideApiClient(FirebaseService firebaseService) {
        return new FirebaseRetrofitApiClient(firebaseService);
    }

    @Singleton
    @Provides
    SettingsClient provideSettingsClient(Context context) {
        return new SettingsInPrefsClient(context);
    }


    @Singleton
    @Provides
    ExampleDatabaseClient provideDataBaseClient(Context context) {
        return new ExampleSQLiteDatabaseClient(context);
    }
}
