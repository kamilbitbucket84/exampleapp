package com.kamil.exampleapp.common.mapper;


public interface Mapper<T,V> {
    V from(T from);
}
