package com.kamil.exampleapp.common.ui.savedview;

import java.util.HashMap;

/**
 * Created by Kamil on 28/09/2017.
 */

public enum EnumViewStateRepository implements ViewStateRepository {

    INSTANCE(new HashMap<>());

    public final HashMap<String, HashMap<String, Object>> appUIStateMap;

    EnumViewStateRepository(HashMap<String, HashMap<String, Object>> appUIStateMap) {
        this.appUIStateMap = appUIStateMap;
    }

    public static final String MULTI_VIEW_ACTIVITY_STATE = "MULTI_VIEW_ACTIVITY_STATE";

    public static final String FIRST_VIEW_MULTI_STATE = "FIRST_VIEW_MULTI_STATE";
    public static final String SECOND_VIEW_MULTI_STATE = "SECOND_VIEW_MULTI_STATE";
    public static final String THIRD_VIEW_MULTI_STATE = "THIRD_VIEW_MULTI_STATE";
    public static final String FOURTH_VIEW_MULTI_STATE = "FOURTH_VIEW_MULTI_STATE";

    public static final String FIRST_VIEW_PAGER_STATE = "FIRST_VIEW_PAGER_STATE";
    public static final String SECOND_VIEW_PAGER_STATE = "SECOND_VIEW_PAGER_STATE";
    public static final String THIRD_VIEW_PAGER_STATE = "THIRD_VIEW_PAGER_STATE";
    public static final String FOURTH_VIEW_PAGER_STATE = "FOURTH_VIEW_PAGER_STATE";

    @Override
    public void saveViewState(String key, HashMap<String, Object> stateMap) {
        if (stateMap == null) {
            throw new IllegalArgumentException("cannot save when state map is NULL");
        }

        if (key == null) {
            throw new IllegalArgumentException("cannot save when key is NULL");
        }

        appUIStateMap.put(key, stateMap);
    }

    @Override
    public void clearViewState(String key) {
        if (key == null) {
            throw new IllegalArgumentException("cannot clear state when key is NULL");
        }
        appUIStateMap.remove(key);
    }

    @Override
    public HashMap<String, Object> getViewState(String key) {
        if (key == null) {
            throw new IllegalArgumentException("cannot get state when key is NULL");
        }
        return appUIStateMap.get(key);
    }
}
