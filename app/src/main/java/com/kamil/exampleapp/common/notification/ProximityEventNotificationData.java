package com.kamil.exampleapp.common.notification;

/**
 * Created by Kamil on 11/05/2017.
 */

public class ProximityEventNotificationData {

    public final Integer id;
    public final String iconUrl;
    public final String title;
    public final String dateString;
    public final String firstValue;
    public final String secondValue;
    public final String thirdValue;

    private ProximityEventNotificationData(Builder builder) {
        id = builder.id;
        iconUrl = builder.iconUrl;
        title = builder.title;
        dateString = builder.dateString;
        firstValue = builder.firstValue;
        secondValue = builder.secondValue;
        thirdValue = builder.thirdValue;
    }


    public static final class Builder {
        private Integer id;
        private String iconUrl;
        private String title;
        private String dateString;
        private String firstValue;
        private String secondValue;
        private String thirdValue;

        public Builder() {
        }

        public Builder id(Integer id) {
            this.id = id;
            return this;
        }

        public Builder iconUrl(String iconUrl) {
            this.iconUrl = iconUrl;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder dateString(String dateString) {
            this.dateString = dateString;
            return this;
        }

        public Builder firstValue(String firstValue) {
            this.firstValue = firstValue;
            return this;
        }

        public Builder secondValue(String secondValue) {
            this.secondValue = secondValue;
            return this;
        }

        public Builder thirdValue(String thirdValue) {
            this.thirdValue = thirdValue;
            return this;
        }

        public ProximityEventNotificationData build() {
            return new ProximityEventNotificationData(this);
        }
    }
}
