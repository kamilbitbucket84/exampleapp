package com.kamil.exampleapp.common.notification;

/**
 * Created by Kamil on 11/05/2017.
 */

public interface NotificationCreator {

    void createOrUpdateTimeEventNotification(TimeEventNotificationData notificationData);

    void createOrUpdateProximityEventNotification(ProximityEventNotificationData notificationData);

    void setNotificationParams(NotificationParams notificationParams);

    void onDestroy();

    void cancelNotificationWithId(int notificationId);

    void cancelAll();
}
