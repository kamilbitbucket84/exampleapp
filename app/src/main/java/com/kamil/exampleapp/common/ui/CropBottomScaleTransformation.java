package com.kamil.exampleapp.common.ui;

import android.graphics.Bitmap;

import com.squareup.picasso.Transformation;

/**
 * Created by Kamil on 26/04/2017.
 */
public class CropBottomScaleTransformation implements Transformation {

    private String imageUrl;
    private int targetWidth;
    private int targetHeight;

    public CropBottomScaleTransformation(int targetWidth, int targetHeight, String imageUrl) {
        this.targetWidth = targetWidth;
        this.targetHeight = targetHeight;
        this.imageUrl = imageUrl;
    }

    @Override
    public Bitmap transform(Bitmap source) {
        double loadedImageRatio = (double) source.getHeight() / (double) source.getWidth();
        int scaledTargetHeight = (int) (targetWidth * loadedImageRatio);

        Bitmap scaledBitmap = Bitmap.createScaledBitmap(source, targetWidth, scaledTargetHeight, false);
        Bitmap croppedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, targetWidth, targetHeight);


        if(croppedBitmap != source) {
            source.recycle();
        }

        if(croppedBitmap != scaledBitmap) {
            scaledBitmap.recycle();
        }

        return croppedBitmap;
    }

    @Override
    public String key() {
        StringBuilder builder = new StringBuilder()
                .append("CropBottomScale: ")
                .append("width: ")
                .append(targetWidth)
                .append("height: ")
                .append(targetHeight)
                .append(" URL: ")
                .append(imageUrl);

        return builder.toString();
    }
}
