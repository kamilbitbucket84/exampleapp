package com.kamil.exampleapp.common.ui;

import android.graphics.Bitmap;

import com.squareup.picasso.Transformation;

public class ImageRatioScaleTransformation implements Transformation {

    private String imageUrl;
    private int targetWidth;
    private int targetHeight;

    public ImageRatioScaleTransformation(int targetWidth, String imageUrl) {
        this.targetWidth = targetWidth;
        this.imageUrl = imageUrl;
    }

    @Override
    public Bitmap transform(Bitmap source) {
        double sizeRatio = (double) source.getHeight() / (double) source.getWidth();
        targetHeight = (int) (targetWidth * sizeRatio);
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
        if (scaledBitmap != source) {
            source.recycle();
        }

        return scaledBitmap;
    }

    @Override
    public String key() {
        StringBuilder builder = new StringBuilder()
                .append("ImageRatioScale: ")
                .append("width: ")
                .append(targetWidth)
                .append("height: ")
                .append(targetHeight)
                .append(" URL: ")
                .append(imageUrl);

        return builder.toString();
    }
}