package com.kamil.exampleapp.common.notification;

/**
 * Created by Kamil on 11/05/2017.
 */

public class NotificationParams {

    public final boolean autoCancel;
    public final int priority;

    public NotificationParams(boolean autoCancel, int priority) {
        this.autoCancel = autoCancel;
        this.priority = priority;
    }
}
