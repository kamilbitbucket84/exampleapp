package com.kamil.exampleapp.common.di;

import com.google.gson.Gson;
import com.kamil.exampleapp.datasource.api.TestApiHeaders;
import com.kamil.exampleapp.datasource.api.FirebaseService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ServiceModule {

    private final boolean loggingEnabled;

    public ServiceModule(boolean loggingEnabled) {
        this.loggingEnabled = loggingEnabled;
    }

    @Singleton
    @Provides
    public OkHttpClient provideOkHttpClient() {
        if (loggingEnabled) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);

            return new OkHttpClient.Builder()
                    .addInterceptor(loggingInterceptor)
                    .addInterceptor(TestApiHeaders.INSTANCE.getApiHeadersInterceptor())
                    .build();
        } else {
            return new OkHttpClient.Builder()
                    .addInterceptor(TestApiHeaders.INSTANCE.getApiHeadersInterceptor())
                    .build();
        }
    }

    @Singleton
    @Provides
    public Gson provideGson() {
        return new Gson();
    }

    @Singleton
    @Provides
    FirebaseService provideFirebaseService(OkHttpClient okHttpClient, Gson gson) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(FirebaseService.BASE_ENDPOINT)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(FirebaseService.class);
    }
}
