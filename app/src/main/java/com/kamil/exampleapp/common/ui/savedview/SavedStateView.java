package com.kamil.exampleapp.common.ui.savedview;

/**
 * Created by Kamil on 28/09/2017.
 */

public interface SavedStateView {

    void setStateKey(String key);

    void saveState();

    void restoreState();
}
