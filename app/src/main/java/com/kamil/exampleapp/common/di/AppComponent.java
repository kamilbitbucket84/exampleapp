package com.kamil.exampleapp.common.di;

import android.app.Application;
import android.content.Context;

import com.kamil.exampleapp.datasource.prefs.SettingsClient;
import com.kamil.exampleapp.datasource.prefs.SettingsInPrefsClient;
import com.kamil.exampleapp.datasource.api.FirebaseApiClient;
import com.kamil.exampleapp.datasource.database.ExampleDatabaseClient;
import com.kamil.exampleapp.service.event.mediator.EventGeneratorMediator;
import com.kamil.exampleapp.service.event.mediator.EventReceiverMediator;

import javax.inject.Singleton;

import dagger.Component;
import rx.Scheduler;

@Singleton
@Component(modules = {
        AppModule.class,
        ServiceModule.class,
        DataSourceModule.class,
        SchedulersModule.class,
        MediatorModule.class
})
public interface AppComponent {

    void inject(Application application);

    Context provideContext();

    SettingsClient provideSettingsClient();

    EventGeneratorMediator provideEventGeneratorMediator();

    EventReceiverMediator provideEventReceiverMediator();

    FirebaseApiClient provideFireBaseApiClient();

    ExampleDatabaseClient provideExampleDatabaseClient();

    Scheduler provideAndroidScheduler();
}
