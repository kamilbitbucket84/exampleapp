package com.kamil.exampleapp.screen.experiment.view;

import com.kamil.exampleapp.common.mvp.MvpView;

public interface ExperimentMvpView extends MvpView {

    void askForAccessExternalStoragePermission();

    void askForAccessFineLocationPermission();

    void onPermissionNeverAskAgainClicked(String title, String message);

    void onStartProximityEventsLauncher();

    void onStopProximityEventsLauncher();

    void onStartTimeEventsLauncher();

    void onStopTimeEventsLauncher();

    void onConnectLaunchEventClient();

    void onDisconnectLaunchEventClient();
}
