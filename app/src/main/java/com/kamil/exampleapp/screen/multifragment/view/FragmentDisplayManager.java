package com.kamil.exampleapp.screen.multifragment.view;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import java.util.HashMap;

/**
 * Created by Kamil on 05/05/2017.
 */

public class FragmentDisplayManager {

    private int containerViewResId;
    private FragmentManager fragmentManager;
    private HashMap<String, Fragment> fragmentMap = new HashMap<>();

    public FragmentDisplayManager(FragmentManager fragmentManager, int containerViewResId) {
        this.fragmentManager = fragmentManager;
        this.containerViewResId = containerViewResId;
    }

    public void onDestroy() {
        fragmentManager = null;
        fragmentMap.clear();
    }

    public void insertFragmentIntoContainer(Fragment fragment, String fragmentTag) {
        fragmentMap.put(fragmentTag, fragment);
    }

    public void showFragmentWithTag(String tag) {
        detachOtherFragmentsIfNeeded(tag);
        addFragmentIfNeeded(tag);
        attachFragmentIfNeeded(tag);
    }

    private void addFragmentIfNeeded(String tag) {
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        if (fragment == null) {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.add(containerViewResId, fragmentMap.get(tag), tag);
            ft.commit();
        }
    }

    private void attachFragmentIfNeeded(String tag) {
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        if (fragment != null && !fragment.isVisible()) {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.attach(fragment);
            ft.commit();
        }
    }

    private void detachOtherFragmentsIfNeeded(String tag) {
        for (String key : fragmentMap.keySet()) {
            if (!key.equals(tag)) {
                detachFragmentWithTagIfNeeded(key);
            }
        }
    }

    private void detachFragmentWithTagIfNeeded(String tag) {
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        if (fragment != null && fragment.isVisible()) {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.detach(fragment);
            ft.commit();
        }
    }
}
