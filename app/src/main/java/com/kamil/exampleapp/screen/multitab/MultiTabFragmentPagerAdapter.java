package com.kamil.exampleapp.screen.multitab;

import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.kamil.exampleapp.R;
import com.kamil.exampleapp.screen.multifragment.view.FifthFragment;
import com.kamil.exampleapp.screen.multifragment.view.FirstFragment;
import com.kamil.exampleapp.screen.multifragment.view.FourthFragment;
import com.kamil.exampleapp.screen.multifragment.view.SecondFragment;
import com.kamil.exampleapp.screen.multifragment.view.ThirdFragment;

/**
 * Created by Kamil on 08/05/2017.
 */

public class MultiTabFragmentPagerAdapter extends FragmentPagerAdapter {

    private enum Pages {
        FIRST(R.string.multitabs_tab_first),
        SECOND(R.string.multitabs_tab_second),
        THIRD(R.string.multitabs_tab_third),
        FOURTH(R.string.multitabs_tab_fourth),
        FIFTH(R.string.multitabs_tab_fifth);

        public final int pageTitleResId;

        Pages(int pageTitleResId) {
            this.pageTitleResId = pageTitleResId;
        }
    }

    private Resources resources;

    public MultiTabFragmentPagerAdapter(FragmentManager fm, Resources resources) {
        super(fm);
        this.resources = resources;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return FirstFragment.newInstance();
            case 1:
                return SecondFragment.newInstance();
            case 2:
                return ThirdFragment.newInstance();
            case 3:
                return FourthFragment.newInstance();
            default:
                return FifthFragment.newInstance();
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return resources.getString(Pages.values()[position].pageTitleResId);
    }

    @Override
    public int getCount() {
        return Pages.values().length;
    }

    public void onDestroy() {
        resources = null;
    }
}
