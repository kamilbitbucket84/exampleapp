package com.kamil.exampleapp.screen.multiviews.second;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.support.annotation.AttrRes;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.annotation.StyleRes;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.kamil.exampleapp.R;
import com.kamil.exampleapp.common.ui.savedview.EnumViewStateRepository;
import com.kamil.exampleapp.common.ui.savedview.SavedStateView;
import com.kamil.exampleapp.common.util.LogUtils;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kamil on 25/09/2017.
 */

public class SecondView extends FrameLayout implements SavedStateView {

    private static final String SECOND_INPUT_STATE = "SECOND_INPUT_STATE";

    @BindView(R.id.view_multi_view_second_input) EditText secondInput;

    private SecondViewEventListener secondViewEventListener = SecondViewEventListener.EMPTY;
    private String viewSaveStateKey;

    public SecondView(@NonNull Context context) {
        this(context, null);
    }

    public SecondView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SecondView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public SecondView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr, @StyleRes int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.view_multi_view_second, this);
        ButterKnife.bind(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        saveState();
        secondViewEventListener.onSecondViewDetached();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        restoreState();
        secondViewEventListener.onSecondViewAttached(this);
    }

    @Override
    public void setStateKey(String key) {
        this.viewSaveStateKey = key;
    }

    @Override
    public void saveState() {
        HashMap<String, Object> savedState = new HashMap<>();
        savedState.put(SECOND_INPUT_STATE, secondInput.getText().toString());
        EnumViewStateRepository.INSTANCE.saveViewState(viewSaveStateKey, savedState);
    }

    @Override
    public void restoreState() {
        HashMap<String, Object> state = EnumViewStateRepository.INSTANCE.getViewState(viewSaveStateKey);
        if (state != null) {
            secondInput.setText((String) state.get(SECOND_INPUT_STATE));
        }
    }

    public void setSecondViewEventListener(SecondViewEventListener secondViewEventListener) {
        this.secondViewEventListener = secondViewEventListener;
    }

    public void onViewDisplayed() {
        LogUtils.d("SECOND on view displayed");
    }
}
