package com.kamil.exampleapp.screen.main.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.kamil.exampleapp.R;
import com.kamil.exampleapp.screen.main.model.HierarchyNode;
import com.kamil.exampleapp.screen.main.model.HierarchyTree;
import com.kamil.exampleapp.widget.hierarchyview.HierarchyView;
import com.kamil.exampleapp.widget.hierarchyview.OnHierarchyViewClickListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HierarchyViewHolder extends RecyclerView.ViewHolder implements OnHierarchyViewClickListener {

    @BindView(R.id.item_hierarchy_root_container) HierarchyView hierarchyView;

    private HierarchyTree hierarchyTree;
    private OnHierarchyViewListItemClickListener onHierarchyViewListItemClickListener;

    public HierarchyViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bindHierarchyItem(HierarchyTree hierarchyTree, OnHierarchyViewListItemClickListener listener) {
        this.hierarchyTree = hierarchyTree;
        this.onHierarchyViewListItemClickListener = listener;

        hierarchyView.setOnHierarchyViewClickListener(this);
        hierarchyView.recreateView(hierarchyTree);
    }

    @Override
    public void onRootExpandClicked() {
        onHierarchyViewListItemClickListener.onRootExpandClicked(hierarchyTree, getAdapterPosition());
    }

    @Override
    public void onRootCollapseClicked() {
        onHierarchyViewListItemClickListener.onRootCollapseClicked(hierarchyTree, getAdapterPosition());
    }

    @Override
    public void onSwitchToNodeClicked(HierarchyNode hierarchyNode) {
        onHierarchyViewListItemClickListener.onSwitchToNodeClicked(hierarchyTree, getAdapterPosition(), hierarchyNode);
    }

    @Override
    public void onBrowseNodeClicked(HierarchyNode hierarchyNode) {
        onHierarchyViewListItemClickListener.onBrowseNodeClicked(hierarchyTree, hierarchyNode);
    }
}
