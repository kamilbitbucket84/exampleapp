package com.kamil.exampleapp.screen.browseproducts.view;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SimpleItemAnimator;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.kamil.exampleapp.R;
import com.kamil.exampleapp.common.di.Dagger;
import com.kamil.exampleapp.common.view.BaseActivity;
import com.kamil.exampleapp.screen.browseproducts.di.BrowseProductsModule;
import com.kamil.exampleapp.screen.browseproducts.di.DaggerBrowseProductsComponent;
import com.kamil.exampleapp.screen.browseproducts.model.InFavoritesState;
import com.kamil.exampleapp.screen.browseproducts.model.ProductItem;
import com.kamil.exampleapp.screen.browseproducts.model.RemovalState;
import com.kamil.exampleapp.screen.browseproducts.presenter.BrowseProductsPresenter;
import com.kamil.exampleapp.screen.main.model.HierarchyPack;
import com.kamil.exampleapp.widget.createproductview.CreateProductView;
import com.kamil.exampleapp.widget.createproductview.OnCreateProductViewListener;

import org.parceler.Parcels;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class BrowseProductsActivity extends BaseActivity implements BrowseProductsMvpView, OnProductItemClickListener {

    private static final String KEY_NODE_PACK = "KEY_NODE_PACK";

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.browse_products_progress_bar) ProgressBar browseProductsProgressbar;
    @BindView(R.id.browse_products_list) RecyclerView browseProductsList;
    @BindString(R.string.view_create_product_invalid) String invalidInput;
    @BindString(R.string.view_create_product_valid) String validInput;
    @BindString(R.string.view_create_product_title) String inputTitle;
    @BindString(R.string.browse_products_error_on_product_item_update) String errorProductUpdate;
    @BindString(R.string.browse_products_error_on_product_item_remove) String errorProductRemove;
    @Inject BrowseProductsPresenter browseProductsPresenter;

    private HierarchyPack nodePack;
    private Dialog dialog;
    private BrowseProductsListAdapter browseProductsListAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_products);
        ButterKnife.bind(this);
        configureToolbar();
        configureListAdapter();
        injectIntoComponent();
        browseProductsPresenter.attachView(this);
        loadDataFromIntent();

        browseProductsPresenter.onScreenCreated(nodePack.currentEntity.id);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        browseProductsPresenter.detachView();
        discardDialogIfNeeded();
    }

    private void discardDialogIfNeeded() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    private void configureToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void configureListAdapter() {
        browseProductsListAdapter = new BrowseProductsListAdapter(this, this);
        browseProductsList.setAdapter(browseProductsListAdapter);
        ((SimpleItemAnimator) browseProductsList.getItemAnimator()).setSupportsChangeAnimations(false);
    }

    private void loadDataFromIntent() {
        nodePack = Parcels.unwrap(getIntent().getParcelableExtra(KEY_NODE_PACK));
        getSupportActionBar().setTitle(nodePack.currentEntity.name);
    }

    private void injectIntoComponent() {
        DaggerBrowseProductsComponent.builder()
                .appComponent(Dagger.appComponent())
                .browseProductsModule(new BrowseProductsModule())
                .build()
                .inject(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_browse_products_release, menu);
        configureSearchView(menu);
        return true;
    }

    private void configureSearchView(Menu menu) {
        MenuItem searchItem = menu.findItem(R.id.menu_item_browse_products_release_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                browseProductsPresenter.onQueryTextSubmit(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                browseProductsPresenter.onQueryTextChange(newText);
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.menu_item_browse_products_release_search:
                break;
            case R.id.menu_item_browse_products_release_create_product:
                browseProductsPresenter.onCreateProductMenuItemClicked();
                break;
        }

        return true;
    }


    @Override
    public void onAddToFavoriteClicked(ProductItem productItem) {
        browseProductsPresenter.onAddToFavoritesClicked(productItem);
    }

    @Override
    public void onRemoveFromFavoriteClicked(ProductItem productItem) {
        browseProductsPresenter.onRemoveFromFavoritesClicked(productItem);
    }

    @Override
    public void onRemoveProductItemClicked(ProductItem productItem) {
        browseProductsPresenter.onRemoveProductItemClicked(productItem);
    }

    ////////////////////////////////////////////////////////////////////////
    //  MVP METHODS
    ////////////////////////////////////////////////////////////////////////

    @Override
    public void onShowCreateProductDialog() {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_create_product);
        dialog.setTitle(inputTitle);
        CreateProductView createProductView = (CreateProductView) dialog.findViewById(R.id.dialog_create_product_view);
        createProductView.setOnCreateProductViewListener(new OnCreateProductViewListener() {

            @Override
            public void onCreateProductCancelClicked() {
                browseProductsPresenter.onCancelButtonClicked();
            }

            @Override
            public void onCreateProductCreateClicked() {
                browseProductsPresenter.onCreateButtonClicked(createProductItemFromInput(createProductView));
            }
        });

        dialog.show();
    }

    @Override
    public void onHideCreateProductDialog() {
        dialog.dismiss();
    }

    private ProductItem createProductItemFromInput(CreateProductView createProductView) {
        return new ProductItem.Builder()
                .id(System.currentTimeMillis())
                .name(createProductView.getNameInput())
                .brand(createProductView.getBrandInput())
                .price(createProductView.getPriceInput())
                .assignedHierarchies(nodePack.getAllHierarchyIds())
                .inFavoriteState(InFavoritesState.NOT_ADDED)
                .removalState(RemovalState.NOT_REMOVED)
                .build();
    }

    @Override
    public void onProductCreationInvalidInput() {
        Toast.makeText(this, invalidInput, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onInsertProductInGivenHierarchiesSuccess(ProductItem productItem) {
        browseProductsListAdapter.addProductItem(productItem);
    }

    @Override
    public void onGetProductItemsFromDbStarted() {
        browseProductsProgressbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onGetProductItemsFromDbSuccess(List<ProductItem> productItems) {
        browseProductsProgressbar.setVisibility(View.GONE);
        browseProductsListAdapter.setProductItems(productItems);
    }

    @Override
    public void onGetProductItemsFromDbError() {
        browseProductsProgressbar.setVisibility(View.GONE);
    }

    @Override
    public void onProductItemUpdate(ProductItem productItem) {
        browseProductsListAdapter.updateProductItem(productItem);
    }

    @Override
    public void onProductItemRemove(ProductItem productItem) {
        browseProductsListAdapter.removeProductItem(productItem);
    }

    @Override
    public void onProductItemUpdateError(ProductItem productItem, String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        browseProductsListAdapter.updateProductItem(productItem);
    }

    @Override
    public void onProductItemRemoveError(ProductItem productItem, String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        browseProductsListAdapter.updateProductItem(productItem);
    }

    ////////////////////////////////////////////////////////////////////////
    //  STATIC METHODS
    ////////////////////////////////////////////////////////////////////////

    public static Intent getIntent(Context context, HierarchyPack hierarchyPack) {
        return new Intent(context, BrowseProductsActivity.class)
                .putExtra(KEY_NODE_PACK, Parcels.wrap(hierarchyPack));
    }

}
