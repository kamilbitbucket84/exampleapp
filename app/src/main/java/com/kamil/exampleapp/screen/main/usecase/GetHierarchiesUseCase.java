package com.kamil.exampleapp.screen.main.usecase;

import com.kamil.exampleapp.common.usecase.UseCase;
import com.kamil.exampleapp.datasource.api.FirebaseApiClient;
import com.kamil.exampleapp.datasource.api.mapper.HierarchiesResponseMapper;
import com.kamil.exampleapp.datasource.api.model.HierarchyResponseEntity;
import com.kamil.exampleapp.screen.main.model.HierarchyTree;

import java.util.List;

import rx.Observable;
import rx.Scheduler;
import rx.schedulers.Schedulers;

public class GetHierarchiesUseCase implements UseCase<List<HierarchyTree>> {

    private final FirebaseApiClient firebaseApiClient;
    private final Scheduler observerScheduler;

    public GetHierarchiesUseCase(FirebaseApiClient apiClient, Scheduler scheduler) {
        this.firebaseApiClient = apiClient;
        this.observerScheduler = scheduler;
    }

    @Override
    public Observable<List<HierarchyTree>> execute() {
        return firebaseApiClient.getHierarchies()
                .map(this::convert)
                .observeOn(observerScheduler)
                .subscribeOn(Schedulers.io());
    }

    private List<HierarchyTree> convert(HierarchyResponseEntity responseEntity) {
        return new HierarchiesResponseMapper().from(responseEntity);
    }
}
