package com.kamil.exampleapp.screen.multiviews.first.di;

import com.kamil.exampleapp.screen.multiviews.first.FirstViewModel;
import com.kamil.exampleapp.screen.multiviews.first.FirstViewPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Kamil on 02/10/2017.
 */

@Module
public class FirstViewModule {

    @Provides
    FirstViewModel provideModel() {
        return new FirstViewModel();
    }

    @Provides
    FirstViewPresenter providePresenter(FirstViewModel firstViewModel) {
        return new FirstViewPresenter(firstViewModel);
    }

}
