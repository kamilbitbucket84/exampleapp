package com.kamil.exampleapp.screen.main.view;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.kamil.exampleapp.R;
import com.kamil.exampleapp.common.di.Dagger;
import com.kamil.exampleapp.common.util.ConvertUtils;
import com.kamil.exampleapp.common.view.BaseActivity;
import com.kamil.exampleapp.screen.browseproducts.view.BrowseProductsActivity;
import com.kamil.exampleapp.screen.experiment.view.ExperimentActivity;
import com.kamil.exampleapp.screen.main.di.DaggerMainComponent;
import com.kamil.exampleapp.screen.main.di.MainModule;
import com.kamil.exampleapp.screen.main.model.HierarchyNode;
import com.kamil.exampleapp.screen.main.model.HierarchyTree;
import com.kamil.exampleapp.screen.main.presenter.MainPresenter;
import com.kamil.exampleapp.screen.multifragment.view.MultiFragmentActivity;
import com.kamil.exampleapp.screen.multitab.MultiTabActivity;
import com.kamil.exampleapp.screen.multiviews.MultiViewsActivity;
import com.kamil.exampleapp.screen.multiviewspager.MultiViewsPagerActivity;
import com.kamil.exampleapp.screen.simpletutorial.SimpleTutorialActivity;
import com.kamil.exampleapp.screen.styling.StylingActivity;
import com.kamil.exampleapp.widget.drawerview.DrawerView;
import com.kamil.exampleapp.widget.drawerview.OnDrawerViewItemClickListener;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindDimen;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

import static com.kamil.exampleapp.widget.drawerview.DrawerView.Option;

public class MainActivity extends BaseActivity implements MainMvpView, OnDrawerViewItemClickListener,
        OnHierarchyViewListItemClickListener {

    @BindView(R.id.main_progress_bar) ProgressBar mainProgressBar;
    @BindView(R.id.main_list) RecyclerView mainList;
    @BindView(R.id.toolbar) Toolbar mainToolbar;
    @BindView(R.id.main_drawer_layout) DrawerLayout mainDrawerLayout;
    @BindView(R.id.main_drawer_view) DrawerView mainDrawerView;
    @BindString(R.string.main_data_available) String noItemsAvailableText;
    @BindDimen(R.dimen.item_hierarchy_bottom_margin) int itemBottomMargin;
    @BindDimen(R.dimen.item_hierarchy_top_margin) int itemTopMargin;
    @BindDimen(R.dimen.item_hierarchy_horizontal_margin) int itemHorizontalMargin;
    @BindDimen(R.dimen.item_hierarchy_last_item_bottom_margin) int lastItemBottomMargin;

    @Inject MainPresenter mainPresenter;
    private HierarchyListAdapter hierarchyListAdapter;
    private HierarchyListItemDecoration hierarchyListItemDecoration;
    private ActionBarDrawerToggle drawerToggle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        configureToolbar();
        configureDrawer();
        configureHierarchyAdapter();
        injectIntoComponent();
        mainPresenter.attachView(this);
        mainPresenter.onScreenCreated();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mainPresenter.detachView();
        mainDrawerLayout.removeDrawerListener(drawerToggle);
        mainList.removeItemDecoration(hierarchyListItemDecoration);
    }

    private void configureToolbar() {
        setSupportActionBar(mainToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.main_title);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_release, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_main_release_refresh:
                mainPresenter.onMenuItemRefreshClicked();
                break;
            case R.id.menu_item_main_release_multiview:
                startActivity(MultiViewsActivity.getIntent(this));
                break;
            case R.id.menu_item_main_release_multiview_viewpager:
                startActivity(MultiViewsPagerActivity.getIntent(this));
                break;
        }

        return true;
    }

    private void configureDrawer() {
        drawerToggle = new ActionBarDrawerToggle(this,
                mainDrawerLayout,
                mainToolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);

        mainDrawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.setDrawerIndicatorEnabled(true);
        mainDrawerView.setOnDrawerViewItemClickListener(this);
    }

    private void configureHierarchyAdapter() {
        hierarchyListItemDecoration = new HierarchyListItemDecoration(
                itemHorizontalMargin,
                itemBottomMargin,
                itemTopMargin,
                lastItemBottomMargin);
        hierarchyListAdapter = new HierarchyListAdapter(this, this);
        mainList.setAdapter(hierarchyListAdapter);
        mainList.addItemDecoration(hierarchyListItemDecoration);
    }

    private void injectIntoComponent() {
        DaggerMainComponent.builder()
                .appComponent(Dagger.appComponent())
                .mainModule(new MainModule())
                .build()
                .inject(this);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        if (mainDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mainDrawerLayout.closeDrawers();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onDrawerViewItemClicked(Option option) {
        switch (option) {
            case TUTORIAL:
                startActivity(SimpleTutorialActivity.getIntent(this));
                break;
            case STYLING:
                startActivity(StylingActivity.getIntent(this));
                break;
            case EXPERIMENT:
                startActivity(ExperimentActivity.getIntent(this));
                break;
            case MULTIFRAGMENTS:
                startActivity(MultiFragmentActivity.getIntent(this));
                break;
            case MULTITAB:
                startActivity(MultiTabActivity.getIntent(this));
        }
    }

    //////////////////////////////////////////////////////////////////////
    // OnHierarchyViewListItemClickListener callbacks
    //////////////////////////////////////////////////////////////////////

    @Override
    public void onRootExpandClicked(HierarchyTree hierarchyTree, int itemPosition) {
        hierarchyTree.expandRoot();
        hierarchyListAdapter.updateHierarchyTree(hierarchyTree, itemPosition);
    }

    @Override
    public void onRootCollapseClicked(HierarchyTree hierarchyTree, int itemPosition) {
        hierarchyTree.collapseRoot();
        hierarchyListAdapter.updateHierarchyTree(hierarchyTree, itemPosition);
    }

    @Override
    public void onSwitchToNodeClicked(HierarchyTree hierarchyTree, int itemPosition, HierarchyNode hierarchyNode) {
        hierarchyTree.switchToNode(hierarchyNode);
        hierarchyListAdapter.updateHierarchyTree(hierarchyTree, itemPosition);
    }

    @Override
    public void onBrowseNodeClicked(HierarchyTree hierarchyTree, HierarchyNode hierarchyNode) {
        startActivity(BrowseProductsActivity.getIntent(this, ConvertUtils.createHierarchyPack(hierarchyNode)));
    }

    //////////////////////////////////////////////////////////////////////
    // MVPVIEW METHODS
    //////////////////////////////////////////////////////////////////////

    @Override
    public void onGetHierarchyTreesStarted() {
        mainProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onGetHierarchyTreesSuccessEmpty() {
        mainProgressBar.setVisibility(View.INVISIBLE);
        Toast.makeText(this, noItemsAvailableText, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGetHierarchyTreesSuccessNotEmpty(List<HierarchyTree> hierarchyTrees) {
        mainProgressBar.setVisibility(View.INVISIBLE);
        hierarchyListAdapter.setHierarchyTrees(hierarchyTrees);
    }

    @Override
    public void onGetHierarchyTreesError() {
        mainProgressBar.setVisibility(View.INVISIBLE);
        Toast.makeText(this, noItemsAvailableText, Toast.LENGTH_SHORT).show();
    }
}
