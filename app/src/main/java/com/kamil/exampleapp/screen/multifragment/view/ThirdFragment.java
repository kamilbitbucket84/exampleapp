package com.kamil.exampleapp.screen.multifragment.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kamil.exampleapp.R;
import com.kamil.exampleapp.common.util.LogUtils;

/**
 * Created by Kamil on 05/05/2017.
 */

public class ThirdFragment extends Fragment {

    public static final String TAG = ThirdFragment.class.getSimpleName();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_third, container, false);
        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) {
            LogUtils.d("VISIBLE - THIRD");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint()) {
            LogUtils.d("RESUME - THIRD");
        }
    }

    public static ThirdFragment newInstance() {
        return new ThirdFragment();
    }

}
