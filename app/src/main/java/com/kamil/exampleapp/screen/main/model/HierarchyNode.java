package com.kamil.exampleapp.screen.main.model;

import java.util.List;

public abstract class HierarchyNode {

    public HierarchyNode(Long id,
                         String name,
                         Long parentId) {
        //  Empty constructor with arguments to enforce using given args
        //  in child classes
    }

    public abstract void connectParentNode(HierarchyNode parentNode);

    public abstract Long getId();

    public abstract String getName();

    public abstract Long getParentId();

    public abstract HierarchyNode getParentNode();

    public abstract List<HierarchyNode> getPreviousNodes();

    public abstract List<HierarchyNode> getChildren();

    public abstract void addChild(HierarchyNode child);

    public abstract Boolean hasChildren();

    public abstract Boolean isRoot();
}
