package com.kamil.exampleapp.screen.main.model;

import java.util.HashMap;
import java.util.List;

public class HierarchyTreeInputValidator {

    public boolean isInputValid(List<HierarchyNode> hierarchyNodes) {
        return checkIfInputNodesNotNullAndNotEmpty(hierarchyNodes) &&
                checkIfInputNodesHasOnlyOneRoot(hierarchyNodes) &&
                checkIfAllInputNodesAreConnected(hierarchyNodes) &&
                checkIfAllInputNodesHasDifferentId(hierarchyNodes);
    }

    private boolean checkIfInputNodesNotNullAndNotEmpty(List<HierarchyNode> hierarchyNodes) {
        return hierarchyNodes != null && !hierarchyNodes.isEmpty();
    }

    private boolean checkIfInputNodesHasOnlyOneRoot(List<HierarchyNode> hierarchyNodes) {
        int rootCounter = 0;
        for (HierarchyNode node : hierarchyNodes) {
            if (node.getParentId() == null) {
                rootCounter++;
            }
        }

        return rootCounter == 1;
    }

    private boolean checkIfAllInputNodesAreConnected(List<HierarchyNode> hierarchyNodes) {
        HashMap<Long, HierarchyNode> nodeCheckMap = new HashMap<>();

        //  PUT ALL NODES TO MAP
        for (HierarchyNode node : hierarchyNodes) {
            nodeCheckMap.put(node.getId(), node);
        }

        //  NOT CONNECTED NODES HAS PARENT IDS DIFFERENT THAN ALL STORED IDS
        boolean allNodesConnected = true;
        for (HierarchyNode node : hierarchyNodes) {
            if (node.getParentId() != null && !nodeCheckMap.containsKey(node.getParentId())) {
                allNodesConnected = false;
                break;
            }
        }

        return allNodesConnected;
    }

    private boolean checkIfAllInputNodesHasDifferentId(List<HierarchyNode> hierarchyNodes) {
        HashMap<Long, HierarchyNode> repeatCheckMap = new HashMap<>();

        for (HierarchyNode node : hierarchyNodes) {
            if (repeatCheckMap.containsKey(node.getId())) {
                return false;
            }

            repeatCheckMap.put(node.getId(), node);
        }

        return true;
    }
}
