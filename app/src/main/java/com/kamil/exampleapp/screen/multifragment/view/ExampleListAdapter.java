package com.kamil.exampleapp.screen.multifragment.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kamil.exampleapp.R;
import com.kamil.exampleapp.common.list.ListItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kamil on 10/05/2017.
 */

public class ExampleListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutInflater inflater;
    private List<ListItem> exampleItems = new ArrayList<>();

    public ExampleListAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_example, parent, false);
        return new ExampleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ExampleViewHolder) holder).bindItem((ExampleItem) exampleItems.get(position));
    }

    @Override
    public int getItemCount() {
        return exampleItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return exampleItems.get(position).getItemViewType();
    }

    public void setExampleItems(List<ListItem> exampleItems) {
        this.exampleItems.clear();
        if (exampleItems != null && !exampleItems.isEmpty()) {
            this.exampleItems.addAll(exampleItems);
        }
        notifyDataSetChanged();
    }
}
