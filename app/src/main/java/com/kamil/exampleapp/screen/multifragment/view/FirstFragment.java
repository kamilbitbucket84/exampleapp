package com.kamil.exampleapp.screen.multifragment.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kamil.exampleapp.R;
import com.kamil.exampleapp.common.ui.TextWatcherAdapter;
import com.kamil.exampleapp.common.util.LogUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Kamil on 05/05/2017.
 */

public class FirstFragment extends Fragment {

    public static final String TAG = FirstFragment.class.getSimpleName();

    @BindView(R.id.first_input) TextInputEditText firstInput;
    private Unbinder unbinder;

    public String firstText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_first, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        firstInput.addTextChangedListener(textWatcherAdapter);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) {
            LogUtils.d("VISIBLE - FIRST");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint()) {
            LogUtils.d("RESUME - FIRST");
        }
    }

    @Override
    public void onDestroyView() {
        firstInput.removeTextChangedListener(textWatcherAdapter);
        super.onDestroyView();
        unbinder.unbind();
    }

    private TextWatcherAdapter textWatcherAdapter = new TextWatcherAdapter() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            super.onTextChanged(s, start, before, count);
            firstText = s.toString();
        }
    };

    public static FirstFragment newInstance() {
        return new FirstFragment();
    }
}
