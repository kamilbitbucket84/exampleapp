package com.kamil.exampleapp.screen.simpletutorial;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.bigfat.library.DotsView;
import com.kamil.exampleapp.R;
import com.kamil.exampleapp.common.view.BaseActivity;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SimpleTutorialActivity extends BaseActivity {

    @BindView(R.id.simple_tutorial_view_pager) ViewPager viewPager;
    @BindView(R.id.simple_tutorial_left_text) TextView leftText;
    @BindView(R.id.simple_tutorial_right_text) TextView rightText;
    @BindView(R.id.simple_tutorial_dots_view) DotsView dotsView;
    @BindString(R.string.simple_view_pager_begin) String beginText;
    @BindString(R.string.simple_view_pager_previous) String previousText;
    @BindString(R.string.simple_view_pager_next) String nextText;

    private SimpleTutorialViewPagerAdapter pagerAdapter;
    private OnPageChangeListenerAdapter onPageChangeListenerAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_tutorial);
        ButterKnife.bind(this);
        configureViewPagerAdapter();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewPager.removeOnPageChangeListener(onPageChangeListenerAdapter);
    }

    private void configureViewPagerAdapter() {
        dotsView.setCount(SimpleTutorialData.values().length);
        pagerAdapter = new SimpleTutorialViewPagerAdapter();
        viewPager.setAdapter(pagerAdapter);
        onPageChangeListenerAdapter = new OnPageChangeListenerAdapter() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                changeUIOnPageSelected(position);
            }
        };
        viewPager.addOnPageChangeListener(onPageChangeListenerAdapter);
        onPageChangeListenerAdapter.onPageSelected(0);
    }

    private void changeUIOnPageSelected(int position) {
        if (position == 0) {
            onFirstPageSelected();
        } else if (position == (SimpleTutorialData.values().length - 1)) {
            onLastPageSelected(position);
        } else {
            onMiddlePageSelected(position);
        }
    }

    private void onFirstPageSelected() {
        dotsView.setCurrent(0);
        leftText.setText("");
        rightText.setText(nextText);
    }

    private void onMiddlePageSelected(int position) {
        dotsView.setCurrent(position);
        leftText.setText(previousText);
        rightText.setText(nextText);
    }

    private void onLastPageSelected(int position) {
        dotsView.setCurrent(position);
        leftText.setText(previousText);
        rightText.setText(beginText);
    }

    @OnClick(R.id.simple_tutorial_left_text)
    public void onLeftTextClicked() {
        if(viewPager.getCurrentItem() != 0) {
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
        }
    }

    @OnClick(R.id.simple_tutorial_right_text)
    public void onRightTextClicked() {
        int position = viewPager.getCurrentItem();
        if (position == (SimpleTutorialData.values().length - 1)) {
            Toast.makeText(this, "END" , Toast.LENGTH_SHORT).show();
        } else {
            viewPager.setCurrentItem(position + 1);
        }
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, SimpleTutorialActivity.class);
    }
}
