package com.kamil.exampleapp.screen.multiviews.third;


/**
 * Created by Kamil on 28/09/2017.
 */

public interface ThirdViewEventListener {

    void onThirdViewAttached(ThirdView thirdView);

    void onThirdViewDetached();

    ThirdViewEventListener EMPTY = new ThirdViewEventListener() {
        @Override
        public void onThirdViewAttached(ThirdView thirdView) {

        }

        @Override
        public void onThirdViewDetached() {

        }
    };
}
