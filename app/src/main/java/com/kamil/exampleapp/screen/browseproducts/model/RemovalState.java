package com.kamil.exampleapp.screen.browseproducts.model;

import com.kamil.exampleapp.R;

public enum RemovalState {
    NOT_REMOVED(R.drawable.trash_not_removed),
    REMOVING(R.drawable.trash_removing);

    public final int drawableResId;

    RemovalState(int drawableResId) {
        this.drawableResId = drawableResId;
    }
}