package com.kamil.exampleapp.screen.main.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExampleHierarchyTree extends HierarchyTree {

    private HierarchyNode rootNode;
    private HierarchyNode currentNode;
    private boolean rootExpanded;
    private boolean valid;
    private HashMap<Long, HierarchyNode> allNodesMap = new HashMap<>();
    private List<HierarchyNode> allNodesList = new ArrayList<>();
    private HierarchyTreeInputValidator validator = new HierarchyTreeInputValidator();
    private HierarchyTreeNodeConnector connector = new HierarchyTreeNodeConnector();

    public ExampleHierarchyTree(List<HierarchyNode> hierarchyNodes) {
        super(hierarchyNodes);

        if (validator.isInputValid(hierarchyNodes)) {
            storeAllNodes(hierarchyNodes);
            rootNode = connector.findRootNode(hierarchyNodes);
            connector.connectAllNodes(rootNode, hierarchyNodes);
            currentNode = rootNode;

            valid = true;
        } else {
            valid = false;
        }
    }

    @Override
    public HierarchyNode findNodeWithId(Long id) {
        return valid ? allNodesMap.get(id) : null;
    }

    @Override
    public List<HierarchyNode> getAllNodes() {
        return allNodesList;
    }

    private void storeAllNodes(List<HierarchyNode> hierarchyNodes) {
        for (HierarchyNode node : hierarchyNodes) {
            allNodesMap.put(node.getId(), node);
            allNodesList.add(node);
        }
    }

    @Override
    public HierarchyNode getCurrentNode() {
        return currentNode;
    }

    @Override
    public HierarchyNode getRootNode() {
        return rootNode;
    }

    @Override
    public boolean switchToNode(HierarchyNode hierarchyNode) {
        boolean switchResult = false;
        if (isGivenNodeInTree(hierarchyNode)) {
            currentNode = hierarchyNode;
            switchResult = true;
        }

        return switchResult;
    }

    @Override
    public void expandRoot() {
        rootExpanded = true;
    }

    @Override
    public void collapseRoot() {
        rootExpanded = false;
    }

    @Override
    public boolean isRootExpanded() {
        return rootExpanded;
    }

    @Override
    public boolean isValid() {
        return valid;
    }

    @Override
    public boolean isGivenNodeInTree(HierarchyNode hierarchyNode) {
        return valid && allNodesMap.containsValue(hierarchyNode);
    }
}
