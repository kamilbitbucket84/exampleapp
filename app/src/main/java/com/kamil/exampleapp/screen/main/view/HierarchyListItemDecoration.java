package com.kamil.exampleapp.screen.main.view;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class HierarchyListItemDecoration extends RecyclerView.ItemDecoration {

    private final int horizontalMargin;
    private final int bottomMargin;
    private final int topMargin;
    private final int lastItemBottomMargin;

    public HierarchyListItemDecoration(int horizontalMargin, int bottomMargin, int topMargin, int lastItemBottomMargin) {
        this.horizontalMargin = horizontalMargin;
        this.bottomMargin = bottomMargin;
        this.topMargin = topMargin;
        this.lastItemBottomMargin = lastItemBottomMargin;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        int itemPosition = parent.getChildLayoutPosition(view);
        int numberOfAllItems = parent.getAdapter().getItemCount();

        if (itemPosition == 0) {
            outRect.top = topMargin;
            outRect.bottom = bottomMargin;
            outRect.left = horizontalMargin;
            outRect.right = horizontalMargin;
        } else if (itemPosition == (numberOfAllItems - 1)) {
            outRect.bottom = lastItemBottomMargin;
            outRect.left = horizontalMargin;
            outRect.right = horizontalMargin;
        } else {
            outRect.bottom = bottomMargin;
            outRect.left = horizontalMargin;
            outRect.right = horizontalMargin;
        }
    }
}
