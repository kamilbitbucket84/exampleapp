package com.kamil.exampleapp.screen.experiment.view;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.kamil.exampleapp.R;
import com.kamil.exampleapp.common.di.Dagger;
import com.kamil.exampleapp.common.util.LogUtils;
import com.kamil.exampleapp.common.util.PermissionUtils;
import com.kamil.exampleapp.common.view.BaseActivity;
import com.kamil.exampleapp.screen.experiment.MediaPlayerWrapper;
import com.kamil.exampleapp.screen.experiment.di.DaggerExperimentComponent;
import com.kamil.exampleapp.screen.experiment.di.ExperimentModule;
import com.kamil.exampleapp.screen.experiment.presenter.ExperimentPresenter;
import com.kamil.exampleapp.service.event.eventtype.LaunchEvent;
import com.kamil.exampleapp.service.event.launcher.ProximityEventLaunchParams;
import com.kamil.exampleapp.service.event.launcher.TimeEventLaunchParams;
import com.kamil.exampleapp.service.event.mediator.EventReceiver;
import com.kamil.exampleapp.service.event.mediator.EventReceiverMediator;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ExperimentActivity extends BaseActivity implements ExperimentMvpView, EventReceiver {

    public static final int REQUEST_CODE_ACCESS_EXTERNAL_STORAGE = 1234;
    public static final int REQUEST_CODE_ACCESS_FINE_LOCATION = 5678;
    private static final String KEY_EXPERIMENT_ID = "EXPERIMENT_ID";

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.experiment_logs) TextView experimentLogs;
    @BindString(R.string.experiment_permission_dialog_ok) String okText;
    @BindString(R.string.experiment_permission_dialog_cancel) String cancelText;
    @BindString(R.string.experiment_external_storage_permission_dialog_title) String externalStorageDialogTitle;
    @BindString(R.string.experiment_external_storage_permission_dialog_message) String externalStorageDialogMessage;
    @BindString(R.string.experiment_location_permission_dialog_title) String userLocationDialogTitle;
    @BindString(R.string.experiment_location_permission_dialog_message) String userLocationDialogMessage;

    @Inject ExperimentPresenter experimentPresenter;
    @Inject EventReceiverMediator eventReceiverMediator;

    private AlertDialog gotoAppSettingsDialog;
    private MediaPlayerWrapper mediaPlayerWrapper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_experiment);
        ButterKnife.bind(this);
        injectIntoComponent();
        configureToolbar();
        experimentPresenter.attachView(this);
        experimentPresenter.onScreenCreated();
        configureMediaPlayerManager();
        loadDataFromIntent();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        experimentPresenter.onScreenDestroyed();
        experimentPresenter.detachView();
        removeGotoSettingsDialogIfNeeded();
        mediaPlayerWrapper.release();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        LogUtils.d("NEW intent passed id: " + intent.getLongExtra(KEY_EXPERIMENT_ID, 0));
    }

    private void loadDataFromIntent() {
        LogUtils.d("LOAD data passed id: " + getIntent().getLongExtra(KEY_EXPERIMENT_ID, 0));
    }

    private void removeGotoSettingsDialogIfNeeded() {
        if (gotoAppSettingsDialog != null && gotoAppSettingsDialog.isShowing()) {
            gotoAppSettingsDialog.dismiss();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_experiment_debug, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.menu_item_experiment_debug_load_file:
                experimentPresenter.onMenuItemLoadFileClicked();
                break;
            case R.id.menu_item_experiment_debug_get_user_location:
                experimentPresenter.onMenuItemGetUserLocationClicked();
                break;
            case R.id.menu_item_experiment_debug_start_proximity_events:
                experimentPresenter.onMenuItemStartProximityEventsLauncherClicked();
                break;
            case R.id.menu_item_experiment_debug_stop_proximity_events:
                experimentPresenter.onMenuItemStopProximityEventsLauncherClicked();
                break;
            case R.id.menu_item_experiment_debug_start_time_events:
                experimentPresenter.onMenuItemStartTimeEventsLauncherClicked();
                break;
            case R.id.menu_item_experiment_debug_stop_time_events:
                experimentPresenter.onMenuItemStopTimeEventsLauncherClicked();
                break;
            case R.id.menu_item_experiment_debug_connect_experiment_service:
                experimentPresenter.onMenuItemConnectToExperimentServiceClicked();
                break;
            case R.id.menu_item_experiment_debug_disconnect_experiment_service:
                experimentPresenter.onMenuItemDisconnectFromExperimentServiceClicked();
                break;
        }

        return true;
    }

    private void injectIntoComponent() {
        DaggerExperimentComponent.builder()
                .experimentModule(new ExperimentModule())
                .appComponent(Dagger.appComponent())
                .build()
                .inject(this);
    }

    private void configureToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void configureMediaPlayerManager() {
        mediaPlayerWrapper = new MediaPlayerWrapper(this);
    }

    @OnClick(R.id.experiement_stop_playing)
    public void stopPlaying() {
        mediaPlayerWrapper.stopPlaying();
    }

    @OnClick(R.id.experiment_play_sound_one)
    public void playOne() {
        mediaPlayerWrapper.playSound(R.raw.sound_chainsaw);
    }

    @OnClick(R.id.experiment_play_sound_two)
    public void playTwo() {
        mediaPlayerWrapper.playSound(R.raw.sound_parrots);
    }

    @OnClick(R.id.experiment_play_sound_three)
    public void playThree() {
        mediaPlayerWrapper.playSound(R.raw.sound_turkey);
    }

    /////////////////////////////////////////////////////////////////////////
    //  MVP methods
    /////////////////////////////////////////////////////////////////////////

    @Override
    public void askForAccessExternalStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            experimentPresenter.onAccessWriteExternalStoragePermissionGranted();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_CODE_ACCESS_EXTERNAL_STORAGE);
        }
    }

    @Override
    public void askForAccessFineLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            experimentPresenter.onAccessFineLocationPermissionGranted();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_CODE_ACCESS_FINE_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ACCESS_EXTERNAL_STORAGE:
                onAccessWriteExternalStoragePermissionResult(grantResults);
                break;
            case REQUEST_CODE_ACCESS_FINE_LOCATION:
                onAccessFineLocationPermissionResult(grantResults);
                break;
        }
    }

    private void onAccessWriteExternalStoragePermissionResult(int[] grantResults) {
        if (PermissionUtils.checkIfPermissionGranted(grantResults)) {
            experimentPresenter.onAccessWriteExternalStoragePermissionGranted();
        } else {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                experimentPresenter.onAccessWriteExternalStoragePermissionNeverAskAgain(externalStorageDialogTitle, externalStorageDialogMessage);
            } else {
                experimentPresenter.onAccessWriteExternalStoragePermissionRefused();
            }
        }
    }

    private void onAccessFineLocationPermissionResult(int[] grantResults) {
        if (PermissionUtils.checkIfPermissionGranted(grantResults)) {
            experimentPresenter.onAccessFineLocationPermissionGranted();
        } else {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                experimentPresenter.onAccessFineLocationPermissionNeverAskAgain(userLocationDialogTitle, userLocationDialogMessage);
            } else {
                experimentPresenter.onAccessFineLocationPermissionRefused();
            }
        }
    }

    @Override
    public void onPermissionNeverAskAgainClicked(String title, String message) {
        gotoAppSettingsDialog = new AlertDialog.Builder(this, R.style.AppThemePermissionDialogStyle)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(okText, (dialog, which) -> {
                    goToApplicationSettings();
                })
                .setNegativeButton(cancelText, (dialog, which) -> {
                    gotoAppSettingsDialog.dismiss();
                }).create();

        gotoAppSettingsDialog.show();
    }

    private void goToApplicationSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }

    @Override
    public void onStartProximityEventsLauncher() {
        eventReceiverMediator.startProximityEventsLauncher(new ProximityEventLaunchParams(4));
    }

    @Override
    public void onStopProximityEventsLauncher() {
        eventReceiverMediator.stopProximityEventsLauncher();
    }

    @Override
    public void onStartTimeEventsLauncher() {
        eventReceiverMediator.startTimeEventsLauncher(new TimeEventLaunchParams(0, 5, TimeUnit.SECONDS));
    }

    @Override
    public void onStopTimeEventsLauncher() {
        eventReceiverMediator.stopTimeEventsLauncher();
    }

    @Override
    public void onConnectLaunchEventClient() {
        eventReceiverMediator.connectEventReceiver(this);
    }

    @Override
    public void onDisconnectLaunchEventClient() {
        eventReceiverMediator.disconnectEventReceiver();
    }

    /////////////////////////////////////////////////////////////////////////
    //  EVENT RECEIVER METHODS
    /////////////////////////////////////////////////////////////////////////

    @Override
    public void onEventReceived(LaunchEvent launchEvent) {
        experimentLogs.setText(launchEvent.toString());
    }

    /////////////////////////////////////////////////////////////////////////
    //  static methods
    /////////////////////////////////////////////////////////////////////////

    public static Intent getIntent(Context context) {
        return new Intent(context, ExperimentActivity.class);
    }

    public static Intent getIntent(Context context, Long id) {
        return new Intent(context, ExperimentActivity.class)
                .putExtra(KEY_EXPERIMENT_ID, id);
    }
}
