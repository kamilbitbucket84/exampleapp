package com.kamil.exampleapp.screen.main.view;

import com.kamil.exampleapp.screen.main.model.HierarchyNode;
import com.kamil.exampleapp.screen.main.model.HierarchyTree;

public interface OnHierarchyViewListItemClickListener {

    void onRootExpandClicked(HierarchyTree hierarchyTree, int itemPosition);

    void onRootCollapseClicked(HierarchyTree hierarchyTree, int itemPosition);

    void onSwitchToNodeClicked(HierarchyTree hierarchyTree, int itemPosition, HierarchyNode hierarchyNode);

    void onBrowseNodeClicked(HierarchyTree hierarchyTree, HierarchyNode hierarchyNode);

    OnHierarchyViewListItemClickListener EMPTY = new OnHierarchyViewListItemClickListener() {
        @Override
        public void onRootExpandClicked(HierarchyTree hierarchyTree, int itemPosition) {
        }

        @Override
        public void onRootCollapseClicked(HierarchyTree hierarchyTree, int itemPosition) {
        }

        @Override
        public void onSwitchToNodeClicked(HierarchyTree hierarchyTree, int itemPosition, HierarchyNode hierarchyNode) {

        }

        @Override
        public void onBrowseNodeClicked(HierarchyTree hierarchyTree, HierarchyNode hierarchyNode) {
        }
    };
}
