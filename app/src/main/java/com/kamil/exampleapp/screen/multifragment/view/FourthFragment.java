package com.kamil.exampleapp.screen.multifragment.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kamil.exampleapp.R;
import com.kamil.exampleapp.common.util.LogUtils;
import com.kamil.exampleapp.common.util.MockedDataUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Kamil on 05/05/2017.
 */
public class FourthFragment extends Fragment {

    public static final String TAG = FourthFragment.class.getSimpleName();

    @BindView(R.id.fourth_list) RecyclerView fourthList;
    private Unbinder unbinder;
    private ExampleListAdapter exampleListAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_fourth, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        configureAdapter();
        loadDataOnViewCreated();
    }

    private void configureAdapter() {
        exampleListAdapter = new ExampleListAdapter(getContext());
        fourthList.setAdapter(exampleListAdapter);
    }

    private void loadDataOnViewCreated() {
        exampleListAdapter.setExampleItems(MockedDataUtils.createMockedExampleItems());
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isResumed()) {
            LogUtils.d("VISIBLE - FOURTH");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint()) {
            LogUtils.d("RESUME - FOURTH");
        }
    }


    public static FourthFragment newInstance() {
        return new FourthFragment();
    }
}
