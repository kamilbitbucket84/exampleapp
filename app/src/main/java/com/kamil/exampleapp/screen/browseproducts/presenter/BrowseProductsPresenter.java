package com.kamil.exampleapp.screen.browseproducts.presenter;

import com.kamil.exampleapp.common.mvp.BaseMvpPresenter;
import com.kamil.exampleapp.common.util.LogUtils;
import com.kamil.exampleapp.datasource.database.DatabaseScheduler;
import com.kamil.exampleapp.screen.browseproducts.model.BrowseProductsModel;
import com.kamil.exampleapp.screen.browseproducts.model.InFavoritesState;
import com.kamil.exampleapp.screen.browseproducts.model.ProductItem;
import com.kamil.exampleapp.screen.browseproducts.model.RemovalState;
import com.kamil.exampleapp.screen.browseproducts.view.BrowseProductsMvpView;

import java.util.concurrent.TimeUnit;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.subjects.PublishSubject;
import rx.subjects.Subject;

public class BrowseProductsPresenter extends BaseMvpPresenter<BrowseProductsMvpView> {

    private final BrowseProductsModel browseProductsModel;
    private Subject<String, String> queryFilter = PublishSubject.create();
    private Long hierarchyId;

    public BrowseProductsPresenter(BrowseProductsModel model) {
        this.browseProductsModel = model;
        createQueryFilter();
    }

    //////////////////////////////////////////////////////////////////////////
    //  UI EVENTS
    //////////////////////////////////////////////////////////////////////////

    public void onScreenCreated(Long currentHierarchyId) {
        hierarchyId = currentHierarchyId;
        selectProductDbEntitiesWithHierarchyId(currentHierarchyId);
    }

    public void onCreateProductMenuItemClicked() {
        getView().onShowCreateProductDialog();
    }

    public void onCancelButtonClicked() {
        getView().onHideCreateProductDialog();
    }

    public void onCreateButtonClicked(ProductItem productItem) {
        if (productItem.isValid()) {
            insertProductDbEntityAndAssignToHierarchies(productItem);
        } else {
            getView().onProductCreationInvalidInput();
        }
    }

    public void onAddToFavoritesClicked(ProductItem productItem) {
        addProductItemToFavorites(productItem);
    }

    public void onRemoveFromFavoritesClicked(ProductItem productItem) {
        removeProductItemFromFavorites(productItem);
    }

    public void onRemoveProductItemClicked(ProductItem productItem) {
        removeProductItem(productItem);
    }

    public void onQueryTextChange(String query) {
        queryFilter.onNext(query);
    }

    public void onQueryTextSubmit(String query) {
        queryFilter.onNext(query);
    }

    private void createQueryFilter() {
        Subscription filterQuery =
                queryFilter.debounce(500, TimeUnit.MILLISECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe((query) -> selectProductDbEntitiesWithHierarchyIdFilteredByQuery(query));

        subscriptions.add(filterQuery);
    }

    //////////////////////////////////////////////////////////////////////////
    //  LOAD data from model
    //////////////////////////////////////////////////////////////////////////

    private void selectProductDbEntitiesWithHierarchyIdFilteredByQuery(String query) {
        getView().onGetProductItemsFromDbStarted();

        Subscription selectFilteredProducts
                = browseProductsModel.selectProductWithHierarchyIdFilteredWithQuery(hierarchyId, query)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(DatabaseScheduler.INSTANCE.getScheduler())
                .subscribe(filteredProductItems -> {
                    getView().onGetProductItemsFromDbSuccess(filteredProductItems);
                }, throwable -> {
                    getView().onGetProductItemsFromDbError();
                });

        subscriptions.add(selectFilteredProducts);
    }

    private void addProductItemToFavorites(ProductItem productItem) {
        ProductItem adding = ProductItem.copyAndModify(productItem, InFavoritesState.ADDING);
        getView().onProductItemUpdate(adding);

        Subscription addToFavorites
                = browseProductsModel.addToFavorites(productItem)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(DatabaseScheduler.INSTANCE.getScheduler())
                .subscribe(addedToFavorites -> {
                    ProductItem added = ProductItem.copyAndModify(productItem, InFavoritesState.ADDED);
                    getView().onProductItemUpdate(added);
                }, throwable -> {
                    ProductItem not_added = ProductItem.copyAndModify(productItem, InFavoritesState.NOT_ADDED);
                    getView().onProductItemUpdateError(not_added, throwable.getMessage());
                });

        subscriptions.add(addToFavorites);
    }

    private void removeProductItemFromFavorites(ProductItem productItem) {
        ProductItem removing = ProductItem.copyAndModify(productItem, InFavoritesState.REMOVING);
        getView().onProductItemUpdate(removing);

        Subscription removeFromFavorites
                = browseProductsModel.removeFromFavorites(productItem)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(DatabaseScheduler.INSTANCE.getScheduler())
                .subscribe(removedFromFavorites -> {
                    ProductItem removed = ProductItem.copyAndModify(productItem, InFavoritesState.NOT_ADDED);
                    getView().onProductItemUpdate(removed);
                }, throwable -> {
                    ProductItem added = ProductItem.copyAndModify(productItem, InFavoritesState.ADDED);
                    getView().onProductItemUpdateError(added, throwable.getMessage());
                });

        subscriptions.add(removeFromFavorites);
    }

    private void removeProductItem(ProductItem productItem) {
        ProductItem removing = ProductItem.copyAndModify(productItem, RemovalState.REMOVING);
        getView().onProductItemUpdate(removing);

        Subscription removeProductItem
                = browseProductsModel.deleteProductItem(productItem)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(DatabaseScheduler.INSTANCE.getScheduler())
                .subscribe(removed -> {
                    getView().onProductItemRemove(productItem);
                }, throwable -> {
                    ProductItem notRemoved = ProductItem.copyAndModify(productItem, RemovalState.NOT_REMOVED);
                    getView().onProductItemRemoveError(notRemoved, throwable.getMessage());
                });

        subscriptions.add(removeProductItem);
    }

    private void selectProductDbEntitiesWithHierarchyId(Long hierarchyId) {
        getView().onGetProductItemsFromDbStarted();

        Subscription selectProductsWithHierarchyId
                = browseProductsModel.selectAllProductsWithHierarchyId(hierarchyId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(DatabaseScheduler.INSTANCE.getScheduler())
                .subscribe(productItems -> {
                    getView().onGetProductItemsFromDbSuccess(productItems);
                }, throwable -> {
                    getView().onGetProductItemsFromDbError();
                });

        subscriptions.add(selectProductsWithHierarchyId);
    }

    private void insertProductDbEntityAndAssignToHierarchies(ProductItem productItem) {
        Subscription insertAndAssign
                = browseProductsModel.insertProductAndAssignToHierarchies(productItem)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(DatabaseScheduler.INSTANCE.getScheduler())
                .subscribe(insertedItem -> {
                    getView().onInsertProductInGivenHierarchiesSuccess(insertedItem);
                }, throwable -> {
                    LogUtils.d("PRODUCT INSERT AND ASSIGN ERROR ------- " + throwable);
                });

        subscriptions.add(insertAndAssign);
    }

}
