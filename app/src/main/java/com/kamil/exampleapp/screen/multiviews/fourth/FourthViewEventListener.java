package com.kamil.exampleapp.screen.multiviews.fourth;

/**
 * Created by Kamil on 29/09/2017.
 */

public interface FourthViewEventListener {

    void onFourthViewAttached(FourthView fourthView);

    void onFourthViewDetached();

    FourthViewEventListener EMPTY = new FourthViewEventListener() {
        @Override
        public void onFourthViewAttached(FourthView fourthView) {

        }

        @Override
        public void onFourthViewDetached() {

        }
    };
}
