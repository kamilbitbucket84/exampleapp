package com.kamil.exampleapp.screen.experiment.di;

import com.kamil.exampleapp.screen.experiment.model.ExperimentModel;
import com.kamil.exampleapp.screen.experiment.presenter.ExperimentPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ExperimentModule {

    @Provides
    ExperimentModel provideModel() {
        return new ExperimentModel();
    }

    @Provides
    ExperimentPresenter providePresenter(ExperimentModel experimentModel) {
        return new ExperimentPresenter(experimentModel);
    }
}
