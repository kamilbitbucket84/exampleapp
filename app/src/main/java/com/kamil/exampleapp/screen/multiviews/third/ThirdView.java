package com.kamil.exampleapp.screen.multiviews.third;

import android.content.Context;
import android.os.Build;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.annotation.StyleRes;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.kamil.exampleapp.R;
import com.kamil.exampleapp.common.ui.savedview.EnumViewStateRepository;
import com.kamil.exampleapp.common.ui.savedview.SavedStateView;
import com.kamil.exampleapp.common.util.LogUtils;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kamil on 25/09/2017.
 */

public class ThirdView extends FrameLayout implements SavedStateView {

    private static final String THIRD_INPUT_STATE = "THIRD_INPUT_STATE";

    @BindView(R.id.view_multi_view_third_input) EditText thirdInput;

    private ThirdViewEventListener thirdViewEventListener = ThirdViewEventListener.EMPTY;
    private String viewStateKey;


    public ThirdView(@NonNull Context context) {
        this(context, null);
    }

    public ThirdView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ThirdView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public ThirdView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr, @StyleRes int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.view_multi_view_third, this);
        ButterKnife.bind(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        saveState();
        thirdViewEventListener.onThirdViewDetached();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        restoreState();
        thirdViewEventListener.onThirdViewAttached(this);
    }

    @Override
    public void setStateKey(String key) {
        this.viewStateKey = key;
    }

    @Override
    public void saveState() {
        HashMap<String, Object> savedState = new HashMap<>();
        savedState.put(THIRD_INPUT_STATE, thirdInput.getText().toString());
        EnumViewStateRepository.INSTANCE.saveViewState(viewStateKey, savedState);
    }

    @Override
    public void restoreState() {
        HashMap<String, Object> savedState = EnumViewStateRepository.INSTANCE.getViewState(viewStateKey);
        if (savedState != null) {
            thirdInput.setText((String) savedState.get(THIRD_INPUT_STATE));
        }
    }

    public void setThirdViewEventListener(ThirdViewEventListener thirdViewEventListener) {
        this.thirdViewEventListener = thirdViewEventListener;
    }

    public void onViewDisplayed() {
        LogUtils.d("THIRD on view displayed");
    }
}
