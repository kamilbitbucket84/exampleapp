package com.kamil.exampleapp.screen.multiviews.fourth;

import android.content.Context;
import android.os.Build;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.annotation.StyleRes;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.kamil.exampleapp.R;
import com.kamil.exampleapp.common.ui.savedview.EnumViewStateRepository;
import com.kamil.exampleapp.common.ui.savedview.SavedStateView;
import com.kamil.exampleapp.common.util.LogUtils;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kamil on 29/09/2017.
 */

public class FourthView extends FrameLayout implements SavedStateView {

    private static final String FOURTH_INPUT_STATE = "FOURTH_INPUT_STATE";

    @BindView(R.id.view_multi_view_fourth_input) EditText fourthInput;

    private FourthViewEventListener fourthViewEventListener = FourthViewEventListener.EMPTY;
    private String viewSaveStateKey;


    public FourthView(@NonNull Context context) {
        this(context, null);
    }

    public FourthView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FourthView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public FourthView(@NonNull Context context,
                      @Nullable AttributeSet attrs,
                      @AttrRes int defStyleAttr,
                      @StyleRes int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.view_multi_view_fourth, this);
        ButterKnife.bind(this);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        restoreState();
        fourthViewEventListener.onFourthViewAttached(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        saveState();
        fourthViewEventListener.onFourthViewDetached();
    }

    @Override
    public void setStateKey(String key) {
        this.viewSaveStateKey = key;
    }

    @Override
    public void saveState() {
        HashMap<String, Object> savedState = new HashMap<>();
        savedState.put(FOURTH_INPUT_STATE, fourthInput.getText().toString());
        EnumViewStateRepository.INSTANCE.saveViewState(viewSaveStateKey, savedState);
    }

    @Override
    public void restoreState() {
        HashMap<String, Object> state = EnumViewStateRepository.INSTANCE.getViewState(viewSaveStateKey);
        if (state != null) {
            fourthInput.setText((String) state.get(FOURTH_INPUT_STATE));
        }
    }

    public void setFourthViewEventListener(FourthViewEventListener fourthViewEventListener) {
        this.fourthViewEventListener = fourthViewEventListener;
    }

    public void onViewDisplayed() {
        LogUtils.d("FOURTH on view displayed");
    }
}
