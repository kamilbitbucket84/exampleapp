package com.kamil.exampleapp.screen.multiviews.first;

import rx.Observable;

/**
 * Created by Kamil on 02/10/2017.
 */

public class FirstViewModel {

    public Observable<String> mockedSendForm() {
        return Observable.fromCallable(() -> {
            Thread.sleep(4000);
            return "mocked result";
        });
    }
}
