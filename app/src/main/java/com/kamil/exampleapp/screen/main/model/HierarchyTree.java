package com.kamil.exampleapp.screen.main.model;

import java.util.List;

public abstract class HierarchyTree {

    protected static final String NO_CHILDREN_AT_INDEX =
            "Operation impossible - current node does not have any children";

    public HierarchyTree(List<HierarchyNode> hierarchyNodes) {
        //  TO enforce such arguments in child classes
    }

    public abstract HierarchyNode findNodeWithId(Long id);

    public abstract List<HierarchyNode> getAllNodes();

    public abstract HierarchyNode getCurrentNode();

    public abstract HierarchyNode getRootNode();

    public abstract boolean switchToNode(HierarchyNode hierarchyNode);

    public abstract void expandRoot();

    public abstract void collapseRoot();

    public abstract boolean isRootExpanded();

    public abstract boolean isValid();

    protected abstract boolean isGivenNodeInTree(HierarchyNode hierarchyNode);
}
