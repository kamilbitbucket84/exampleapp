package com.kamil.exampleapp.screen.multiviews.first;

import android.content.Context;
import android.os.Build;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.annotation.StyleRes;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;

import com.kamil.exampleapp.R;
import com.kamil.exampleapp.common.di.Dagger;
import com.kamil.exampleapp.common.ui.savedview.EnumViewStateRepository;
import com.kamil.exampleapp.common.ui.savedview.SavedStateView;
import com.kamil.exampleapp.common.util.LogUtils;
import com.kamil.exampleapp.screen.multiviews.first.di.DaggerFirstViewComponent;
import com.kamil.exampleapp.screen.multiviews.first.di.FirstViewModule;

import java.util.HashMap;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FirstView extends FrameLayout implements SavedStateView, FirstMvpView {

    private static final String KEY_NAME_STATE = "NAME_STATE";
    private static final String KEY_SURNAME_STATE = "SURNAME_STATE";
    private static final String KEY_HOBBY_STATE = "HOBBY_STATE";
    private static final String KEY_SALARY_DESIRE_STATE = "SALARY_DESIRE_STATE";
    private static final String KEY_SCROLL_VERTICAL_STATE = "SCROLL_VERTICAL_STATE";
    private static final String KEY_ABOUT_STATE = "ABOUT_STATE";

    @BindView(R.id.view_multi_view_first_name) EditText nameInput;
    @BindView(R.id.view_multi_view_first_surname) EditText surnameInput;
    @BindView(R.id.view_multi_view_first_hobby) EditText hobbyInput;
    @BindView(R.id.view_multi_view_first_salary) EditText salaryInput;
    @BindView(R.id.view_multi_view_first_about) EditText aboutInput;
    @BindView(R.id.view_multi_view_first_submit) FloatingActionButton submitForm;
    @BindView(R.id.view_multi_view_first_scroll_view) ScrollView scrollView;
    @BindView(R.id.view_multi_view_first_progressbar) ProgressBar progressBar;

    private String viewSaveStateKey;
    private FirstViewEventListener firstViewEventListener;
    @Inject FirstViewPresenter firstViewPresenter;

    public FirstView(@NonNull Context context) {
        this(context, null);
    }

    public FirstView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FirstView(@NonNull Context context,
                     @Nullable AttributeSet attrs,
                     @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public FirstView(@NonNull Context context,
                     @Nullable AttributeSet attrs,
                     @AttrRes int defStyleAttr,
                     @StyleRes int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.view_multi_view_first, this);
        ButterKnife.bind(this);
        setFocusable(true);
        setFocusableInTouchMode(true);
    }

    public void setFirstViewEventListener(FirstViewEventListener firstViewEventListener) {
        this.firstViewEventListener = firstViewEventListener;
    }


    @Override
    public void setStateKey(String key) {
        this.viewSaveStateKey = key;
    }

    @Override
    public void saveState() {
        HashMap<String, Object> savedState = new HashMap<>();
        savedState.put(KEY_NAME_STATE, nameInput.getText().toString());
        savedState.put(KEY_SURNAME_STATE, surnameInput.getText().toString());
        savedState.put(KEY_HOBBY_STATE, hobbyInput.getText().toString());
        savedState.put(KEY_SALARY_DESIRE_STATE, salaryInput.getText().toString());
        savedState.put(KEY_SCROLL_VERTICAL_STATE, scrollView.getScrollY());
        savedState.put(KEY_ABOUT_STATE, aboutInput.getText().toString());

        EnumViewStateRepository.INSTANCE.saveViewState(viewSaveStateKey, savedState);
    }

    @Override
    public void restoreState() {
        final HashMap<String, Object> savedState = EnumViewStateRepository.INSTANCE.getViewState(viewSaveStateKey);

        if (savedState != null) {
            nameInput.setText((String) savedState.get(KEY_NAME_STATE));
            surnameInput.setText((String) savedState.get(KEY_SURNAME_STATE));
            hobbyInput.setText((String) savedState.get(KEY_HOBBY_STATE));
            salaryInput.setText((String) savedState.get(KEY_SALARY_DESIRE_STATE));
            aboutInput.setText((String) savedState.get(KEY_ABOUT_STATE));

            scrollView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    scrollView.scrollTo(0, (Integer) savedState.get(KEY_SCROLL_VERTICAL_STATE));
                    scrollView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    // UI events
    ////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        restoreState();
        firstViewEventListener.onFirstViewAttached(this);
        initInjector();
        firstViewPresenter.attachView(this);
    }

    private void initInjector() {
        DaggerFirstViewComponent.builder()
                .appComponent(Dagger.appComponent())
                .firstViewModule(new FirstViewModule())
                .build()
                .inject(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        saveState();
        firstViewEventListener.onFirstViewDetached();
        firstViewPresenter.detachView();
    }

    @OnClick(R.id.view_multi_view_first_submit)
    protected void onFormSubmitClicked() {
        firstViewPresenter.onSubmitFormClicked();
    }

    public void onViewDisplayed() {
        LogUtils.d("FIRST on view displayed");
    }

    ////////////////////////////////////////////////////////////////////////////////
    // MVP methods
    ////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onSendFormStarted() {
        progressBar.setVisibility(VISIBLE);
    }

    @Override
    public void onSendFormSuccess() {
        progressBar.setVisibility(GONE);
        nameInput.setText("");
        surnameInput.setText("");
        aboutInput.setText("");
        hobbyInput.setText("");
        salaryInput.setText("");
        EnumViewStateRepository.INSTANCE.clearViewState(viewSaveStateKey);
    }

    @Override
    public void onSendFormError() {
        progressBar.setVisibility(GONE);
    }
}
