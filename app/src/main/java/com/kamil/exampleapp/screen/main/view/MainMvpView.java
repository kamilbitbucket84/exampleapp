package com.kamil.exampleapp.screen.main.view;

import com.kamil.exampleapp.common.mvp.MvpView;
import com.kamil.exampleapp.screen.main.model.HierarchyTree;

import java.util.List;

public interface MainMvpView extends MvpView {
    void onGetHierarchyTreesStarted();
    void onGetHierarchyTreesSuccessEmpty();
    void onGetHierarchyTreesSuccessNotEmpty(List<HierarchyTree> hierarchyTrees);
    void onGetHierarchyTreesError();
}
