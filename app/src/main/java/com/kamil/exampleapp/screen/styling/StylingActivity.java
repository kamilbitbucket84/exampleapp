package com.kamil.exampleapp.screen.styling;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;

import com.kamil.exampleapp.R;
import com.kamil.exampleapp.common.ui.CropBottomScaleTransformation;
import com.kamil.exampleapp.common.util.MockedDataUtils;
import com.kamil.exampleapp.common.view.BaseActivity;
import com.squareup.picasso.Picasso;

import butterknife.BindDimen;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class StylingActivity extends BaseActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.styling_collapsing_toolbar_layout) CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.styling_toolbar_image) ImageView collapsingImage;
    @BindView(R.id.styling_password_input_layout) TextInputLayout passwordInputLayout;
    @BindView(R.id.styling_password_input) TextInputEditText passwordInput;
    @BindView(R.id.styling_login_input_layout) TextInputLayout loginInputLayout;
    @BindView(R.id.styling_login_input) TextInputEditText loginInput;
    @BindView(R.id.styling_button_correct_login) Button correctLogin;
    @BindView(R.id.styling_button_correct_password) Button correctPassword;
    @BindView(R.id.styling_button_error_login) Button errorLogin;
    @BindView(R.id.styling_button_error_password) Button errorPassword;
    @BindString(R.string.styling_incorrect_login_or_password) String incorrectCredentials;
    @BindString(R.string.empty_title) String emptyTitle;
    @BindString(R.string.styling_title) String collapsingTitle;
    @BindDimen(R.dimen.styling_collapsing_toolbar_height) int collapsingToolbarHeight;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_styling);
        ButterKnife.bind(this);
        configureWeirdPasswordEditTextBehavior();
        configureToolbar();
        loadImageIntoCollapsingToolbar();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return true;
    }

    private void configureToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            collapsingToolbarLayout.setTitle(collapsingTitle);
        }
    }

    private void loadImageIntoCollapsingToolbar() {
        DisplayMetrics dm = getResources().getDisplayMetrics();
        int screenWidth = dm.widthPixels;

        Picasso.with(this)
                .load(MockedDataUtils.EXAMPLE_PHOTO)
                .transform(new CropBottomScaleTransformation(screenWidth,
                        collapsingToolbarHeight,
                        MockedDataUtils.EXAMPLE_PHOTO))
                .into(collapsingImage);
    }

    private void configureWeirdPasswordEditTextBehavior() {
        // when you set inputType in xml for textPassword
        // android change typeface to monospace
        // in order to avoid it
        // remove inputType from xml and set it here
        // and after that apply previous typeface

        // https://github.com/chrisjenx/Calligraphy/issues/186
        // http://stackoverflow.com/questions/9892617/programmatically-change-input-type-of-the-edittext-from-password-to-normal-vic

        Typeface typeface = passwordInputLayout.getTypeface();
        passwordInput.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        passwordInput.setTypeface(typeface);
    }

    @OnClick(R.id.styling_button_correct_password)
    public void correctPasswordClicked() {
        passwordInputLayout.setErrorEnabled(false);
    }

    @OnClick(R.id.styling_button_error_password)
    public void errorPasswordClicked() {
        passwordInputLayout.setErrorEnabled(true);
        passwordInputLayout.setError(incorrectCredentials);
    }

    @OnClick(R.id.styling_button_correct_login)
    public void correctloginClicked() {
        loginInputLayout.setErrorEnabled(false);
    }

    @OnClick(R.id.styling_button_error_login)
    public void errorloginClicked() {
        loginInputLayout.setErrorEnabled(true);
        loginInputLayout.setError(incorrectCredentials);
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, StylingActivity.class);
    }
}
