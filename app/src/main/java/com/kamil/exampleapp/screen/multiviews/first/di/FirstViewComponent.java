package com.kamil.exampleapp.screen.multiviews.first.di;

import com.kamil.exampleapp.common.di.AppComponent;
import com.kamil.exampleapp.common.di.PerView;
import com.kamil.exampleapp.screen.multiviews.first.FirstView;

import dagger.Component;

/**
 * Created by Kamil on 02/10/2017.
 */

@PerView
@Component(dependencies = AppComponent.class, modules = FirstViewModule.class)
public interface FirstViewComponent {
    void inject(FirstView firstView);
}
