package com.kamil.exampleapp.screen.main.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kamil.exampleapp.R;
import com.kamil.exampleapp.screen.main.model.HierarchyTree;

import java.util.ArrayList;
import java.util.List;

public class HierarchyListAdapter extends RecyclerView.Adapter<HierarchyViewHolder> {

    private List<HierarchyTree> hierarchyTrees = new ArrayList<>();
    private LayoutInflater inflater;
    private OnHierarchyViewListItemClickListener onHierarchyViewListItemClickListener;

    public HierarchyListAdapter(Context context, OnHierarchyViewListItemClickListener listener) {
        inflater = LayoutInflater.from(context);
        this.onHierarchyViewListItemClickListener = listener;
    }

    @Override
    public HierarchyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_hierarchy, parent, false);
        return new HierarchyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HierarchyViewHolder holder, int position) {
        holder.bindHierarchyItem(hierarchyTrees.get(position), onHierarchyViewListItemClickListener);
    }

    @Override
    public int getItemCount() {
        return hierarchyTrees.size();
    }

    public void setHierarchyTrees(List<HierarchyTree> hierarchyTrees) {
        this.hierarchyTrees.clear();
        if (hierarchyTrees != null && !hierarchyTrees.isEmpty()) {
            this.hierarchyTrees.addAll(hierarchyTrees);
        }
        notifyDataSetChanged();
    }

    public void updateHierarchyTree(HierarchyTree updatedTree, int position) {
        hierarchyTrees.set(position, updatedTree);
        notifyItemChanged(position);
    }
}
