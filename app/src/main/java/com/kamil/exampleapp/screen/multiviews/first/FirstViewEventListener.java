package com.kamil.exampleapp.screen.multiviews.first;


/**
 * Created by Kamil on 27/09/2017.
 */

public interface FirstViewEventListener {

    void onFirstViewAttached(FirstView firstView);

    void onFirstViewDetached();

    FirstViewEventListener EMPTY = new FirstViewEventListener() {
        @Override
        public void onFirstViewAttached(FirstView firstView) {

        }

        @Override
        public void onFirstViewDetached() {

        }
    };
}
