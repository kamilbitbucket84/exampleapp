package com.kamil.exampleapp.screen.main.di;

import com.kamil.exampleapp.common.di.AppComponent;
import com.kamil.exampleapp.common.di.PerActivity;
import com.kamil.exampleapp.screen.main.view.MainActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = AppComponent.class, modules = MainModule.class)
public interface MainComponent {
    void inject(MainActivity mainActivity);
}
