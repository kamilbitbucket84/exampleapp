package com.kamil.exampleapp.screen.multiviews.first;

import com.kamil.exampleapp.common.mvp.MvpView;

/**
 * Created by Kamil on 02/10/2017.
 */

public interface FirstMvpView extends MvpView {

    void onSendFormStarted();

    void onSendFormSuccess();

    void onSendFormError();
}
