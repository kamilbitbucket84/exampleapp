package com.kamil.exampleapp.screen.browseproducts.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kamil.exampleapp.R;
import com.kamil.exampleapp.common.list.ListItem;
import com.kamil.exampleapp.screen.browseproducts.model.ProductItem;

import java.util.ArrayList;
import java.util.List;

public class BrowseProductsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutInflater inflater;
    private List<ProductItem> productItems = new ArrayList<>();
    private OnProductItemClickListener onProductItemClickListener;

    public BrowseProductsListAdapter(Context context, OnProductItemClickListener onProductItemClickListener) {
        this.inflater = LayoutInflater.from(context);
        this.onProductItemClickListener = onProductItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_product, parent, false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ProductViewHolder productViewHolder = (ProductViewHolder) holder;
        productViewHolder.bindItem(productItems.get(position), onProductItemClickListener);
    }

    @Override
    public int getItemViewType(int position) {
        return productItems.get(position).getItemViewType();
    }

    @Override
    public int getItemCount() {
        return productItems.size();
    }

    public void setProductItems(List<ProductItem> productItems) {
        this.productItems.clear();
        if (productItems != null && !productItems.isEmpty()) {
            this.productItems.addAll(productItems);
        }
        notifyDataSetChanged();
    }

    public void addProductItem(ProductItem productItem) {
        productItems.add(productItem);
        notifyItemInserted(productItems.size() - 1);
    }

    public void updateProductItem(ProductItem productItem) {
        int itemPosition = findItemPositionById(productItem);
        productItems.set(itemPosition, productItem);
        notifyItemChanged(itemPosition);
    }

    public void removeProductItem(ProductItem productItem) {
        int itemPosition = findItemPositionById(productItem);
        productItems.remove(itemPosition);
        notifyItemRemoved(itemPosition);
    }

    private int findItemPositionById(ProductItem productItem) {
        int position = 0;

        for(ProductItem item: productItems) {
            if(productItem.getId().equals(item.getId())) {
                break;
            }

            position++;
        }

        return position;
    }
}
