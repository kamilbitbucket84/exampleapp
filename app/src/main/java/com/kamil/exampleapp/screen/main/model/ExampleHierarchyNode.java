package com.kamil.exampleapp.screen.main.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExampleHierarchyNode extends HierarchyNode {

    private final Long id;
    private final String name;
    private final Long parentId;
    private HierarchyNode parentNode;
    private final List<HierarchyNode> children = new ArrayList<>();

    public ExampleHierarchyNode(Long id,
                                String name,
                                Long parentId) {
        super(id, name, parentId);
        this.id = id;
        this.name = name;
        this.parentId = parentId;
    }

    @Override
    public void connectParentNode(HierarchyNode parentNode) {
        this.parentNode = parentNode;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Long getParentId() {
        return parentId;
    }

    @Override
    public HierarchyNode getParentNode() {
        return parentNode;
    }

    @Override
    public List<HierarchyNode> getPreviousNodes() {
        List<HierarchyNode> allParents = new ArrayList<>();
        HierarchyNode parentNode = this.getParentNode();

        while (parentNode != null) {
            allParents.add(parentNode);
            parentNode = parentNode.getParentNode();
        }

        Collections.reverse(allParents);
        return allParents;
    }

    @Override
    public List<HierarchyNode> getChildren() {
        return children;
    }

    @Override
    public void addChild(HierarchyNode child) {
        children.add(child);
    }

    @Override
    public Boolean hasChildren() {
        return !children.isEmpty();
    }

    @Override
    public Boolean isRoot() {
        return parentNode == null;
    }
}
