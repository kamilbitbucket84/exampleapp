package com.kamil.exampleapp.screen.multifragment.view;

import com.kamil.exampleapp.common.list.ListItem;

/**
 * Created by Kamil on 10/05/2017.
 */

public class ExampleItem implements ListItem {

    public final String title;
    public final String input;
    public final boolean checked;
    public final int progress;

    public ExampleItem(String title, String input, boolean checked, int progress) {
        this.title = title;
        this.input = input;
        this.checked = checked;
        this.progress = progress;
    }

    @Override
    public int getItemViewType() {
        return ListItem.EXAMPLE;
    }
}
