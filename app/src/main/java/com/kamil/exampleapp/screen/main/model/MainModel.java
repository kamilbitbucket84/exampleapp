package com.kamil.exampleapp.screen.main.model;

import com.kamil.exampleapp.datasource.database.ExampleDatabaseClient;
import com.kamil.exampleapp.datasource.database.model.HierarchyDbEntityRefresh;
import com.kamil.exampleapp.screen.main.usecase.GetHierarchiesUseCase;

import java.util.List;

import rx.Observable;

public class MainModel {

    private final GetHierarchiesUseCase getHierarchiesUseCase;
    private final ExampleDatabaseClient dbClient;

    public MainModel(GetHierarchiesUseCase getHierarchiesUseCase, ExampleDatabaseClient dbClient) {
        this.getHierarchiesUseCase = getHierarchiesUseCase;
        this.dbClient = dbClient;
    }

    public Observable<List<HierarchyTree>> getHierarchyTrees() {
        return getHierarchiesUseCase.execute();
    }

    public Observable<HierarchyDbEntityRefresh> refreshDbHierarchies(List<HierarchyTree> hierarchyTrees) {
        return dbClient.refreshDbHierarchies(hierarchyTrees);
    }
}
