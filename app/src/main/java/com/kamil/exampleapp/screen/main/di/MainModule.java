package com.kamil.exampleapp.screen.main.di;

import com.kamil.exampleapp.datasource.api.FirebaseApiClient;
import com.kamil.exampleapp.datasource.database.ExampleDatabaseClient;
import com.kamil.exampleapp.screen.main.model.MainModel;
import com.kamil.exampleapp.screen.main.presenter.MainPresenter;
import com.kamil.exampleapp.screen.main.usecase.GetHierarchiesUseCase;

import dagger.Module;
import dagger.Provides;
import rx.Scheduler;

@Module
public class MainModule {

    @Provides
    GetHierarchiesUseCase provideHierarchiesUseCase(FirebaseApiClient firebaseApiClient, Scheduler scheduler) {
        return new GetHierarchiesUseCase(firebaseApiClient, scheduler);
    }

    @Provides
    MainModel provideModel(GetHierarchiesUseCase getHierarchiesUseCase, ExampleDatabaseClient databaseClient) {
        return new MainModel(getHierarchiesUseCase, databaseClient);
    }

    @Provides
    MainPresenter providePresenter(MainModel mainModel) {
        return new MainPresenter(mainModel);
    }
}
