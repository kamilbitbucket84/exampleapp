package com.kamil.exampleapp.screen.experiment.presenter;

import com.kamil.exampleapp.common.mvp.BaseMvpPresenter;
import com.kamil.exampleapp.screen.experiment.model.ExperimentModel;
import com.kamil.exampleapp.screen.experiment.view.ExperimentMvpView;

public class ExperimentPresenter extends BaseMvpPresenter<ExperimentMvpView> {

    private final ExperimentModel model;

    public ExperimentPresenter(ExperimentModel model) {
        this.model = model;
    }

    /////////////////////////////////////////////////////////////////////////
    //  intercept UI events
    /////////////////////////////////////////////////////////////////////////

    public void onScreenCreated() {
        getView().onConnectLaunchEventClient();
    }

    public void onScreenDestroyed() {
        getView().onDisconnectLaunchEventClient();
    }

    public void onMenuItemLoadFileClicked() {
        getView().askForAccessExternalStoragePermission();
    }

    public void onMenuItemGetUserLocationClicked() {
        getView().askForAccessFineLocationPermission();
    }

    public void onMenuItemStartProximityEventsLauncherClicked() {
        getView().onStartProximityEventsLauncher();
    }

    public void onMenuItemStopProximityEventsLauncherClicked() {
        getView().onStopProximityEventsLauncher();
    }

    public void onMenuItemStartTimeEventsLauncherClicked() {
        getView().onStartTimeEventsLauncher();
    }

    public void onMenuItemStopTimeEventsLauncherClicked() {
        getView().onStopTimeEventsLauncher();
    }

    public void onMenuItemConnectToExperimentServiceClicked() {
        getView().onConnectLaunchEventClient();
    }

    public void onMenuItemDisconnectFromExperimentServiceClicked() {
        getView().onDisconnectLaunchEventClient();
    }

    public void onAccessWriteExternalStoragePermissionGranted() {
        //  TODO on accessWriteExternalStorage granted
    }

    public void onAccessWriteExternalStoragePermissionRefused() {
        //  TODO on accessWriteExternalstorage refused
    }

    public void onAccessWriteExternalStoragePermissionNeverAskAgain(String title, String message) {
        getView().onPermissionNeverAskAgainClicked(title, message);
    }

    public void onAccessFineLocationPermissionGranted() {
        //  TODO on accessFineLocationPermission granted
    }

    public void onAccessFineLocationPermissionRefused() {
        //  TODO on accessFineLocationPermission granted
    }

    public void onAccessFineLocationPermissionNeverAskAgain(String title, String message) {
        getView().onPermissionNeverAskAgainClicked(title, message);
    }


    /////////////////////////////////////////////////////////////////////////
    //  load data from model
    /////////////////////////////////////////////////////////////////////////

}

