package com.kamil.exampleapp.screen.multifragment.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.kamil.exampleapp.R;
import com.kamil.exampleapp.common.view.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Kamil on 05/05/2017.
 */

public class MultiFragmentActivity extends BaseActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.multifragment_container) FrameLayout fragmentContainer;

    private FragmentDisplayManager fragmentDisplayManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_fragment);
        ButterKnife.bind(this);
        configureToolbar();
        configureFragmentManager();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        fragmentDisplayManager.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }

    private void configureToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void configureFragmentManager() {
        fragmentDisplayManager = new FragmentDisplayManager(getSupportFragmentManager(), fragmentContainer.getId());
        fragmentDisplayManager.insertFragmentIntoContainer(FirstFragment.newInstance(), FirstFragment.TAG);
        fragmentDisplayManager.insertFragmentIntoContainer(SecondFragment.newInstance(), SecondFragment.TAG);
        fragmentDisplayManager.insertFragmentIntoContainer(ThirdFragment.newInstance(), ThirdFragment.TAG);
        fragmentDisplayManager.insertFragmentIntoContainer(FourthFragment.newInstance(), FourthFragment.TAG);
        fragmentDisplayManager.insertFragmentIntoContainer(FifthFragment.newInstance(), FifthFragment.TAG);
    }

    @OnClick(R.id.multifragment_button_first)
    public void firstClicked() {
        fragmentDisplayManager.showFragmentWithTag(FirstFragment.TAG);
    }

    @OnClick(R.id.multifragment_button_second)
    public void secondClicked() {
        fragmentDisplayManager.showFragmentWithTag(SecondFragment.TAG);
    }

    @OnClick(R.id.multifragment_button_third)
    public void thirdClicked() {
        fragmentDisplayManager.showFragmentWithTag(ThirdFragment.TAG);
    }

    @OnClick(R.id.multifragment_button_fourth)
    public void fourthClicked() {
        fragmentDisplayManager.showFragmentWithTag(FourthFragment.TAG);
    }

    @OnClick(R.id.multifragment_button_fifth)
    public void fifthClicked() {
        fragmentDisplayManager.showFragmentWithTag(FifthFragment.TAG);
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, MultiFragmentActivity.class);
    }
}
