package com.kamil.exampleapp.screen.browseproducts.model;

import com.kamil.exampleapp.R;

public enum InFavoritesState {

    NOT_ADDED(R.drawable.favorites_not_added),
    ADDING(R.drawable.favorites_adding),
    ADDED(R.drawable.favorites_added),
    REMOVING(R.drawable.favorites_removing);

    public final int inFavoriteDrawableId;

    InFavoritesState(int inFavoriteDrawableId) {
        this.inFavoriteDrawableId = inFavoriteDrawableId;
    }
}
