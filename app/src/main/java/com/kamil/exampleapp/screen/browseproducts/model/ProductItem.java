package com.kamil.exampleapp.screen.browseproducts.model;

import com.kamil.exampleapp.common.list.ListItem;
import com.kamil.exampleapp.common.util.Preconditions;

import java.util.ArrayList;
import java.util.List;

public class ProductItem implements ListItem {

    private final Long id;
    private final String name;
    private final String brand;
    private final String price;
    private List<Long> assignedHierarchies = new ArrayList<>();
    private final InFavoritesState inFavoritesState;
    private final RemovalState removalState;
    private final boolean valid;

    public static class Builder {

        private Long id;
        private String name;
        private String brand;
        private String price;
        private List<Long> assignedHierarchies = new ArrayList<>();
        private InFavoritesState inFavoritesState;
        private RemovalState removalState;


        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder brand(String brand) {
            this.brand = brand;
            return this;
        }

        public Builder price(String price) {
            this.price = price;
            return this;
        }

        public Builder assignedHierarchies(List<Long> assignedHierarchies) {
            if (assignedHierarchies != null) {
                this.assignedHierarchies.addAll(assignedHierarchies);
            }

            return this;
        }

        public Builder inFavoriteState(InFavoritesState inFavoritesState) {
            this.inFavoritesState = inFavoritesState;
            return this;
        }

        public Builder removalState(RemovalState removalState) {
            this.removalState = removalState;
            return this;
        }

        public ProductItem build() {
            return new ProductItem(this);
        }

    }

    private ProductItem(Builder builder) {
        if (validateInputData(builder)) {
            this.valid = true;
            this.id = builder.id;
            this.name = builder.name;
            this.brand = builder.brand;
            this.price = builder.price;
            this.assignedHierarchies.addAll(builder.assignedHierarchies);
            this.inFavoritesState = builder.inFavoritesState;
            this.removalState = builder.removalState;
        } else {
            this.valid = false;
            this.id = builder.id;
            this.name = builder.name;
            this.brand = builder.brand;
            this.price = builder.price;
            this.assignedHierarchies.addAll(builder.assignedHierarchies);
            this.inFavoritesState = builder.inFavoritesState;
            this.removalState = builder.removalState;
        }
    }

    private boolean validateInputData(Builder builder) {
        try {

            if (builder.price != null) {
                Double.parseDouble(builder.price.trim());
            }

            return builder.id != null &&
                    builder.id != 0 &&
                    builder.name != null &&
                    !builder.name.trim().isEmpty() &&
                    builder.brand != null &&
                    !builder.brand.trim().isEmpty() &&
                    builder.price != null &&
                    !builder.price.trim().isEmpty() &&
                    builder.assignedHierarchies != null &&
                    !builder.assignedHierarchies.isEmpty() &&
                    builder.inFavoritesState != null &&
                    builder.removalState != null;

        } catch (NumberFormatException e) {
            return false;
        }
    }

    public boolean isValid() {
        return valid;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getBrand() {
        return brand;
    }

    public String getPrice() {
        return price;
    }

    public InFavoritesState getInFavoritesState() {
        return inFavoritesState;
    }

    public RemovalState getRemovalState() {
        return removalState;
    }

    public List<Long> getAssignedHierarchies() {
        return assignedHierarchies;
    }

    @Override
    public int getItemViewType() {
        return ListItem.PRODUCT;
    }

    public static ProductItem copyAndModify(ProductItem source, InFavoritesState state) {
        Preconditions.checkNotNull(source);
        return new Builder()
                .id(source.id)
                .name(source.name)
                .brand(source.brand)
                .price(source.price)
                .assignedHierarchies(source.assignedHierarchies)
                .inFavoriteState(state)
                .removalState(source.getRemovalState())
                .build();
    }

    public static ProductItem copyAndModify(ProductItem source, RemovalState state) {
        Preconditions.checkNotNull(source);
        return new Builder()
                .id(source.id)
                .name(source.name)
                .brand(source.brand)
                .price(source.price)
                .assignedHierarchies(source.assignedHierarchies)
                .inFavoriteState(source.getInFavoritesState())
                .removalState(state)
                .build();
    }
}
