package com.kamil.exampleapp.screen.browseproducts.view;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kamil.exampleapp.R;
import com.kamil.exampleapp.common.util.ImageUtils;
import com.kamil.exampleapp.screen.browseproducts.model.InFavoritesState;
import com.kamil.exampleapp.screen.browseproducts.model.ProductItem;
import com.kamil.exampleapp.screen.browseproducts.model.RemovalState;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProductViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.item_kitchen_root_container) RelativeLayout rootContainer;
    @BindView(R.id.item_product_name) TextView nameTextView;
    @BindView(R.id.item_product_brand) TextView brandTextView;
    @BindView(R.id.item_product_price) TextView priceTextView;
    @BindView(R.id.item_product_image) ImageView thumbImageView;
    @BindView(R.id.item_product_add_to_favorites) ImageView addToFavoritesImageView;
    @BindView(R.id.item_product_remove) ImageView removeImageView;
    @BindString(R.string.item_product_price) String priceText;


    private OnProductItemClickListener onProductItemClickListener = OnProductItemClickListener.EMPTY;
    private ProductItem productItem;

    public ProductViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bindItem(ProductItem productItem, OnProductItemClickListener onProductItemClickListener) {
        this.onProductItemClickListener = onProductItemClickListener;
        this.productItem = productItem;

        nameTextView.setText(productItem.getName());
        brandTextView.setText(productItem.getBrand());
        priceTextView.setText(String.format(priceText, Double.parseDouble(productItem.getPrice())));

        modifyUIonBind(productItem);
        ImageUtils.loadImageSafelyCenterInside(R.drawable.product, thumbImageView);
    }

    private void modifyUIonBind(ProductItem productItem) {
        addToFavoritesImageView.setImageDrawable(ContextCompat.getDrawable(itemView.getContext(),
                productItem.getInFavoritesState().inFavoriteDrawableId));

        removeImageView.setImageDrawable(ContextCompat.getDrawable(itemView.getContext(),
                productItem.getRemovalState().drawableResId));
    }

    @OnClick(R.id.item_product_add_to_favorites)
    protected void onAddToFavoritesClicked() {
        if (productItem.getInFavoritesState() == InFavoritesState.NOT_ADDED &&
                productItem.getRemovalState() != RemovalState.REMOVING) {
            onProductItemClickListener.onAddToFavoriteClicked(productItem);
        } else if (productItem.getInFavoritesState() == InFavoritesState.ADDED &&
                productItem.getRemovalState() != RemovalState.REMOVING) {
            onProductItemClickListener.onRemoveFromFavoriteClicked(productItem);
        }
    }

    @OnClick(R.id.item_product_remove)
    protected void onRemoveClicked() {
        if (productItem.getRemovalState() == RemovalState.NOT_REMOVED &&
                productItem.getInFavoritesState() != InFavoritesState.ADDING &&
                productItem.getInFavoritesState() != InFavoritesState.REMOVING) {
            onProductItemClickListener.onRemoveProductItemClicked(productItem);
        }
    }
}
