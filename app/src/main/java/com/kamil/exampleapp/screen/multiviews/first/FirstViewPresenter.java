package com.kamil.exampleapp.screen.multiviews.first;

import com.kamil.exampleapp.common.mvp.BaseMvpPresenter;
import com.kamil.exampleapp.common.util.LogUtils;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Kamil on 02/10/2017.
 */

public class FirstViewPresenter extends BaseMvpPresenter<FirstMvpView> {

    private final FirstViewModel firstViewModel;

    public FirstViewPresenter(FirstViewModel firstViewModel) {
        this.firstViewModel = firstViewModel;
    }

    public void onSubmitFormClicked() {
        sendForm();
    }

    private void sendForm() {
        getView().onSendFormStarted();

        subscriptions.add(firstViewModel.mockedSendForm()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                    LogUtils.d("SUCCESS");
                    getView().onSendFormSuccess();
                }, throwable -> {
                    LogUtils.d("ERROR");
                    getView().onSendFormError();
                })
        );
    }

}
