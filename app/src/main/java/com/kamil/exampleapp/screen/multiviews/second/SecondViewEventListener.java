package com.kamil.exampleapp.screen.multiviews.second;


/**
 * Created by Kamil on 28/09/2017.
 */

public interface SecondViewEventListener {

    void onSecondViewAttached(SecondView secondView);

    void onSecondViewDetached();

    SecondViewEventListener EMPTY = new SecondViewEventListener() {
        @Override
        public void onSecondViewAttached(SecondView secondView) {

        }

        @Override
        public void onSecondViewDetached() {

        }
    };
}
