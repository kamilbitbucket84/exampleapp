package com.kamil.exampleapp.screen.main.model;

import com.kamil.exampleapp.datasource.api.model.HierarchyNodeEntity;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

@Parcel
public class HierarchyPack {
    public HierarchyNodeEntity currentEntity;
    public List<HierarchyNodeEntity> parentEntities;

    public List<Long> getAllHierarchyIds() {
        List<Long> allHierarchyIds = new ArrayList<>();
        allHierarchyIds.add(currentEntity.id);
        for (HierarchyNodeEntity entity: parentEntities) {
            allHierarchyIds.add(entity.id);
        }

        return allHierarchyIds;
    }
}
