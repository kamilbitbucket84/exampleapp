package com.kamil.exampleapp.screen.simpletutorial;

import com.kamil.exampleapp.R;

public enum SimpleTutorialData {

    FIRST_EXPLORE(
            R.string.simple_view_pager_first_explore_title,
            R.string.simple_view_pager_first_explore_subtitle,
            R.drawable.kicked_square),

    SECOND_READ(
            R.string.simple_view_pager_second_read_title,
            R.string.simple_view_pager_second_read_subtitle,
            R.drawable.square),

    THIRD_ADMIRE(
            R.string.simple_view_pager_third_admire_title,
            R.string.simple_view_pager_third_admire_subtitle,
            R.drawable.triangle),

    FOURTH_USE(
            R.string.simple_view_pager_fourth_use_title,
            R.string.simple_view_pager_fourth_use_subtitle,
            R.drawable.kicked_square);

    private int titleResId;
    private int subTitleResId;
    private int imageResId;

    SimpleTutorialData(int titleResId,
                       int subTitleResId,
                       int imageResId) {
        this.titleResId = titleResId;
        this.subTitleResId = subTitleResId;
        this.imageResId = imageResId;
    }

    public int getTitleResId() {
        return titleResId;
    }

    public int getSubTitleResId() {
        return subTitleResId;
    }

    public int getImageResId() {
        return imageResId;
    }
}
