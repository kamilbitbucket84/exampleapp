package com.kamil.exampleapp.screen.main.model;

import java.util.ArrayList;
import java.util.List;

public class HierarchyTreeNodeConnector {

    public HierarchyNode findRootNode(List<HierarchyNode> hierarchyNodes) {
        HierarchyNode rootNode = null;
        for (HierarchyNode node : hierarchyNodes) {
            if (node.getParentId() == null) {
                rootNode = node;
            }
        }
        return rootNode;
    }

    public void connectAllNodes(HierarchyNode rootNode, List<HierarchyNode> hierarchyNodes) {
        findAndConnectChildrenNodes(rootNode, hierarchyNodes);
    }

    private void findAndConnectChildrenNodes(HierarchyNode currentNode, List<HierarchyNode> hierarchyNodes) {
        List<HierarchyNode> foundChildren = findAllChildrenInList(currentNode, hierarchyNodes);
        if (!foundChildren.isEmpty()) {
            for (HierarchyNode child : foundChildren) {
                currentNode.addChild(child);
                child.connectParentNode(currentNode);
                findAndConnectChildrenNodes(child, hierarchyNodes);
            }
        }
    }

    private List<HierarchyNode> findAllChildrenInList(HierarchyNode node, List<HierarchyNode> hierarchyNodes) {
        List<HierarchyNode> foundChildren = new ArrayList<>();
        for (HierarchyNode child : hierarchyNodes) {
            if (node.getId().equals(child.getParentId())) {
                foundChildren.add(child);
            }
        }
        return foundChildren;
    }
}
