package com.kamil.exampleapp.screen.browseproducts.model;

import com.kamil.exampleapp.common.list.ListItem;
import com.kamil.exampleapp.common.util.Preconditions;
import com.kamil.exampleapp.datasource.database.ExampleDatabaseClient;
import com.kamil.exampleapp.datasource.database.model.HierarchyDbEntity;
import com.kamil.exampleapp.datasource.database.model.ProductDbEntity;

import java.util.List;

import rx.Observable;

public class BrowseProductsModel {

    private final ExampleDatabaseClient dbClient;

    public BrowseProductsModel(ExampleDatabaseClient dbClient) {
        Preconditions.checkNotNull(dbClient);
        this.dbClient = dbClient;
    }

    public Observable<ProductItem> insertProductAndAssignToHierarchies(ProductItem productItem) {
        return dbClient.insertProductItemWithAssignedHierarchies(productItem);
    }

    public Observable<List<ProductItem>> selectAllProductsWithHierarchyId(Long hierarchyId) {
        return dbClient.selectProductItemsWithHierarchyId(hierarchyId);
    }

    public Observable<List<ProductItem>> selectProductWithHierarchyIdFilteredWithQuery(Long hierarchyId, String query) {
        return dbClient.selectProductItemsWithHierarchyIdAndQuery(hierarchyId, query);
    }

    public Observable<Boolean> deleteProductItem(ProductItem productItem) {
        return dbClient.removeProductItem(productItem);
    }

    public Observable<Boolean> addToFavorites(ProductItem productItem) {
        return dbClient.addToFavorites(productItem);
    }

    public Observable<Boolean> removeFromFavorites(ProductItem productItem) {
        return dbClient.removeFromFavorites(productItem);
    }

    public Observable<List<HierarchyDbEntity>> selectAllHierarchies() {
        return dbClient.selectAllHierarchyDbEntities();
    }

    public Observable<List<ProductDbEntity>> selectAllProducts() {
        return dbClient.selectAllProductDbEntities();
    }

}
