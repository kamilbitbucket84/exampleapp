package com.kamil.exampleapp.screen.simpletutorial;

import android.content.res.Resources;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kamil.exampleapp.R;
import com.kamil.exampleapp.common.util.ImageUtils;

public class SimpleTutorialViewPagerAdapter extends PagerAdapter {

    @Override
    public int getCount() {
        return SimpleTutorialData.values().length;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View pageView = LayoutInflater.from(container.getContext())
                .inflate(R.layout.view_simple_tutorial, container, false);
        findViewsAndBindData(pageView, SimpleTutorialData.values()[position]);
        container.addView(pageView);

        return pageView;
    }

    private void findViewsAndBindData(View view, SimpleTutorialData screen) {
        Resources resources = view.getResources();
        TextView title = (TextView) view.findViewById(R.id.view_simple_tutorial_title);
        TextView subtitle = (TextView) view.findViewById(R.id.view_simple_tutorial_subtitle);
        ImageView image = (ImageView) view.findViewById(R.id.view_simple_tutorial_image);

        title.setText(resources.getString(screen.getTitleResId()));
        subtitle.setText(resources.getString(screen.getSubTitleResId()));
        ImageUtils.loadImageSafelyCenterInside(screen.getImageResId(), image);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
