package com.kamil.exampleapp.screen.browseproducts.view;

import com.kamil.exampleapp.screen.browseproducts.model.ProductItem;

public interface OnProductItemClickListener {

    void onAddToFavoriteClicked(ProductItem productItem);

    void onRemoveFromFavoriteClicked(ProductItem productItem);

    void onRemoveProductItemClicked(ProductItem productItem);

    OnProductItemClickListener EMPTY = new OnProductItemClickListener() {
        @Override
        public void onAddToFavoriteClicked(ProductItem productItem) {
        }

        @Override
        public void onRemoveFromFavoriteClicked(ProductItem productItem) {
        }

        @Override
        public void onRemoveProductItemClicked(ProductItem productItem) {
        }
    };
}
