package com.kamil.exampleapp.screen.browseproducts.di;

import com.kamil.exampleapp.common.di.AppComponent;
import com.kamil.exampleapp.common.di.PerActivity;
import com.kamil.exampleapp.screen.browseproducts.view.BrowseProductsActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = AppComponent.class, modules = BrowseProductsModule.class)
public interface BrowseProductsComponent {
    void inject(BrowseProductsActivity browseProductsActivity);
}
