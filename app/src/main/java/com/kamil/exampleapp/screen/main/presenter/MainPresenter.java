package com.kamil.exampleapp.screen.main.presenter;

import com.kamil.exampleapp.common.mvp.BaseMvpPresenter;
import com.kamil.exampleapp.common.util.LogUtils;
import com.kamil.exampleapp.datasource.api.TestApiHeaders;
import com.kamil.exampleapp.datasource.database.DatabaseScheduler;
import com.kamil.exampleapp.screen.main.model.HierarchyTree;
import com.kamil.exampleapp.screen.main.model.MainModel;
import com.kamil.exampleapp.screen.main.view.MainMvpView;

import java.util.List;

import rx.Subscription;
import rx.schedulers.Schedulers;

public class MainPresenter extends BaseMvpPresenter<MainMvpView> {

    private final MainModel mainModel;

    public MainPresenter(MainModel mainModel) {
        this.mainModel = mainModel;
    }

    ////////////////////////////////////////////////////////////////////
    // UI events
    ////////////////////////////////////////////////////////////////////

    public void onScreenCreated() {
        loadHierarchies();
    }

    public void onMenuItemRefreshClicked() {
        loadHierarchies();
    }

    ////////////////////////////////////////////////////////////////////
    // LOAD Data from model
    ////////////////////////////////////////////////////////////////////

    private void loadHierarchies() {
        getView().onGetHierarchyTreesStarted();

        Subscription getHierarchiesSubscription =
                mainModel.getHierarchyTrees()
                        .filter(hierarchyTrees -> {
                            if (hierarchyTrees != null && hierarchyTrees.isEmpty() || hierarchyTrees == null) {
                                getView().onGetHierarchyTreesSuccessEmpty();
                                return false;
                            } else {
                                return true;
                            }
                        })
                        .subscribe(hierarchyTrees -> {
                            getView().onGetHierarchyTreesSuccessNotEmpty(hierarchyTrees);
                            refreshDbHierarchies(hierarchyTrees);
                        }, throwable -> {
                            getView().onGetHierarchyTreesError();
                        });

        subscriptions.add(getHierarchiesSubscription);
    }

    private void refreshDbHierarchies(List<HierarchyTree> hierarchyTrees) {
        Subscription refreshHierarchies =
                mainModel.refreshDbHierarchies(hierarchyTrees)
                        .subscribeOn(DatabaseScheduler.INSTANCE.getScheduler())
                        .observeOn(Schedulers.io())
                        .subscribe(hierarchyDbEntityRefresh -> {
                        }, throwable -> {
                            LogUtils.d("refresh ERROR: " + throwable);
                        });

        subscriptions.add(refreshHierarchies);
    }
}
