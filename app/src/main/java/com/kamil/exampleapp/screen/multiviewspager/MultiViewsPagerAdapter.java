package com.kamil.exampleapp.screen.multiviewspager;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.kamil.exampleapp.common.ui.savedview.EnumViewStateRepository;
import com.kamil.exampleapp.screen.multiviews.first.FirstView;
import com.kamil.exampleapp.screen.multiviews.first.FirstViewEventListener;
import com.kamil.exampleapp.screen.multiviews.fourth.FourthView;
import com.kamil.exampleapp.screen.multiviews.fourth.FourthViewEventListener;
import com.kamil.exampleapp.screen.multiviews.second.SecondView;
import com.kamil.exampleapp.screen.multiviews.second.SecondViewEventListener;
import com.kamil.exampleapp.screen.multiviews.third.ThirdView;
import com.kamil.exampleapp.screen.multiviews.third.ThirdViewEventListener;

/**
 * Created by Kamil on 29/09/2017.
 */

public class MultiViewsPagerAdapter extends PagerAdapter {

    private FirstViewEventListener firstViewEventListener;
    private SecondViewEventListener secondViewEventListener;
    private ThirdViewEventListener thirdViewEventListener;
    private FourthViewEventListener fourthViewEventListener;
    private String firstTitle;
    private String secondTitle;
    private String thirdTitle;
    private String fourthTitle;

    public MultiViewsPagerAdapter(String firstTitle,
                                  String secondTitle,
                                  String thirdTitle,
                                  String fourthTitle) {
        this.firstTitle = firstTitle;
        this.secondTitle = secondTitle;
        this.thirdTitle = thirdTitle;
        this.fourthTitle = fourthTitle;
    }

    public void setFirstViewEventListener(FirstViewEventListener firstViewEventListener) {
        this.firstViewEventListener = firstViewEventListener;
    }

    public void setSecondViewEventListener(SecondViewEventListener secondViewEventListener) {
        this.secondViewEventListener = secondViewEventListener;
    }

    public void setThirdViewEventListener(ThirdViewEventListener thirdViewEventListener) {
        this.thirdViewEventListener = thirdViewEventListener;
    }

    public void setFourthViewEventListener(FourthViewEventListener fourthViewEventListener) {
        this.fourthViewEventListener = fourthViewEventListener;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        switch (position) {
            case 0:
                FirstView firstView = new FirstView(container.getContext());
                firstView.setStateKey(EnumViewStateRepository.FIRST_VIEW_PAGER_STATE);
                firstView.setFirstViewEventListener(firstViewEventListener);
                container.addView(firstView);
                return firstView;
            case 1:
                SecondView secondView = new SecondView(container.getContext());
                secondView.setStateKey(EnumViewStateRepository.SECOND_VIEW_PAGER_STATE);
                secondView.setSecondViewEventListener(secondViewEventListener);
                container.addView(secondView);
                return secondView;
            case 2:
                ThirdView thirdView = new ThirdView(container.getContext());
                thirdView.setStateKey(EnumViewStateRepository.THIRD_VIEW_PAGER_STATE);
                thirdView.setThirdViewEventListener(thirdViewEventListener);
                container.addView(thirdView);
                return thirdView;
            default:
                FourthView fourthView = new FourthView(container.getContext());
                fourthView.setStateKey(EnumViewStateRepository.FOURTH_VIEW_PAGER_STATE);
                fourthView.setFourthViewEventListener(fourthViewEventListener);
                container.addView(fourthView);
                return fourthView;
        }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title;
        switch (position) {
            case 0:
                title = firstTitle;
                break;
            case 1:
                title = secondTitle;
                break;
            case 2:
                title = thirdTitle;
                break;
            default:
                title = fourthTitle;
                break;
        }

        return title;
    }
}
