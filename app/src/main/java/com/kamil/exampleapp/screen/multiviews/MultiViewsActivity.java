package com.kamil.exampleapp.screen.multiviews;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.kamil.exampleapp.R;
import com.kamil.exampleapp.common.ui.savedview.EnumViewStateRepository;
import com.kamil.exampleapp.common.ui.savedview.MultiViewDisplayManager;
import com.kamil.exampleapp.common.view.BaseActivity;
import com.kamil.exampleapp.screen.multiviews.first.FirstView;
import com.kamil.exampleapp.screen.multiviews.first.FirstViewEventListener;
import com.kamil.exampleapp.screen.multiviews.fourth.FourthView;
import com.kamil.exampleapp.screen.multiviews.fourth.FourthViewEventListener;
import com.kamil.exampleapp.screen.multiviews.second.SecondView;
import com.kamil.exampleapp.screen.multiviews.second.SecondViewEventListener;
import com.kamil.exampleapp.screen.multiviews.third.ThirdView;
import com.kamil.exampleapp.screen.multiviews.third.ThirdViewEventListener;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Kamil on 29/09/2017.
 */

public class MultiViewsActivity extends BaseActivity implements
        FirstViewEventListener,
        SecondViewEventListener,
        ThirdViewEventListener,
        FourthViewEventListener {

    private static final String KEY_ACTIVE_SCREEN_STATE = "ACTIVE_SCREEN_STATE";

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.multi_view_container) FrameLayout viewContainer;

    private MultiViewDisplayManager multiViewDisplayManager;
    private FirstView firstView;
    private SecondView secondView;
    private ThirdView thirdView;
    private FourthView fourthView;
    private ActiveView activeView;

    public enum ActiveView {
        FIRST,
        SECOND,
        THIRD,
        FOURTH
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_view);
        ButterKnife.bind(this);
        configureToolbar();
        multiViewDisplayManager = new MultiViewDisplayManager(viewContainer);
        showFirstView();
    }

    private void configureToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void showFirstView() {
        FirstView firstView = new FirstView(this);
        firstView.setFirstViewEventListener(this);
        multiViewDisplayManager.displayView(firstView, EnumViewStateRepository.FIRST_VIEW_MULTI_STATE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        HashMap<String, Object> savedState = new HashMap<>();
        savedState.put(KEY_ACTIVE_SCREEN_STATE, activeView);
        EnumViewStateRepository.INSTANCE.saveViewState(EnumViewStateRepository.MULTI_VIEW_ACTIVITY_STATE, savedState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        HashMap<String, Object> savedState
                = EnumViewStateRepository.INSTANCE.getViewState(EnumViewStateRepository.MULTI_VIEW_ACTIVITY_STATE);
        if (savedState != null) {
            ActiveView activeView = (ActiveView) savedState.get(KEY_ACTIVE_SCREEN_STATE);
            switch (activeView) {
                case FIRST:
                    FirstView firstView = new FirstView(this);
                    firstView.setFirstViewEventListener(this);
                    multiViewDisplayManager.displayView(firstView, EnumViewStateRepository.FIRST_VIEW_MULTI_STATE);
                    break;
                case SECOND:
                    SecondView secondView = new SecondView(this);
                    secondView.setSecondViewEventListener(this);
                    multiViewDisplayManager.displayView(secondView, EnumViewStateRepository.SECOND_VIEW_MULTI_STATE);
                    break;
                case THIRD:
                    ThirdView thirdView = new ThirdView(this);
                    thirdView.setThirdViewEventListener(this);
                    multiViewDisplayManager.displayView(thirdView, EnumViewStateRepository.THIRD_VIEW_MULTI_STATE);
                    break;
                case FOURTH:
                    FourthView fourthView = new FourthView(this);
                    fourthView.setFourthViewEventListener(this);
                    multiViewDisplayManager.displayView(fourthView, EnumViewStateRepository.FOURTH_VIEW_MULTI_STATE);
                    break;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        multiViewDisplayManager.clear();
        firstView = null;
        secondView = null;
        thirdView = null;
        fourthView = null;

        clearStateWhenFinishing();
    }

    private void clearStateWhenFinishing() {
        if (!isChangingConfigurations()) {
            EnumViewStateRepository.INSTANCE.clearViewState(EnumViewStateRepository.FIRST_VIEW_MULTI_STATE);
            EnumViewStateRepository.INSTANCE.clearViewState(EnumViewStateRepository.SECOND_VIEW_MULTI_STATE);
            EnumViewStateRepository.INSTANCE.clearViewState(EnumViewStateRepository.THIRD_VIEW_MULTI_STATE);
            EnumViewStateRepository.INSTANCE.clearViewState(EnumViewStateRepository.FOURTH_VIEW_MULTI_STATE);

            EnumViewStateRepository.INSTANCE.clearViewState(EnumViewStateRepository.MULTI_VIEW_ACTIVITY_STATE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return true;
    }

    @OnClick(R.id.multi_view_first_button)
    protected void firstClicked() {
        FirstView firstView = new FirstView(this);
        firstView.setFirstViewEventListener(this);
        multiViewDisplayManager.displayView(firstView, EnumViewStateRepository.FIRST_VIEW_MULTI_STATE);
    }

    @OnClick(R.id.multi_view_second_button)
    protected void secondClicked() {
        SecondView secondView = new SecondView(this);
        secondView.setSecondViewEventListener(this);
        multiViewDisplayManager.displayView(secondView, EnumViewStateRepository.SECOND_VIEW_MULTI_STATE);
    }

    @OnClick(R.id.multi_view_third_button)
    protected void thirdClicked() {
        ThirdView thirdView = new ThirdView(this);
        thirdView.setThirdViewEventListener(this);
        multiViewDisplayManager.displayView(thirdView, EnumViewStateRepository.THIRD_VIEW_MULTI_STATE);
    }

    @OnClick(R.id.multi_view_fourth_button)
    protected void fourthClicked() {
        FourthView fourthView = new FourthView(this);
        fourthView.setFourthViewEventListener(this);
        multiViewDisplayManager.displayView(fourthView, EnumViewStateRepository.FOURTH_VIEW_MULTI_STATE);
    }

    @Override
    public void onFirstViewAttached(FirstView firstView) {
        this.firstView = firstView;
        activeView = ActiveView.FIRST;
    }

    @Override
    public void onSecondViewAttached(SecondView secondView) {
        this.secondView = secondView;
        activeView = ActiveView.SECOND;
    }

    @Override
    public void onThirdViewAttached(ThirdView thirdView) {
        this.thirdView = thirdView;
        activeView = ActiveView.THIRD;
    }

    @Override
    public void onFourthViewAttached(FourthView fourthView) {
        this.fourthView = fourthView;
        activeView = ActiveView.FOURTH;
    }

    @Override
    public void onFirstViewDetached() {
    }

    @Override
    public void onSecondViewDetached() {
    }

    @Override
    public void onThirdViewDetached() {
    }

    @Override
    public void onFourthViewDetached() {
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (activeView != null) {
            switch (activeView) {
                case FIRST:
                    firstView.onViewDisplayed();
                    break;
                case SECOND:
                    secondView.onViewDisplayed();
                    break;
                case THIRD:
                    thirdView.onViewDisplayed();
                    break;
                case FOURTH:
                    fourthView.onViewDisplayed();
                    break;
            }
        }
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, MultiViewsActivity.class);
    }
}
