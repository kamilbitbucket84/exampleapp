package com.kamil.exampleapp.screen.multiviewspager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.kamil.exampleapp.R;
import com.kamil.exampleapp.common.ui.savedview.EnumViewStateRepository;
import com.kamil.exampleapp.common.view.BaseActivity;
import com.kamil.exampleapp.screen.multiviews.first.FirstView;
import com.kamil.exampleapp.screen.multiviews.first.FirstViewEventListener;
import com.kamil.exampleapp.screen.multiviews.fourth.FourthView;
import com.kamil.exampleapp.screen.multiviews.fourth.FourthViewEventListener;
import com.kamil.exampleapp.screen.multiviews.second.SecondView;
import com.kamil.exampleapp.screen.multiviews.second.SecondViewEventListener;
import com.kamil.exampleapp.screen.multiviews.third.ThirdView;
import com.kamil.exampleapp.screen.multiviews.third.ThirdViewEventListener;
import com.kamil.exampleapp.screen.simpletutorial.OnPageChangeListenerAdapter;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kamil on 29/09/2017.
 */

public class MultiViewsPagerActivity extends BaseActivity implements
        FirstViewEventListener,
        SecondViewEventListener,
        ThirdViewEventListener,
        FourthViewEventListener {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.multi_view_tab_tablayout) TabLayout tabLayout;
    @BindView(R.id.multi_view_tab_view_pager) ViewPager viewPager;
    @BindString(R.string.multi_view_first_view_title) String firstTitle;
    @BindString(R.string.multi_view_second_view_title) String secondTitle;
    @BindString(R.string.multi_view_third_view_title) String thirdTitle;
    @BindString(R.string.multi_view_fourth_view_title) String fourthTitle;

    private MultiViewsPagerAdapter pagerAdapter;
    private OnPageChangeListenerAdapter pageChangeListenerAdapter;
    private FirstView firstView;
    private SecondView secondView;
    private ThirdView thirdView;
    private FourthView fourthView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_view_tab);
        ButterKnife.bind(this);
        configureToolbar();
        configureViewPager();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        viewPager.removeAllViews();
        firstView = null;
        secondView = null;
        thirdView = null;
        fourthView = null;

        clearStateWhenFinishing();
        viewPager.removeOnPageChangeListener(pageChangeListenerAdapter);
    }

    private void clearStateWhenFinishing() {
        if (!isChangingConfigurations()) {
            EnumViewStateRepository.INSTANCE.clearViewState(EnumViewStateRepository.FIRST_VIEW_PAGER_STATE);
            EnumViewStateRepository.INSTANCE.clearViewState(EnumViewStateRepository.SECOND_VIEW_PAGER_STATE);
            EnumViewStateRepository.INSTANCE.clearViewState(EnumViewStateRepository.THIRD_VIEW_PAGER_STATE);
            EnumViewStateRepository.INSTANCE.clearViewState(EnumViewStateRepository.FOURTH_VIEW_PAGER_STATE);
        }
    }

    private void configureToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void configureViewPager() {
        pagerAdapter = new MultiViewsPagerAdapter(firstTitle, secondTitle, thirdTitle, fourthTitle);
        pagerAdapter.setFirstViewEventListener(this);
        pagerAdapter.setSecondViewEventListener(this);
        pagerAdapter.setThirdViewEventListener(this);
        pagerAdapter.setFourthViewEventListener(this);
        viewPager.setAdapter(pagerAdapter);
        pageChangeListenerAdapter = new OnPageChangeListenerAdapter() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                switch (position) {
                    case 0:
                        if (firstView != null) {
                            firstView.onViewDisplayed();
                        }
                        break;
                    case 1:
                        if (secondView != null) {
                            secondView.onViewDisplayed();
                        }
                        break;
                    case 2:
                        if (thirdView != null) {
                            thirdView.onViewDisplayed();
                        }
                        break;
                    case 3:
                        if (fourthView != null) {
                            fourthView.onViewDisplayed();
                        }
                        break;
                }
            }
        };
        viewPager.addOnPageChangeListener(pageChangeListenerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        pageChangeListenerAdapter.onPageSelected(0);
    }

    @Override
    protected void onStart() {
        super.onStart();
        int currentPage = viewPager.getCurrentItem();
        switch (currentPage) {
            case 0:
                if (firstView != null) {
                    firstView.onViewDisplayed();
                }
                break;
            case 1:
                if (secondView != null) {
                    secondView.onViewDisplayed();
                }
                break;
            case 2:
                if (thirdView != null) {
                    thirdView.onViewDisplayed();
                }
                break;
            case 3:
                if (fourthView != null) {
                    fourthView.onViewDisplayed();
                }
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }

    @Override
    public void onFirstViewAttached(FirstView firstView) {
        this.firstView = firstView;
    }

    @Override
    public void onSecondViewAttached(SecondView secondView) {
        this.secondView = secondView;
    }

    @Override
    public void onThirdViewAttached(ThirdView thirdView) {
        this.thirdView = thirdView;
    }

    @Override
    public void onFourthViewAttached(FourthView fourthView) {
        this.fourthView = fourthView;
    }

    @Override
    public void onFirstViewDetached() {
    }

    @Override
    public void onSecondViewDetached() {

    }

    @Override
    public void onThirdViewDetached() {
    }

    @Override
    public void onFourthViewDetached() {

    }

    public static Intent getIntent(Context context) {
        return new Intent(context, MultiViewsPagerActivity.class);
    }
}
