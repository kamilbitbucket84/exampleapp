package com.kamil.exampleapp.screen.multifragment.view;

import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.kamil.exampleapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kamil on 10/05/2017.
 */

public class ExampleViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.item_example_label) TextView label;
    @BindView(R.id.item_example_edittext) EditText editText;
    @BindView(R.id.item_example_checkbox) AppCompatCheckBox checkBox;
    @BindView(R.id.item_example_seekbar) AppCompatSeekBar seekBar;

    private ExampleItem exampleItem;

    public ExampleViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bindItem(ExampleItem exampleItem) {
        this.exampleItem = exampleItem;

        label.setText(exampleItem.title);
        editText.setText(exampleItem.input);
        checkBox.setChecked(exampleItem.checked);
        seekBar.setProgress(exampleItem.progress);
    }
}