package com.kamil.exampleapp.screen.browseproducts.di;


import com.kamil.exampleapp.datasource.database.ExampleDatabaseClient;
import com.kamil.exampleapp.screen.browseproducts.model.BrowseProductsModel;
import com.kamil.exampleapp.screen.browseproducts.presenter.BrowseProductsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class BrowseProductsModule {

    @Provides
    BrowseProductsModel provideModel(ExampleDatabaseClient databaseClient) {
        return new BrowseProductsModel(databaseClient);
    }

    @Provides
    BrowseProductsPresenter providePresenter(BrowseProductsModel browseProductsModel) {
        return new BrowseProductsPresenter(browseProductsModel);
    }
}
