package com.kamil.exampleapp.screen.experiment;

import android.content.Context;
import android.media.MediaPlayer;

/**
 * Created by Kamil on 04/05/2017.
 */

public class MediaPlayerWrapper {

    private MediaPlayer mediaPlayer;
    private Context context;
    private int currentSoundResId;

    public MediaPlayerWrapper(Context context) {
        this.context = context;
        mediaPlayer = new MediaPlayer();
    }

    public void playSound(int soundResId) {
        changeDataSourceIfNeeded(soundResId);
        startOrRestartSound();
    }

    private void startOrRestartSound() {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.seekTo(0);
            mediaPlayer.start();
        } else {
            mediaPlayer.start();
        }
    }

    private void changeDataSourceIfNeeded(int soundResId) {
        if (currentSoundResId != soundResId) {
            currentSoundResId = soundResId;
            mediaPlayer.reset();
            mediaPlayer = MediaPlayer.create(context, soundResId);
        }
    }

    public void stopPlaying() {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.seekTo(0);
            mediaPlayer.stop();
            mediaPlayer.prepareAsync();
        }
    }

    public void release() {
        mediaPlayer.reset();
        mediaPlayer.release();
        mediaPlayer = null;
        context = null;
    }

}
