package com.kamil.exampleapp.screen.multitab;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.kamil.exampleapp.R;
import com.kamil.exampleapp.common.util.LogUtils;
import com.kamil.exampleapp.common.view.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kamil on 08/05/2017.
 */

public class MultiTabActivity extends BaseActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.multitab_tab_layout) TabLayout tabLayout;
    @BindView(R.id.multitab_view_pager) ViewPager viewPager;

    private MultiTabFragmentPagerAdapter pagerAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_tab);
        ButterKnife.bind(this);
        configureToolbar();
        configureViewPager();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pagerAdapter.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }

    private void configureToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void configureViewPager() {
        pagerAdapter = new MultiTabFragmentPagerAdapter(getSupportFragmentManager(), getResources());
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, MultiTabActivity.class);
    }

}
