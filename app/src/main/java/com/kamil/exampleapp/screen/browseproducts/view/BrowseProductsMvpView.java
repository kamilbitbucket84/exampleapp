package com.kamil.exampleapp.screen.browseproducts.view;

import com.kamil.exampleapp.common.mvp.MvpView;
import com.kamil.exampleapp.screen.browseproducts.model.ProductItem;

import java.util.List;

public interface BrowseProductsMvpView extends MvpView {
    void onShowCreateProductDialog();
    void onHideCreateProductDialog();
    void onProductCreationInvalidInput();
    void onInsertProductInGivenHierarchiesSuccess(ProductItem productItem);
    void onGetProductItemsFromDbStarted();
    void onGetProductItemsFromDbSuccess(List<ProductItem> productItems);
    void onGetProductItemsFromDbError();
    void onProductItemUpdate(ProductItem productItem);
    void onProductItemRemove(ProductItem productItem);
    void onProductItemUpdateError(ProductItem productItem, String message);
    void onProductItemRemoveError(ProductItem productItem, String message);
}
