package com.kamil.exampleapp.screen.experiment.di;

import com.kamil.exampleapp.common.di.AppComponent;
import com.kamil.exampleapp.common.di.PerActivity;
import com.kamil.exampleapp.screen.experiment.view.ExperimentActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = AppComponent.class, modules = ExperimentModule.class)
public interface ExperimentComponent {
    void inject(ExperimentActivity experimentActivity);
}
