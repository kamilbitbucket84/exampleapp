package com.kamil.exampleapp.service.event.launcher;


import com.kamil.exampleapp.service.event.eventtype.LaunchEvent;

public interface OnEventLaunchedListener {

    void onEventLaunched(LaunchEvent launchEvent);

    OnEventLaunchedListener EMPTY = launchEvent -> {
    };
}
