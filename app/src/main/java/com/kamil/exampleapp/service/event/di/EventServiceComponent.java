package com.kamil.exampleapp.service.event.di;

import com.kamil.exampleapp.common.di.AppComponent;
import com.kamil.exampleapp.common.di.PerService;
import com.kamil.exampleapp.service.event.EventService;

import dagger.Component;

@PerService
@Component(dependencies = AppComponent.class, modules = EventServiceModule.class)
public interface EventServiceComponent {
    void inject(EventService eventService);
}
