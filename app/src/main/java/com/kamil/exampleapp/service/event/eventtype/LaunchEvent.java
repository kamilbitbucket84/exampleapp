package com.kamil.exampleapp.service.event.eventtype;

public interface LaunchEvent {

    enum EventType {
        TIME,
        PROXIMITY
    }

    long getTimeStamp();

    EventType getEventType();
}
