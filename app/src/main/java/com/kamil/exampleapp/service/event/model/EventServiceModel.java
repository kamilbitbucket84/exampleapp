package com.kamil.exampleapp.service.event.model;

import com.kamil.exampleapp.datasource.prefs.SettingsClient;

public class EventServiceModel {

    private final SettingsClient settingsClient;

    public EventServiceModel(SettingsClient settingsClient) {
        this.settingsClient = settingsClient;
    }
}
