package com.kamil.exampleapp.service.event.launcher;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;

import com.kamil.exampleapp.service.event.eventtype.ProximityEvent;

public class ProximityEventLauncher implements EventLauncher<ProximityEventLaunchParams> {

    private OnEventLaunchedListener onEventLaunchedListener = OnEventLaunchedListener.EMPTY;
    private final SensorManager sensorManager;
    private final Sensor proximitySensor;
    private ProximityEventLaunchParams eventLaunchParams;
    private boolean isWorking;

    public ProximityEventLauncher(Context context) {
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        proximitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
    }

    @Override
    public void start(ProximityEventLaunchParams eventLaunchParams) {
        this.eventLaunchParams = eventLaunchParams;
        sensorManager.registerListener(sensorEventListenerAdapter, proximitySensor, SensorManager.SENSOR_DELAY_NORMAL);
        isWorking = true;
    }

    @Override
    public void stop() {
        sensorManager.unregisterListener(sensorEventListenerAdapter);
        isWorking = false;
    }

    @Override
    public ProximityEventLaunchParams getEventLaunchParams() {
        return eventLaunchParams;
    }

    @Override
    public boolean isWorking() {
        return isWorking;
    }

    @Override
    public void setOnEventLaunchedListener(OnEventLaunchedListener listener) {
        onEventLaunchedListener = listener;
    }

    private SensorEventListenerAdapter sensorEventListenerAdapter = new SensorEventListenerAdapter() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            onEventLaunchedListener.onEventLaunched(new ProximityEvent(System.currentTimeMillis()));
        }
    };
}
