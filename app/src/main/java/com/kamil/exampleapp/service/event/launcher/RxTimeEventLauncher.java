package com.kamil.exampleapp.service.event.launcher;

import com.kamil.exampleapp.service.event.eventtype.TimeEvent;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

public class RxTimeEventLauncher implements EventLauncher<TimeEventLaunchParams> {

    private OnEventLaunchedListener onEventLaunchedListener = OnEventLaunchedListener.EMPTY;
    private Subscription timeEventSubscription = Subscriptions.empty();
    private TimeEventLaunchParams timeEventLaunchParams;
    private boolean isWorking;

    @Override
    public void start(TimeEventLaunchParams eventLaunchParams) {
        timeEventLaunchParams = eventLaunchParams;

        timeEventSubscription.unsubscribe();
        timeEventSubscription = Observable.interval
                (eventLaunchParams.delay, eventLaunchParams.eventPeriod, eventLaunchParams.timeUnit)
                .onBackpressureDrop()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(timeEvent -> {
                    onEventLaunchedListener.onEventLaunched(new TimeEvent(System.currentTimeMillis()));
                }, throwable -> {
                });

        isWorking = true;
    }

    @Override
    public void stop() {
        timeEventSubscription.unsubscribe();
        isWorking = false;
    }

    @Override
    public TimeEventLaunchParams getEventLaunchParams() {
        return timeEventLaunchParams;
    }

    @Override
    public boolean isWorking() {
        return isWorking;
    }

    @Override
    public void setOnEventLaunchedListener(OnEventLaunchedListener listener) {
        onEventLaunchedListener = listener;
    }
}
