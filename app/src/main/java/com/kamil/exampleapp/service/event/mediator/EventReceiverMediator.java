package com.kamil.exampleapp.service.event.mediator;

import com.kamil.exampleapp.service.event.launcher.ProximityEventLaunchParams;
import com.kamil.exampleapp.service.event.launcher.TimeEventLaunchParams;

public class EventReceiverMediator {

    private final EventMediator coreEventMediator;

    public EventReceiverMediator(EventMediator eventMediator) {
        this.coreEventMediator = eventMediator;
    }

    public void connectEventReceiver(EventReceiver eventReceiver) {
        coreEventMediator.connectEventReceiver(eventReceiver);
    }

    public void disconnectEventReceiver() {
        coreEventMediator.disconnectEventReceiver();
    }

    public boolean startProximityEventsLauncher(ProximityEventLaunchParams eventLaunchParams) {
        return coreEventMediator.startProximityEventsLauncher(eventLaunchParams);
    }

    public boolean stopProximityEventsLauncher() {
        return coreEventMediator.stopProximityEventsLauncher();
    }

    public boolean startTimeEventsLauncher(TimeEventLaunchParams timeEventLaunchParams) {
        return coreEventMediator.startTimeEventsLauncher(timeEventLaunchParams);
    }

    public boolean stopTimeEventsLauncher() {
        return coreEventMediator.stopTimeEventsLauncher();
    }
}
