package com.kamil.exampleapp.service.event.eventtype;

public class TimeEvent implements LaunchEvent {

    private final long timeStamp;

    public TimeEvent(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public long getTimeStamp() {
        return timeStamp;
    }

    @Override
    public EventType getEventType() {
        return EventType.TIME;
    }

    @Override
    public String toString() {
        return "TimeEvent{" +
                "timeStamp=" + timeStamp +
                '}';
    }
}
