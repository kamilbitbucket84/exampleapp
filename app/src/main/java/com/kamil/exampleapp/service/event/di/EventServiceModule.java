package com.kamil.exampleapp.service.event.di;

import android.content.Context;

import com.kamil.exampleapp.common.di.PerService;
import com.kamil.exampleapp.datasource.prefs.SettingsClient;
import com.kamil.exampleapp.service.event.presenter.EventServicePresenter;
import com.kamil.exampleapp.service.event.launcher.ProximityEventLauncher;
import com.kamil.exampleapp.service.event.launcher.RxTimeEventLauncher;
import com.kamil.exampleapp.service.event.model.EventServiceModel;

import dagger.Module;
import dagger.Provides;

@Module
public class EventServiceModule {

    @Provides
    @PerService
    RxTimeEventLauncher provideTimeEventLauncher() {
        return new RxTimeEventLauncher();
    }

    @Provides
    @PerService
    ProximityEventLauncher provideProximityEventLauncher(Context context) {
        return new ProximityEventLauncher(context);
    }

    @Provides
    @PerService
    EventServiceModel provideEventServiceModel(SettingsClient settingsClient) {
        return new EventServiceModel(settingsClient);
    }

    @Provides
    @PerService
    EventServicePresenter provideEventServiceInteractor(RxTimeEventLauncher rxTimeEventLauncher,
                                                        ProximityEventLauncher proximityEventLauncher,
                                                        EventServiceModel eventServiceModel) {
        return new EventServicePresenter(rxTimeEventLauncher, proximityEventLauncher, eventServiceModel);
    }
}
