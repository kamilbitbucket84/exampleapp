package com.kamil.exampleapp.service.event.launcher;

import java.util.concurrent.TimeUnit;

public class TimeEventLaunchParams {

    public final int delay;
    public final int eventPeriod;
    public final TimeUnit timeUnit;

    public TimeEventLaunchParams(int delay, int eventPeriod, TimeUnit timeUnit) {
        this.delay = delay;
        this.eventPeriod = eventPeriod;
        this.timeUnit = timeUnit;
    }
}
