package com.kamil.exampleapp.service.event.presenter;

import com.kamil.exampleapp.common.mvp.BaseMvpPresenter;
import com.kamil.exampleapp.service.event.EventServiceCallbacks;
import com.kamil.exampleapp.service.event.eventtype.LaunchEvent;
import com.kamil.exampleapp.service.event.launcher.OnEventLaunchedListener;
import com.kamil.exampleapp.service.event.launcher.ProximityEventLaunchParams;
import com.kamil.exampleapp.service.event.launcher.ProximityEventLauncher;
import com.kamil.exampleapp.service.event.launcher.TimeEventLaunchParams;
import com.kamil.exampleapp.service.event.launcher.RxTimeEventLauncher;
import com.kamil.exampleapp.service.event.model.EventServiceModel;

public class EventServicePresenter extends BaseMvpPresenter<EventServiceCallbacks>
        implements OnEventLaunchedListener {

    private final RxTimeEventLauncher rxTimeEventLauncher;
    private final ProximityEventLauncher proximityEventLauncher;
    private final EventServiceModel eventServiceModel;

    public EventServicePresenter(RxTimeEventLauncher rxTimeEventLauncher,
                                 ProximityEventLauncher proximityEventLauncher,
                                 EventServiceModel eventServiceModel) {
        this.rxTimeEventLauncher = rxTimeEventLauncher;
        this.proximityEventLauncher = proximityEventLauncher;
        this.eventServiceModel = eventServiceModel;
        configureListeners();
    }

    private void configureListeners() {
        rxTimeEventLauncher.setOnEventLaunchedListener(this);
        proximityEventLauncher.setOnEventLaunchedListener(this);
    }

    public void onServiceStop() {
        rxTimeEventLauncher.stop();
        proximityEventLauncher.stop();
    }

    public void onServiceStart() {
    }

    public void onStartTimeEventsLauncher(TimeEventLaunchParams timeEventLaunchParams) {
        rxTimeEventLauncher.start(timeEventLaunchParams);
    }

    public void onStopTimeEventsLauncher() {
        rxTimeEventLauncher.stop();
    }

    public void onStartProximityEventsLauncher(ProximityEventLaunchParams proximityEventLaunchParams) {
        proximityEventLauncher.start(proximityEventLaunchParams);
    }

    public void onStopProximityEventsLauncher() {
        proximityEventLauncher.stop();
    }

    @Override
    public void onEventLaunched(LaunchEvent launchEvent) {
        getView().onEventLaunched(launchEvent);
    }
}
