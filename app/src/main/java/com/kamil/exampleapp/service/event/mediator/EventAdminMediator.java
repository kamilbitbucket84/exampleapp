package com.kamil.exampleapp.service.event.mediator;

import com.kamil.exampleapp.service.event.eventtype.LaunchEvent;
import com.kamil.exampleapp.service.event.launcher.ProximityEventLaunchParams;
import com.kamil.exampleapp.service.event.launcher.TimeEventLaunchParams;

public class EventAdminMediator implements EventMediator {

    private EventGenerator eventGenerator;
    private EventReceiver eventReceiver;

    @Override
    public void connectEventGenerator(EventGenerator eventGenerator) {
        this.eventGenerator = null;
        this.eventGenerator = eventGenerator;
    }

    @Override
    public void disconnectEventGenerator() {
        this.eventGenerator = null;
    }

    @Override
    public void connectEventReceiver(EventReceiver eventReceiver) {
        this.eventReceiver = null;
        this.eventReceiver = eventReceiver;
    }

    @Override
    public void disconnectEventReceiver() {
        this.eventReceiver = null;
    }

    @Override
    public boolean startTimeEventsLauncher(TimeEventLaunchParams timeEventLaunchParams) {
        if (isConnectionEstablished()) {
            eventGenerator.startTimeEventsLauncher(timeEventLaunchParams);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean stopTimeEventsLauncher() {
        if (isConnectionEstablished()) {
            eventGenerator.stopTimeEventsLauncher();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean startProximityEventsLauncher(ProximityEventLaunchParams proximityEventLaunchParams) {
        if (isConnectionEstablished()) {
            eventGenerator.startProximityEventsLauncher(proximityEventLaunchParams);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean stopProximityEventsLauncher() {
        if (isConnectionEstablished()) {
            eventGenerator.stopProximityEventsLauncher();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean launchEvent(LaunchEvent launchEvent) {
        if (isConnectionEstablished()) {
            eventReceiver.onEventReceived(launchEvent);
            return true;
        } else {
            return false;
        }
    }

    private boolean isConnectionEstablished() {
        return eventGenerator != null && eventReceiver != null;
    }
}
