package com.kamil.exampleapp.service.event.mediator;

import com.kamil.exampleapp.service.event.launcher.ProximityEventLaunchParams;
import com.kamil.exampleapp.service.event.launcher.TimeEventLaunchParams;

public interface EventGenerator {

    void startTimeEventsLauncher(TimeEventLaunchParams timeEventLaunchParams);

    void stopTimeEventsLauncher();

    void startProximityEventsLauncher(ProximityEventLaunchParams proximityEventLaunchParams);

    void stopProximityEventsLauncher();

    EventGenerator EMPTY = new EventGenerator() {

        @Override
        public void startTimeEventsLauncher(TimeEventLaunchParams timeEventLaunchParams) {
        }

        @Override
        public void stopTimeEventsLauncher() {
        }

        @Override
        public void startProximityEventsLauncher(ProximityEventLaunchParams proximityEventLaunchParams) {
        }

        @Override
        public void stopProximityEventsLauncher() {
        }
    };
}
