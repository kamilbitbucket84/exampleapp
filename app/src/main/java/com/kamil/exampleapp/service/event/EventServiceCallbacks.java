package com.kamil.exampleapp.service.event;

import com.kamil.exampleapp.common.mvp.MvpView;
import com.kamil.exampleapp.service.event.eventtype.LaunchEvent;

public interface EventServiceCallbacks extends MvpView {

    void onEventLaunched(LaunchEvent launchEvent);

    EventServiceCallbacks EMPTY = launchEvent -> {
    };
}
