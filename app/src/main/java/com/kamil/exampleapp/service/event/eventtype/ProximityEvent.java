package com.kamil.exampleapp.service.event.eventtype;

public class ProximityEvent implements LaunchEvent {

    private final long timeStamp;

    public ProximityEvent(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public long getTimeStamp() {
        return timeStamp;
    }

    @Override
    public EventType getEventType() {
        return EventType.PROXIMITY;
    }

    @Override
    public String toString() {
        return "ProximityEvent{" +
                "timeStamp=" + timeStamp +
                '}';
    }
}
