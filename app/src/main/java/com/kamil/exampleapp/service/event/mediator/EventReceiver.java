package com.kamil.exampleapp.service.event.mediator;

import com.kamil.exampleapp.service.event.eventtype.LaunchEvent;

public interface EventReceiver {

    void onEventReceived(LaunchEvent launchEvent);

    EventReceiver EMPTY = launchEvent -> {
    };
}
