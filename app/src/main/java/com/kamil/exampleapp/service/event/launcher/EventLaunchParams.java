package com.kamil.exampleapp.service.event.launcher;

public class EventLaunchParams {

    public final boolean timeEventsLauncherWorking;
    public final boolean proximityEventsLauncherWorking;
    public final TimeEventLaunchParams timeEventLaunchParams;
    public final ProximityEventLaunchParams proximityEventLaunchParams;

    public EventLaunchParams(boolean timeEventsLauncherWorking,
                             boolean proximityEventsLauncherWorking,
                             TimeEventLaunchParams timeEventLaunchParams,
                             ProximityEventLaunchParams proximityEventLaunchParams) {
        this.timeEventsLauncherWorking = timeEventsLauncherWorking;
        this.proximityEventsLauncherWorking = proximityEventsLauncherWorking;
        this.timeEventLaunchParams = timeEventLaunchParams;
        this.proximityEventLaunchParams = proximityEventLaunchParams;
    }
}
