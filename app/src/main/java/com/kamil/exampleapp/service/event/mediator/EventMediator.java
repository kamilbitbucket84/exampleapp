package com.kamil.exampleapp.service.event.mediator;

import com.kamil.exampleapp.service.event.eventtype.LaunchEvent;
import com.kamil.exampleapp.service.event.launcher.ProximityEventLaunchParams;
import com.kamil.exampleapp.service.event.launcher.TimeEventLaunchParams;

public interface EventMediator {

    void connectEventGenerator(EventGenerator eventGenerator);

    void disconnectEventGenerator();

    void connectEventReceiver(EventReceiver eventReceiver);

    void disconnectEventReceiver();

    boolean startTimeEventsLauncher(TimeEventLaunchParams timeEventLaunchParams);

    boolean stopTimeEventsLauncher();

    boolean startProximityEventsLauncher(ProximityEventLaunchParams proximityEventLaunchParams);

    boolean stopProximityEventsLauncher();

    boolean launchEvent(LaunchEvent launchEvent);
}
