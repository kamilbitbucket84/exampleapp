package com.kamil.exampleapp.service.event.launcher;

public class ProximityEventLaunchParams {

    public final int numberOfEventsToLaunch;

    public ProximityEventLaunchParams(int numberOfEventsToLaunch) {
        this.numberOfEventsToLaunch = numberOfEventsToLaunch;
    }
}
