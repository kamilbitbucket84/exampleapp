package com.kamil.exampleapp.service.event;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.kamil.exampleapp.common.di.Dagger;
import com.kamil.exampleapp.common.notification.ExampleNotificationCreator;
import com.kamil.exampleapp.common.notification.NotificationCreator;
import com.kamil.exampleapp.common.notification.NotificationParams;
import com.kamil.exampleapp.common.notification.ProximityEventNotificationData;
import com.kamil.exampleapp.common.notification.TimeEventNotificationData;
import com.kamil.exampleapp.common.util.LogUtils;
import com.kamil.exampleapp.common.util.MockedDataUtils;
import com.kamil.exampleapp.service.event.di.DaggerEventServiceComponent;
import com.kamil.exampleapp.service.event.di.EventServiceModule;
import com.kamil.exampleapp.service.event.eventtype.LaunchEvent;
import com.kamil.exampleapp.service.event.launcher.ProximityEventLaunchParams;
import com.kamil.exampleapp.service.event.launcher.TimeEventLaunchParams;
import com.kamil.exampleapp.service.event.mediator.EventGenerator;
import com.kamil.exampleapp.service.event.mediator.EventGeneratorMediator;
import com.kamil.exampleapp.service.event.presenter.EventServicePresenter;

import javax.inject.Inject;

public class EventService extends Service implements EventGenerator, EventServiceCallbacks {

    private NotificationCreator notificationCreator;
    @Inject EventGeneratorMediator eventGeneratorMediator;
    @Inject EventServicePresenter eventServicePresenter;


    private BroadcastReceiver notificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case ExampleNotificationCreator.PROXIMITY_NOTIFICATION_ACTION_ONE:
                    LogUtils.d("ONE");
                    break;
                case ExampleNotificationCreator.PROXIMITY_NOTIFICATION_ACTION_TWO:
                    LogUtils.d("TWO");
                    break;
                case ExampleNotificationCreator.PROXIMITY_NOTIFICATION_ACTION_THREE:
                    LogUtils.d("THREE");
                    break;
                case ExampleNotificationCreator.PROXIMITY_NOTIFICATION_ACTION_FOUR:
                    LogUtils.d("FOUR");
                    break;
            }
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        injectIntoComponent();
        registerNotificationReceiver();
    }

    private void injectIntoComponent() {
        DaggerEventServiceComponent.builder()
                .appComponent(Dagger.appComponent())
                .eventServiceModule(new EventServiceModule())
                .build()
                .inject(this);

        notificationCreator = new ExampleNotificationCreator(this,
                new NotificationParams(true, NotificationCompat.PRIORITY_DEFAULT));
    }

    private void registerNotificationReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ExampleNotificationCreator.PROXIMITY_NOTIFICATION_ACTION_ONE);
        intentFilter.addAction(ExampleNotificationCreator.PROXIMITY_NOTIFICATION_ACTION_TWO);
        intentFilter.addAction(ExampleNotificationCreator.PROXIMITY_NOTIFICATION_ACTION_THREE);
        intentFilter.addAction(ExampleNotificationCreator.PROXIMITY_NOTIFICATION_ACTION_FOUR);
        registerReceiver(notificationReceiver, intentFilter);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        onServiceStart();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        onServiceStop();
        unregisterReceiver(notificationReceiver);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        onServiceStop();
    }

    private void onServiceStart() {
        eventGeneratorMediator.connectEventGenerator(this);
        eventServicePresenter.attachView(this);
        eventServicePresenter.onServiceStart();
    }

    private void onServiceStop() {
        eventGeneratorMediator.disconnectEventGenerator();
        eventServicePresenter.onServiceStop();
        eventServicePresenter.detachView();
    }

    @Override
    public void onEventLaunched(LaunchEvent launchEvent) {
        eventGeneratorMediator.launchEvent(launchEvent);
        createProperNotification(launchEvent);
    }

    private void createProperNotification(LaunchEvent launchEvent) {
        if (launchEvent.getEventType() == LaunchEvent.EventType.TIME) {
            TimeEventNotificationData data = new TimeEventNotificationData.Builder()
                    .eventId(1)
                    .iconResId(android.R.drawable.stat_notify_sync)
                    .titleText("Time event")
                    .contentText(launchEvent.getTimeStamp() + "")
                    .build();

            notificationCreator.createOrUpdateTimeEventNotification(data);
        } else {
            ProximityEventNotificationData data = new ProximityEventNotificationData.Builder()
                    .id(2)
                    .iconUrl(MockedDataUtils.EXAMPLE_THUMB)
                    .title("Proximity event")
                    .dateString("12.45")
                    .firstValue("First: 1")
                    .secondValue("Second: 2")
                    .thirdValue("Third: 3")
                    .build();

            notificationCreator.createOrUpdateProximityEventNotification(data);
        }
    }

    ///////////////////////////////////////////////////////////////////////////////
    //  EVENT GENERATOR METHODS
    ///////////////////////////////////////////////////////////////////////////////

    @Override
    public void startTimeEventsLauncher(TimeEventLaunchParams timeEventLaunchParams) {
        eventServicePresenter.onStartTimeEventsLauncher(timeEventLaunchParams);
    }

    @Override
    public void stopTimeEventsLauncher() {
        eventServicePresenter.onStopTimeEventsLauncher();
    }

    @Override
    public void startProximityEventsLauncher(ProximityEventLaunchParams eventLaunchParams) {
        eventServicePresenter.onStartProximityEventsLauncher(eventLaunchParams);
    }

    @Override
    public void stopProximityEventsLauncher() {
        eventServicePresenter.onStopProximityEventsLauncher();
    }
}
