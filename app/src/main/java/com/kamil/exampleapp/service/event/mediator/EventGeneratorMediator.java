package com.kamil.exampleapp.service.event.mediator;

import com.kamil.exampleapp.service.event.eventtype.LaunchEvent;

public class EventGeneratorMediator {

    private final EventMediator coreEventMediator;

    public EventGeneratorMediator(EventMediator eventMediator) {
        this.coreEventMediator = eventMediator;
    }

    public void connectEventGenerator(EventGenerator eventGenerator) {
        coreEventMediator.connectEventGenerator(eventGenerator);
    }

    public void disconnectEventGenerator() {
        coreEventMediator.disconnectEventGenerator();
    }

    public boolean launchEvent(LaunchEvent launchEvent) {
        return coreEventMediator.launchEvent(launchEvent);
    }
}
