package com.kamil.exampleapp.service.event.launcher;

public interface EventLauncher<T> {

    void start(T eventLaunchParams);

    void stop();

    T getEventLaunchParams();

    boolean isWorking();

    void setOnEventLaunchedListener(OnEventLaunchedListener listener);
}
