# ExampleApp

This app is my sandbox for learning old and new android technologies  
For example:  

  - Model View Presenter architecture with UseCases
  - Retrofit - requests to API
  - RxJava
  - Picasso - image loading
  - RecyclerView
  - ViewPager - with views and fragments
  - Navigation Drawer
  - Compound Views with custom attributes
  - Notifications
  - MediaPlayer
  - Sensors - proximity
  - Android service
  - SharedPreferences
  - Sqlite
  - Junit
  - Roboelectric  
  
  
I will add more things succesively  
Have a nice code reading  

Best Regards  
Kamil
